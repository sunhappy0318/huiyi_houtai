<?php

namespace addons\mrbs\controller;

use think\addons\Controller;

class Base extends Controller
{
    public function _initialize()
    {
        parent::_initialize();
    }
}
