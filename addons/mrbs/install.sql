-- ----------------------------
-- Table structure for __PREFIX__mrbs_edifice
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_edifice` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `city` varchar(100) NOT NULL COMMENT '城市',
  `name` varchar(100) NOT NULL COMMENT '大厦',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='大厦表';
BEGIN;
-- ----------------------------
-- Records of __PREFIX__mrbs_edifice
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__mrbs_edifice` VALUES (1, '北京/北京市/宣武区', '百度大厦', 0);
COMMIT;
-- ----------------------------
-- Table structure for __PREFIX__mrbs_facilities
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_facilities` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) NOT NULL COMMENT '设施名称',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='设施表';
-- ----------------------------
-- Records of __PREFIX__mrbs_facilities
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__mrbs_facilities` VALUES (1, ' 投影仪', 0);
COMMIT;

-- ----------------------------
-- Table structure for __PREFIX__mrbs_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `token` varchar(255) NOT NULL COMMENT '用户唯一标识',
  `contact` varchar(200) NOT NULL COMMENT '联系方式',
  `room_id` int(10) NOT NULL COMMENT '房间ID',
  `title` varchar(100) NOT NULL COMMENT '会议名称',
  `people` int(10) NOT NULL DEFAULT '0' COMMENT '人数',
  `starttime` int(10) NOT NULL COMMENT '开始时间',
  `endtime` int(10) NOT NULL COMMENT '结束时间',
  `status` enum('0','1','-1') NOT NULL COMMENT '状态:0=待审核,1=通过,-1=已拒绝',
  `hour` decimal(10,2) NOT NULL COMMENT '会议时长',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='预订表';

-- ----------------------------
-- Table structure for __PREFIX__mrbs_room
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_room` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `edifice_id` int(10) NOT NULL COMMENT '大厦',
  `title` varchar(200) NOT NULL COMMENT '房间标题',
  `floor` varchar(100) DEFAULT NULL COMMENT '所在楼层',
  `number` varchar(100) DEFAULT NULL COMMENT '房间号',
  `images` varchar(500) DEFAULT NULL COMMENT '房间图片',
  `people` int(10) NOT NULL COMMENT '最大人数',
  `facilities` varchar(1000) DEFAULT NULL COMMENT '设施',
  `content` text COMMENT '房间介绍',
  `is_verify` enum('0','1') NOT NULL DEFAULT '1' COMMENT '是否审核:0=否,1=是',
  `status` enum('0','1') NOT NULL COMMENT '状态:0=关闭,1=开放',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='房间表';
-- ----------------------------
-- Records of __PREFIX__mrbs_room
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__mrbs_room` VALUES (1, 1, '21楼会议室', '21', '2102', '/assets/img/qrcode.png', 10, '[\"1\"]', '', '1', '1', 0);
COMMIT;

-- ----------------------------
-- Table structure for __PREFIX__mrbs_room_times
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_room_times` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `room_id` int(10) NOT NULL COMMENT '房间id',
  `time` int(2) NOT NULL COMMENT '开放时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of __PREFIX__mrbs_room_times
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (1, 1, 9);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (2, 1, 10);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (3, 1, 11);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (4, 1, 12);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (5, 1, 13);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (6, 1, 14);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (7, 1, 15);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (8, 1, 16);
INSERT INTO `__PREFIX__mrbs_room_times` VALUES (9, 1, 17);
COMMIT;

-- ----------------------------
-- Table structure for __PREFIX__mrbs_room_weeks
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__mrbs_room_weeks` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `room_id` int(10) NOT NULL COMMENT '房间id',
  `week` int(1) NOT NULL COMMENT '星期几',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='开放周期表';
-- ----------------------------
-- Records of __PREFIX__mrbs_room_weeks
-- ----------------------------
BEGIN;
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (1, 1, 0);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (2, 1, 1);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (3, 1, 2);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (4, 1, 3);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (5, 1, 4);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (6, 1, 5);
INSERT INTO `__PREFIX__mrbs_room_weeks` VALUES (7, 7, 4);
COMMIT;