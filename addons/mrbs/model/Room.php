<?php

namespace addons\mrbs\model;

use think\Model;

class Room extends Model
{

    // 表名
    protected $name = 'mrbs_room';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'room_type_text'
    ];

    // 类型转换
    protected $type = [
        'facilities' => 'array',
    ];

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }
    //会议室类型
    public  function getRoomTypeList(){
        return ['0' => __('Room_type 0'), '1' => __('Room_type 1')];
    }
    //获取会议室类型文本
    public function getRoomTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['room_type']) ? $data['room_type'] : '');
        $list  = $this->getRoomTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list  = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function edifice()
    {
        return $this->belongsTo('Edifice', 'edifice_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
