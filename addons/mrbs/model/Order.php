<?php

namespace addons\mrbs\model;

use think\Model;

class Order extends Model
{

    // 表名
    protected $name = 'mrbs_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'starttime_text',
        'endtime_text',
        'status_text',
        'push_text',
    ];

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1'), '-1' => __('Status -1')];
    }

    public function getPushList()
    {
        return ['0' => __('Push 0'), '1' => __('Push 1'), '2' => __('Push 2')];
    }

    public function getStarttimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['starttime']) ? $data['starttime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getEndtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['endtime']) ? $data['endtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list  = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getPushTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['push']) ? $data['push'] : '');
        $list  = $this->getPushList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setStarttimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndtimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function room()
    {
        return $this->belongsTo('Room', 'room_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function meet()
    {
        return $this->belongsTo('\app\admin\model\Meet', 'meet_id', 'id', [], 'LEFT');
    }
    public function user()
    {
        return $this->belongsTo('\app\api\model\MeetUserModel', 'user_id', 'id', [], 'LEFT');
    }
}
