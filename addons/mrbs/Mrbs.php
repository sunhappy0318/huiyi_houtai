<?php

namespace addons\mrbs;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Mrbs extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'mrbs',
                'title'   => '预约管理',
                'icon'    => 'fa fa-home',
                'sublist' => [
                    [
                        'name'    => 'mrbs/edifice',
                        'title'   => '大厦管理',
                        'icon'    => 'fa fa-institution',
                        'sublist' => [
                            ['name' => 'mrbs/edifice/index', 'title' => '查看'],
                            ['name' => 'mrbs/edifice/add', 'title' => '添加'],
                            ['name' => 'mrbs/edifice/edit', 'title' => '变更'],
                            ['name' => 'mrbs/edifice/del', 'title' => '删除'],
                            ['name' => 'mrbs/edifice/multi', 'title' => '批量更新'],
                        ],
                    ],
                    [
                        'name'    => 'mrbs/facilities',
                        'title'   => '设施管理',
                        'icon'    => 'fa fa-medkit',
                        'sublist' => [
                            ['name' => 'mrbs/facilities/index', 'title' => '查看'],
                            ['name' => 'mrbs/facilities/add', 'title' => '添加'],
                            ['name' => 'mrbs/facilities/edit', 'title' => '变更'],
                            ['name' => 'mrbs/facilities/del', 'title' => '删除'],
                            ['name' => 'mrbs/facilities/multi', 'title' => '批量更新'],
                        ],
                    ],
                    [
                        'name'    => 'mrbs/room',
                        'title'   => '会议室管理',
                        'icon'    => 'fa fa-home',
                        'sublist' => [
                            ['name' => 'mrbs/room/index', 'title' => '查看'],
                            ['name' => 'mrbs/room/add', 'title' => '添加'],
                            ['name' => 'mrbs/room/edit', 'title' => '变更'],
                            ['name' => 'mrbs/room/del', 'title' => '删除'],
                            ['name' => 'mrbs/room/multi', 'title' => '批量更新'],
                        ],
                    ],
                    [
                        'name'    => 'mrbs/order',
                        'title'   => '预定管理',
                        'icon'    => 'fa fa-reorder',
                        'sublist' => [
                            ['name' => 'mrbs/order/index', 'title' => '查看'],
                            ['name' => 'mrbs/order/edit', 'title' => '变更'],
                            ['name' => 'mrbs/order/del', 'title' => '删除'],
                            ['name' => 'mrbs/order/multi', 'title' => '批量更新'],
                        ],
                    ],
                ],
            ],
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete("mrbs");
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable("mrbs");
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable("mrbs");
        return true;
    }

}
