<?php
namespace addons\mydemo\controller;

use think\Config;
use think\Db;

/**
 * 定时任务
 */
class Autotask extends \think\addons\Controller
{
    protected $noNeedLogin = ["*"];
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();

        if (!$this->request->isCli()) {
            $this->error('只允许在终端进行操作!');
        }
    }

    /**
     * 定时任务逻辑
     */
    public function index()
    {
        //这里编写我们的定时任务逻辑
        echo "done";
        return;
    }
}
