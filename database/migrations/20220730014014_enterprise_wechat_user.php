<?php

use think\migration\Migrator;
use think\migration\db\Column;

class EnterpriseWechatUser extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('enterprise_wechat_user');
        $table->addColumn('name', 'string', ['limit' => 16, 'null' => false,'comment'=>'视频名称'])
            ->addColumn('email', 'string', ['limit' => 32, 'null' => false,'comment'=>'邮箱'])
            ->addColumn('password', 'string', ['limit' => 64, 'null' => false,'comment'=>'密码'])
            ->create();
    }
}
