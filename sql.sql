CREATE TABLE `fa_sign` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) DEFAULT '0' COMMENT '会员ID',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='签到管理';



CREATE TABLE `fa_sign_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) DEFAULT '0' COMMENT '会员ID',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '姓名',
  `account` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '账号',
  `bumen` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '部门',
  `sex` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '性别',
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='签到用户';

CREATE TABLE `fa_meet_place` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `place_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '姓名',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='使用场所';




CREATE TABLE `fa_meet_topic_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '模板标题',
  `content` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '议题模板内容',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='议题模板';


CREATE TABLE `fa_meet_topic_top` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_top` int(4) DEFAULT '0' COMMENT '1=顶置,0=默认',
  `meet_id` int(10) NOT NULL DEFAULT '0' COMMENT '会议ID',
  `user_id` int(10) DEFAULT '0' COMMENT '议题参与人ID',
  `createtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8mb4 COMMENT='议题用户置顶';


CREATE TABLE `fa_dev` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `domain` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '域名',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='开发环境域名';

CREATE TABLE `fa_teacher_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_ids` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '查看人',
  `user_auth_ids` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '查看人权限',
  `is_all` tinyint(2) NOT NULL COMMENT '是否为全部:1=是,0=否',
  `createtime` bigint(16) DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='教师档案配置';
