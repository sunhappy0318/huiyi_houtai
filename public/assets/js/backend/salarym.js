define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'salarym/index' + location.search,
                    add_url: 'salary/add',
                    // edit_url: 'salary/edit',
                    del_url: 'salarym/del',
                    multi_url: 'salary/multi',
                    import_url: 'salary/import',
                    table: 'teacher_salary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                searchFormVisible:false,
                fixedRightNumber: 1,
                search:false,//快速搜索，搜索框
                // showSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user.nickname', title: __('Name'), operate: 'LIKE'},
                        {field: 'send_time', title: __('Send_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'user.id', title: __('教师ID')},
                        {field: 'salary_month', title: __('Salary_month'), operate: false},
                        {field: 'post_salary', title: __('Post_salary'), operate:'BETWEEN'},
                        {field: 'subtotal_1', title: __('Subtotal_1'), operate:'BETWEEN'},
                        {field: 'special_salary', title: __('Special_salary'), operate:'BETWEEN'},
                        {field: 'basic_salary', title: __('Basic_salary'), operate:'BETWEEN'},
                        {field: 'post_subsidy', title: __('Post_subsidy'), operate:'BETWEEN'},
                        {field: 'subtotal_2', title: __('Subtotal_2'), operate:'BETWEEN'},
                        {field: 'life_salary', title: __('Life_salary'), operate:'BETWEEN'},
                        {field: 'rescue_salary', title: __('Rescue_salary'), operate:'BETWEEN'},
                        {field: 'only_child_salary', title: __('Only_child_salary'), operate:'BETWEEN'},
                        {field: 'subtotal_3', title: __('Subtotal_3'), operate:'BETWEEN'},
                        {field: 'reserve', title: __('Reserve'), operate:'BETWEEN'},
                        {field: 'class_salary', title: __('Class_salary'), operate:'BETWEEN'},
                        {field: 'surplus_salary', title: __('Surplus_salary'), operate:'BETWEEN'},
                        {field: 'job_salary', title: __('Job_salary'), operate:'BETWEEN'},
                        {field: 'up_1360', title: __('Up_1360'), operate:'BETWEEN'},
                        {field: 'check_work_salary', title: __('Check_work_salary'), operate:'BETWEEN'},
                        {field: 'buckle_class_salary', title: __('Buckle_class_salary'), operate:'BETWEEN'},
                        {field: 'post_subsidy_salary', title: __('Post_subsidy_salary'), operate:'BETWEEN'},
                        {field: 'subtotal_4', title: __('Subtotal_4'), operate:'BETWEEN'},
                        {field: 'reissue_salary', title: __('Reissue_salary'), operate:'BETWEEN'},
                        {field: 'subtotal_5', title: __('Subtotal_5'), operate:'BETWEEN'},
                        {field: 'fund_salary', title: __('Fund_salary'), operate:'BETWEEN'},
                        {field: 'repair_fund_salary', title: __('Repair_fund_salary'), operate:'BETWEEN'},
                        {field: 'medical_salary', title: __('Medical_salary'), operate:'BETWEEN'},
                        {field: 'repair_medical_salary', title: __('Repair_medical_salary'), operate:'BETWEEN'},
                        {field: 'old_salary', title: __('Old_salary'), operate:'BETWEEN'},
                        {field: 'repair_old_salary', title: __('Repair_old_salary'), operate:'BETWEEN'},
                        {field: 'occupation_salary', title: __('Occupation_salary'), operate:'BETWEEN'},
                        {field: 'repair_occupation_salary', title: __('Repair_occupation_salary'), operate:'BETWEEN'},
                        {field: 'subtotal_6', title: __('Subtotal_6'), operate:'BETWEEN'},
                        {field: 'total_1', title: __('Total_1'), operate:'BETWEEN'},
                        {field: 'expense_deduction', title: __('Expense_deduction'), operate:'BETWEEN'},
                        {field: 'taxable_income', title: __('Taxable_income'), operate:'BETWEEN'},
                        {field: 'tax_rate', title: __('Tax_rate'), operate:'BETWEEN'},
                        {field: 'personal_tax', title: __('Personal_tax'), operate:'BETWEEN'},
                        {field: 'repair_personal_tax', title: __('Repair_personal_tax'), operate:'BETWEEN'},
                        {field: 'issued_amount', title: __('Issued_amount'), operate:'BETWEEN'},
                        {field: 'two_stickers', title: __('Two_stickers'), operate:'BETWEEN'},
                        {field: 'base_salary', title: __('Base_salary'), operate:'BETWEEN'},
                        {field: 'boss_salary', title: __('Boss_salary'), operate:'BETWEEN'},
                        {field: 'rank_salary', title: __('Rank_salary'), operate:'BETWEEN'},
                        {field: 'best_salary', title: __('Best_salary'), operate:'BETWEEN'},
                        {field: 'basic_merits_salary', title: __('Basic_merits_salary'), operate:'BETWEEN'},
                        {field: 'house_salary', title: __('House_salary'), operate:'BETWEEN'},
                        {field: 'total', title: __('Total'), operate:'BETWEEN'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(".btn-download").on('click', function () {
                window.open(document.location.origin + "/工资表头.xlsx")
                // location.replace('teacher/download');
                // console.log(document.location)
                // console.log();
            });
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'salary/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'salary/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'salary/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
