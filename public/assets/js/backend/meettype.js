define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'meettype/index' + location.search,
                    add_url: 'meettype/add',
                    edit_url: 'meettype/edit',
                    del_url: 'meettype/del',
                    multi_url: 'meettype/multi',
                    import_url: 'meettype/import',
                    table: 'meet_type',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        {field: 'meet_type', title: __('系统'),searchList: {"1":__('听评课系统'),"0":__('清外会议系统')},operate:false,formatter: Table.api.formatter.status},
                        {field: 'type_text', title: __('Type'),operate:false},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                      //  {field: 'auth', title: __('Auth'), operate: 'LIKE',operate:false},
                      //  {field: 'examine', title: __('Examine'), operate: 'LIKE',operate:false},
                        {field: 'faqi_ids', title: '发起人',operate:false},
                        {field: 'attend_meet_ids', title: __('Attend_meet_ids'),operate:false},
                        {field: 'attend_topic_ids', title: __('Attend_topic_ids'),operate:false},
                        //{field: 'approver_ids', title: __('Approver_ids'),operate:false},
                        {field: 'issued_ids', title: __('Issued_ids'),operate:false},
                        {field: 'check_ids', title: __('Check_ids'), operate: 'LIKE',operate:false},
                        {field: 'collector.nickname', title: '收集人', operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'meettype/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'meettype/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'meettype/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
