define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'corp/index' + location.search,
                    add_url: 'corp/add',
                    edit_url: 'corp/edit',
                    del_url: 'corp/del',
                    multi_url: 'corp/multi',
                    import_url: 'corp/import',
                    table: 'corp',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'ticket_time', title: __('Ticket_time'), operate: 'LIKE'},
                        {field: 'SuiteId', title: __('Suiteid'), operate: 'LIKE'},
                        {field: 'suite_ticket', title: __('Suite_ticket'), operate: 'LIKE'},
                        {field: 'suite_secret', title: __('Suite_secret'), operate: 'LIKE'},
                        {field: 'open_corpid', title: __('Open_corpid'), operate: 'LIKE'},
                        {field: 'permanent_code', title: __('Permanent_code'), operate: 'LIKE'},
                        {field: 'corpid', title: __('Corpid'), operate: 'LIKE'},
                        {field: 'corp_name', title: __('Corp_name'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
