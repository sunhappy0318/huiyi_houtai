define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'meetweek/index' + location.search,
                    add_url: 'meetweek/add',
                    //edit_url: 'meetweek/edit',
                    //del_url: 'meetweek/del',
                    multi_url: 'meetweek/multi',
                    import_url: 'meetweek/import',
                    table: 'meet_week',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'leader_content', title: __('Leader_content'), operate: 'LIKE'},
                        {field: 'end_content', title: __('End_content'), operate: 'LIKE'},
                        {field: 'xiaojie', title: __('Xiaojie'), operate: 'LIKE'},
                        {field: 'next_content', title: __('Next_content'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'user.nickname', title: '用户', operate: 'LIKE'},
                        {field: 'department.name', title: __('Department.name'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
