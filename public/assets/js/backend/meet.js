define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'meet/index' + location.search,
                    add_url: 'meet/add',
                    edit_url: 'meet/edit',
                    topic_url: 'meettopic/edit',
                    //del_url: 'meet/del',
                    multi_url: 'meet/multi',
                    import_url: 'meet/import',
                    table: 'meet',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                       // {field: 'type_id', title: __('Type_id')},
                       // {field: 'user_id', title: __('User_id')},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'type', title: '会议类型',operate: false},
                        {field: 'room', title: '会议室', operate: false},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3'),"21":__('Status 21'),"8":__('Status 8'),"-1":__('Status -1'),"9":__('Status 9'),"31":__('Status 31')}, formatter: Table.api.formatter.status},
                      //  {field: 'weigh', title: __('Weigh'), operate: false},
                       // {field: 'room_id', title: __('Room_id')},
                      //  {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                      //  {field: 'images', title: __('Images'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.images},
                      //  {field: 'params', title: __('Params'), operate: 'LIKE'},
                        {field: 'start_time', title: __('Start_time'), operate: 'LIKE'},
                        {field: 'end_time', title: __('End_time'), operate: 'LIKE'},
                        {field: 'sign_way_text', title: __('Sign_way')},
                        //{field: 'have_goods', title: __('Have_goods') ,searchList: {"1":'有',"0":'无'}, formatter: Table.api.formatter.have_goods},
                        //{field: 'goods_name', title: __('Goods_name'), operate: 'LIKE'},
                      //  {field: 'views', title: __('Views')},

                      //  {field: 'files', title: __('Files'), operate: false, formatter: Table.api.formatter.files},
                      //  {field: 'check_room_ids', title: __('Check_room_ids'), operate: 'LIKE'},
                      //  {field: 'check_ids', title: __('Check_ids'), operate: 'LIKE'},
                      //  {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                      //  {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                     //   {field: 'late_time', title: __('Late_time'), operate: 'LIKE'},
                    //    {field: 'normal_time', title: __('Normal_time'), operate: 'LIKE'},
                    //    {field: 'normal_tip', title: __('Normal_tip'), operate: 'LIKE'},
                    //    {field: 'late_tip', title: __('Late_tip'), operate: 'LIKE'},
                     //   {field: 'absence_tip', title: __('Absence_tip'), operate: 'LIKE'},
                       {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                           buttons: [{
                               name: 'detail',
                               text: '签到',
                               icon: 'fa fa-list',
                               classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                               url: 'meetsign/index?meet_id={ids}'
                           },{
                               name: 'detail',
                               text: '周计划',
                               icon: 'fa fa-list',
                               classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                               url: 'meetplan/index?meet_id={ids}'
                           },{
                               name: 'detail',
                               text: '周反馈',
                               icon: 'fa fa-list',
                               classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                               url: 'meetweek/index?meet_id={ids}'
                           },{
                               name: 'detail',
                               text: '会议议题',
                               icon: 'fa fa-list',
                               classname: 'btn btn-info btn-xs btn-detail btn-dialog',
                               url: 'meettopic/index?meet_id={ids}'
                           }],
                           formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'meet/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,

                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'meet/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'meet/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        topic: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
