define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'meetplan/index' + location.search,
                    add_url: 'meetplan/add',
                    //edit_url: 'meetplan/edit',
                    //del_url: 'meetplan/del',
                    multi_url: 'meetplan/multi',
                    import_url: 'meetplan/import',
                    table: 'meet_plan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        //{field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        {field: 'date_aaa', title: __('Date_text'), operate: 'LIKE'},
                        {field: 'zhongxin', title:'中心工作', operate: 'LIKE'},
                        {field: 'department.name', title: '牵头部门', operate: 'LIKE'},
                        {field: 'fuzeren', title: __('Fuzeren'), operate: 'LIKE'},
                        {field: 'cangsuo', title: __('Cangsuo'), operate: 'LIKE'},
                        {field: 'use_time', title: __('Use_time'), operate: 'LIKE'},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: '提交时间', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},


                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
