define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'mrbs/room/index' + location.search,
                    add_url: 'mrbs/room/add',
                    edit_url: 'mrbs/room/edit',
                    book_url: 'mrbs/book/index',
                    del_url: 'mrbs/room/del',
                    multi_url: 'mrbs/room/multi',
                    table: 'mrbs/room',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        {field: 'name', title: __('Name')},
                        {field: 'title', title: __('Title')},
                        {field: 'floor', title: __('Floor'),operate:false},
                       {field: 'number', title: __('Number'),operate:false},
                      //  {field: 'images', title: __('Images'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'people', title: __('People'),operate:false},
                       // {field: 'manager_name', title: __('Manager')},
                        //房间类型
                       // {field: 'room_type', title: __('Room_type')},
                      {field: 'room_type_text', title: __('Room_type'),operate:false},
                        //开放时间
                        {field: 'times', title: __('times'),operate:false},
                     //   {field: 'facilities_list', title: __('Facilities')},
                      //  {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                       // {field: 'weigh', title: __('Weigh')},
                        {field: 'operate', title: __('Operate'),
                            buttons: [
                                {
                                    name: 'execute',
                                    title: '预约记录',
                                    text: '预约记录',
                                    url: 'mrbs/book/index?room_id={ids}',
                                    icon: 'fa fa-list',
                                    classname: 'btn btn-info btn-xs btn-execute btn-dialog'
                                }
                            ],
                            table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        book: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
