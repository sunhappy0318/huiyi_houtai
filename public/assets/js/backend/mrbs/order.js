define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'mrbs/order/index' + location.search,
                    add_url: 'mrbs/order/add',
                    edit_url: 'mrbs/order/edit',
                    del_url: 'mrbs/order/del',
                    multi_url: 'mrbs/order/multi',
                    table: 'mrbs/order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        //{field: 'contact', title: __('Contact'),operate:false},
                        {field: 'meet.title', title:'会议主题'},
                        {field: 'user.nickname', title: '发起人'},
                        {field: 'type', title: '会议类型'},
                        {field: 'people', title: __('People'),operate:false},
                        {field: 'starttime', title: __('Starttime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'endtime', title: __('Endtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"-1":__('Status -1')}, formatter: Table.api.formatter.status},
                     /*
                        {field: 'status_text', title: __('Status'),events: Controller.api.events.operate, operate:false,formatter:function(value, row, index){
                            var html = [];
                            if(row.status==0){
                                html.push('<a href="javascript:;" class="btn btn-success btn-pass btn-xs" data-params="status=1">通过</a>');
                                html.push('<a href="javascript:;" class="btn btn-danger btn-refuse btn-xs" data-params="status=-1">拒绝</a>');
                            }else{
                                html.push(value);
                            }
                            return html.join(' ');
                        }},
                        */
                        {field: 'operate', title: __('Operate'), table: table, events: Controller.api.events.operate, formatter: function(value, row, index){
                            var html = [];
                            html.push('<a href="javascript:;" class="btn btn-success btn-editone btn-xs">详情</a>');
                            html.push('<a href="javascript:;" class="btn btn-danger btn-delone btn-xs">删除</a>');
                            return html.join(' ');
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            events: {//绑定事件的方法
                operate: $.extend({
                    'click .btn-pass':function(e, value, row, index){
                        e.stopPropagation();
                        var that = this;
                        console.log($(that).data())
                        var index = Layer.confirm(
                                __('确定要通过该申请吗?'),
                                {icon: 3, title: __('Warning'), shadeClose: true},
                                function () {
                                    Table.api.multi("pass", row['id'], $("#table"), that);
                                    Layer.close(index);
                                }
                        );
                    },
                    'click .btn-refuse':function(e, value, row, index){
                        e.stopPropagation();
                        var that = this;
                        var index = Layer.confirm(
                                __('确定要拒绝该申请吗?'),
                                {icon: 3, title: __('Warning'), shadeClose: true},
                                function () {
                                    Table.api.multi("refuse", row['id'], $("#table"), that);
                                    Layer.close(index);
                                }
                        );
                    }
                },Table.api.events.operate)
            }
        }
    };
    return Controller;
});
