define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'meet.plan/index' + location.search,
                    add_url: 'meet.plan/add',
                    edit_url: 'meet.plan/edit',
                    del_url: 'meet.plan/del',
                    multi_url: 'meet.plan/multi',
                    import_url: 'meet.plan/import',
                    table: 'meet_plan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'date_text', title: __('Date_text'), operate: 'LIKE'},
                        {field: 'week_text', title: __('Week_text'), operate: 'LIKE'},
                        {field: 'fuzeren', title: __('Fuzeren'), operate: 'LIKE'},
                        {field: 'cangsuo', title: __('Cangsuo'), operate: 'LIKE'},
                        {field: 'use_time', title: __('Use_time'), operate: 'LIKE'},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        {field: 'department.name', title: __('Department.name'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
