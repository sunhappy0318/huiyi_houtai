define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'teacher/index' + location.search,
                    add_url: 'teacher/add',
                    edit_url: 'teacher/edit',
                    del_url: 'teacher/del',
                    multi_url: 'teacher/multi',
                    import_url: 'teacher/import',
                    auth_url: 'teacher/auth',
                    table: 'teacher',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                searchFormVisible:false,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'subject', title: __('Subject'), operate: 'LIKE'},
                        {field: 'marital_status', title: __('Marital_status'), operate: 'LIKE'},
                        {field: 'home_address', title: __('Home_address'), operate: 'LIKE'},
                        {field: 'department', title: __('Department'), operate: 'LIKE'},
                        {field: 'gender', title: __('Gender'), operate: 'LIKE'},
                        {field: 'id_number', title: __('Id_number'), operate: 'LIKE'},
                        {field: 'age', title: __('Age'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'native_place', title: __('Native_place'), operate: 'LIKE'},
                        {field: 'birth', title: __('Birth'), operate: 'LIKE'},
                        {field: 'nation', title: __('Nation'), operate: 'LIKE'},
                        {field: 'working_time', title: __('Working_time'), operate: 'LIKE'},
                        {field: 'working_years', title: __('Working_years'), operate: 'LIKE'},
                        {field: 'political_face', title: __('Political_face'), operate: 'LIKE'},
                        {field: 'education', title: __('Education'), operate: 'LIKE'},
                        {field: 'academic_degree', title: __('Academic_degree'), operate: 'LIKE'},
                        {field: 'major', title: __('Major'), operate: 'LIKE'},
                        {field: 'school', title: __('School'), operate: 'LIKE'},
                        {field: 'qualification', title: __('Qualification'), operate: 'LIKE'},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'teaching_subject', title: __('Teaching_subject'), operate: 'LIKE'},
                        {field: 'personnel_category', title: __('Personnel_category'), operate: 'LIKE'},
                        {field: 'first_degree', title: __('First_degree'), operate: 'LIKE'},
                        {field: 'first_degree_school', title: __('First_degree_school'), operate: 'LIKE'},
                        {field: 'highest_education', title: __('Highest_education'), operate: 'LIKE'},
                        {field: 'highest_education_school', title: __('Highest_education_school'), operate: 'LIKE'},
                        {field: 'highest_education_major', title: __('Highest_education_major'), operate: 'LIKE'},
                        {field: 'working_time_sz', title: __('Working_time_sz'), operate: 'LIKE'},
                        {field: 'registered_residence', title: __('Registered_residence'), operate: 'LIKE'},
                        {field: 'three_projects', title: __('Three_projects'), operate: 'LIKE'},
                        {field: 'future_educators', title: __('Future_educators'), operate: 'LIKE'},
                        {field: 'professional_competition', title: __('Professional_competition'), operate: 'LIKE'},
                        {field: 'normal_student', title: __('Normal_student'), operate: 'LIKE'},
                        {field: 'personnel_source', title: __('Personnel_source'), operate: 'LIKE'},
                        {field: 'entry_time', title: __('Entry_time'), operate: 'LIKE'},
                        {field: 'leave_time', title: __('Leave_time'), operate: 'LIKE'},
                        {field: 'emergency_contact', title: __('Emergency_contact'), operate: 'LIKE'},
                        {field: 'relationship', title: __('Relationship'), operate: 'LIKE'},
                        {field: 'emergency_contact_mobile', title: __('Emergency_contact_mobile'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.id', title: __('User.id')},
                        // {field: 'user.mobile', title: __('User.mobile'), operate: 'LIKE'},
                        // {field: 'user.user_type', title: __('User.user_type')},
                        // {field: 'user.username', title: __('User.username'), operate: 'LIKE'},
                        // {field: 'user.group_id', title: __('User.group_id')},
                        // {field: 'user.department_id', title: __('User.department_id'), operate: 'LIKE'},
                        // {field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        // {field: 'user.password', title: __('User.password'), operate: 'LIKE'},
                        // {field: 'user.salt', title: __('User.salt'), operate: 'LIKE'},
                        // {field: 'user.email', title: __('User.email'), operate: 'LIKE'},
                        // {field: 'user.avatar', title: __('User.avatar'), operate: 'LIKE', events: Table.api.events.image, formatter: Table.api.formatter.image},
                        // {field: 'user.level', title: __('User.level')},
                        // {field: 'user.gender', title: __('User.gender')},
                        // {field: 'user.birthday', title: __('User.birthday'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'user.bio', title: __('User.bio'), operate: 'LIKE'},
                        // {field: 'user.money', title: __('User.money'), operate:'BETWEEN'},
                        // {field: 'user.score', title: __('User.score')},
                        // {field: 'user.successions', title: __('User.successions')},
                        // {field: 'user.maxsuccessions', title: __('User.maxsuccessions')},
                        // {field: 'user.prevtime', title: __('User.prevtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.logintime', title: __('User.logintime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.loginip', title: __('User.loginip'), operate: 'LIKE'},
                        // {field: 'user.loginfailure', title: __('User.loginfailure')},
                        // {field: 'user.joinip', title: __('User.joinip'), operate: 'LIKE'},
                        // {field: 'user.jointime', title: __('User.jointime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.createtime', title: __('User.createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.updatetime', title: __('User.updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'user.token', title: __('User.token'), operate: 'LIKE'},
                        // {field: 'user.status', title: __('User.status'), operate: 'LIKE', formatter: Table.api.formatter.status},
                        // {field: 'user.verification', title: __('User.verification'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [{name: 'auth', text: '权限', title: '权限', classname: 'btn btn-xs btn-primary btn-dialog', url: 'teacher/auth'}]}
                    ]
                ]
            });
            // 点击
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        auth: function () {
            $(document).on("change", "#c-switch", function(){
                //开关切换后的回调事件
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
