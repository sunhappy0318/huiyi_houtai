var success = new Vue({
	el: '#success',
	data: {
		info: {
			images: [],
			orderTotal: 0,
			price: 0
		},
		selected: '1',
		tip: false,
		enroll: false,
		user: {
			username: "",
			mobile: ""
		},
		timer:"",
		times:0,
		time:{
			day:0,
			hour:0,
			minute:0,
			second:0
		}
	},
	methods: {
		handleClick: function() {
			this.$toast('Hello world!')
		},
		get: function() {
			var _this = this;
			_this.$indicator.open('加载中...');
			axios.get(api + '/goods?id=' + id).then(function(response) {
				if (response.status == 200 && response.data.code == 1) {
					_this.info = response.data.data;
					// 注册倒计时
					_this.registerCountDown();
					_this.$indicator.close();
				} else {
					_this.$indicator.close();
					console.error('获取失败');
				}
			}).catch(function(error) {
				_this.$indicator.close();
				console.error('服务器异常');
			})
		},
		tipToggle: function() {
			this.tip = !this.tip;
		},
		enrollToggle: function() {
			this.enroll = !this.enroll;
		},
		post: function() {
			var _this = this;
			if (this.user.username.length == 0 || this.user.mobile.length == 0) {
				this.$toast('请输入姓名和联系电话');
				return false;
			}
			if (this.user.mobile.length != 11) {
				this.$toast('手机号长度不正确');
				return false;
			}
			if(!(/^1[3|4|5|6|7|8|9][0-9]\d{4,8}$/.test(this.user.mobile))){
				this.$toast('手机号格式不正确');
				return false;
			}
			this.enrollToggle();
			var params = new URLSearchParams();
			params.append('username', this.user.username);
			params.append('mobile', this.user.mobile);
			params.append('goods_id', this.info.id);
			axios.post(api + '/order', params).then(function(response) {
				if (response.status == 200) {
					_this.$toast(response.data.msg);
					if (response.data.code == 1) {

					} else {

					}
				} else {
					console.error('获取失败');
				}
			}).catch(function(error) {
				console.log(error)
				console.error('服务器异常');
			})
		},
		registerCountDown:function(){
			this.times=new Date(this.info.end_time.replace(/\-/g, "/")).getTime()/1000-Date.now()/1000;
			this.countDown();
		},
		countDown: function() {
			var _this=this;
			_this.timer = setInterval(function() {
				var day = 0,
					hour = 0,
					minute = 0,
					second = 0; //时间默认值
				if (_this.times > 0) {
					day = Math.floor(_this.times / (60 * 60 * 24));
					hour = Math.floor(_this.times / (60 * 60)) - (day * 24);
					minute = Math.floor(_this.times / 60) - (day * 24 * 60) - (hour * 60);
					second = Math.floor(_this.times) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
				}
				if (day <= 9) day = '0' + day;
				if (hour <= 9) hour = '0' + hour;
				if (minute <= 9) minute = '0' + minute;
				if (second <= 9) second = '0' + second;

				_this.time={
					day:day,
					hour:hour,
					minute:minute,
					second:second
				}
				if (_this.times <= 0) {
					clearInterval(_this.timer);
					_this.time={
						day:0,
						hour:0,
						minute:0,
						second:0
					}
				}else{
					_this.times--;
				}
			}, 1000);
		}
	},
	mounted: function() {
		this.get();
	},
	filters: {
		progress: function(value, start, end) {
			if (value >= end) {
				// 已超过本阶段
				return 100;
			} else if (value < start) {
				// 未达到本阶段
				return 0;
			} else {
				// 本阶段
				return value / end * 100;
			}
		}
	},
	computed: {
		price: function() {
			var price = this.info.price;
			for (var i in this.info.roles) {
				var role = this.info.roles[i];
				if (this.info.orderTotal >= role.count) {
					return role.price;
				}
			}
			return price;
		}
	}
})