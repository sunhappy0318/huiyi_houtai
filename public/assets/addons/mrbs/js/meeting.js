var vm = new Vue({
	el: '#app',
	data: {
		list: [],
		status:1
	},
	methods: {
		get: function(status) {
			var _this = this;
			_this.$indicator.open('加载中...');
			if (typeof(status) != 'undefined') {
				_this.status=status;
			}
			var params={status:_this.status,token:token};
			axios.get(root + '/api/order',{params:params}).then(function(response) {
				if (response.status == 200 && response.data.code == 1) {
					_this.list = response.data.data;
					_this.$indicator.close();
				} else {
					_this.$indicator.close();
					console.error('获取失败');
				}
			}).catch(function(error) {
				_this.$indicator.close();
				console.log(error)
				console.error('服务器异常');
			})
		},
	},
	mounted: function() {
		this.get();
	}
})