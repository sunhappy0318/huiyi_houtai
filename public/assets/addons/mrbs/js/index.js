var vm = new Vue({
    el: '#app',
    data() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        return {
            edificeActions: [],
            addressPopup: false,
            edifice: {},
            edifices: [],
            year:year,
            month: month,
            day: day,
            room: [],
            tabbared: 'index',
            startDate:new Date()
        }
    },
    computed: {

    },
    methods: {
        toggleEdifice() {
            this.addressPopup = !this.addressPopup;
        },
        cancel() {
            this.addressPopup = false;
        },
        getEdifice() {
            var _this = this;
            var params = {}
            params.day = this.day
            params.month = this.month
            params.year = this.year
            this.startDate=new Date(this.year+'/'+this.month+'/'+this.day);
            axios.get(root + '/api/edifice', { params: params }).then(function(res) {
                if (res.status == 200 && res.data.code == 1) {
                    _this.edifice = res.data.data[0];
                    if (_this.room.length < 1) {
                        _this.getRoom();
                    }
                    _this.edifices = res.data.data;
                }
            });
        },
        setEdifice(item) {
            this.edifice = item;
            this.cancel();
            this.getRoom();
        },
        setDay(day, week) {
            this.day = day;
            this.week = week;
        },
        getRoom() {
            var _this = this;
            _this.$indicator.open('加载中...');
            var params = {}
            params.edifice_id = this.edifice.id
            params.day = this.day
            params.month = this.month
            params.year = this.year
            axios.get(root + '/api/room', { params: params }).then(function(res) {
                if (res.status == 200 && res.data.code == 1) {
                    _this.room = res.data.data;
                    _this.$indicator.close();
                } else {
                    _this.$indicator.close();
                }
            }).catch(function(error) {
                console.error(error)
                _this.$indicator.close();
            })
        },
        jump(id) {
            window.location.href = root+'/room/show.html?id=' + id + '&day=' + this.day+'&year='+this.year+'&month='+this.month
        },
        openPicker() {
            this.$refs.picker.open();
        },
        selectDate(date) {
            this.month = date.getMonth() + 1;
            this.day = date.getDate();
            this.getEdifice();
        },
    },
    mounted: function() {
        this.getEdifice();
    },
    watch: {
        day() {
            this.getRoom()
        }
    }
})

$(function() {
    var dateTime=new Date();
    var currYear = dateTime.getFullYear();
    var opt = {};
    opt.datetime = {
        preset: 'date'
    };
    opt.default = {
        theme: 'android-ics light', //皮肤样式
        display: 'modal', //显示方式 ：modal（正中央）  bottom（底部）
        mode: 'scroller', //日期选择模式
        dateFormat: 'yyyy-mm-dd', // 日期格式
        lang: 'zh',
        startYear: currYear - 5, //开始年份currYear-5不起作用的原因是加了minDate: new Date()
        endYear: currYear + 5, //结束年份
        minDate: new Date(), //加上这句话会导致startYear:currYear-5失效，去掉就可以激活startYear:currYear-5,
        // maxDate: new Date(dateTime.setDate(dateTime.getDate()+7)),
        onSelect: function(value) {
            var date=new Date(value.split(" ")[0].replace(/\-/g, "/"));
            vm.year = date.getFullYear();
            vm.month = date.getMonth() + 1;
            vm.day = date.getDate();
            var difValue = Math.ceil((date - vm.startDate) / (1000 * 60 * 60 * 24));
            if(difValue>6){
                vm.getEdifice();
            }else{
                vm.getRoom();
            }
        }
    };
    var optDateTime = $.extend(opt['datetime'], opt['default']);
    $("#appDateTime1").mobiscroll(optDateTime).datetime(optDateTime);
});