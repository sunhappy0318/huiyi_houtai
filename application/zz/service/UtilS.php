<?php

namespace app\zz\service;

use Exception;
use ReflectionClass;
use think\Db;
use think\Log;

class UtilS
{
    public static function getConstDocs($class, $prefix): array
    {
        $class = new ReflectionClass($class);
        $arr   = $class->getConstants();
        $arr   = array_flip($arr);
        $arr   = array_filter($arr, function ($v) use ($prefix) {
            return str_starts_with($v, $prefix);
        });
        array_walk($arr, function (&$v) use ($class) {
            $doc = $class->getReflectionConstant($v)->getDocComment();
            if (!$doc) {
                throw new Exception("$v no doc");
            }
            $arr = explode(' ', $class->getReflectionConstant($v)->getDocComment());
            $v   = $arr[1];
        });
        return $arr;
    }

    public static function array2xml($arr): string
    {
        if (!is_array($arr) || count($arr) <= 0) {
            throw new Exception('error xml');
        }
        $xml = '<xml>';
        foreach ($arr as $key => $val) {
            $xml .= is_numeric($val) ? '<' . $key . '>' . $val . '</' . $key . '>' :
                '<' . $key . '><![CDATA[' . $val . ']]></' . $key . '>';
        }
        $xml .= '</xml>';
        return $xml;
    }

    public static function xml2array($xml): array
    {
        if (!$xml) {
            throw new Exception('error xml');
        }
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA), JSON_UNESCAPED_UNICODE), true);
    }

    public static function sign($arr, $key): string
    {
        ksort($arr);
        $str = '';
        foreach ($arr as $k => $v) {
            $str .= $k . "=" . $v . "&";
        }
        $str  .= "key=$key";
        $sign = md5($str);
        return ($sign);
    }

    public static function data_to_xml($params)
    {
        if (!is_array($params) || count($params) <= 0) {
            return false;
        }
        $xml = "<xml>";
        foreach ($params as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        return $xml;
    }

    public static function debug($log, $driver = 'file', $flag = '')
    {
        if ($driver == 'file') {
            Log::init([
                'type'        => 'File',
                'path'        => RUNTIME_PATH,
                'single'      => true,
                'apart_level' => ['debug']
            ]);
            Log::write($log, 'debug');
        } else {
            $table_name = 'zz_debug';
            $exist      = Db::query("show tables like '$table_name'");
            if (!$exist) {
                Db::query("create table $table_name(create_at varchar(255),content text)");
            }
            $time = date('Y-m-d H:i:s');
            if (!is_string($log)) {
                if (is_array($log) && $flag) {
                    $log = ['a' => $flag] + $log;
                    ksort($log);
                }
                $log = json_encode($log, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }
            Db::query("insert $table_name value('$time','$log')");
        }
    }
}
