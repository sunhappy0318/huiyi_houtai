<?php


namespace app\api\model;


use think\Model;

class MeetPlanModel extends Model
{
    // 表名
    protected $name = 'meet_plan';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'department_name',
    ];

    public function getDepartmentNameAttr($var,$data)
    {
        if(!empty($data['department_id'])){
            $row = MeetDepartmentModel::get($data['department_id']);
            return $row?$row['name']:'';
        }else{
            return '';
        }
    }

    //判断会议时间和其他人是否冲突
    public static function isChongtu($params,$id = 0){
        $map = [
            'meet_id' => $params['meet_id'],
            'cangsuo' => $params['cangsuo'],
            'date_text' => $params['date_text'],
        ];
        $list  = self::where($map)->select();
        if(empty($list)){
            return 0;
        }
        $count = count($list);
        if($count <= 1){
            return 0;
        }
        $list = collection($list)->toArray();
        //print_R($list);
        $p = explode('-',$params['use_time']);
        $m1 = 0;
        $s1 = false;
        if(isset($p[0])){
            $m1 = (int)$p[0];
            $s1 = true;
        }
        $m2 = 0;
        $s2 = false;
        if(isset($p[1])){
            $m2 = (int)$p[1];
            $s2 = true;
        }
       /* echo $m1.PHP_EOL;
        echo $params['use_time'].PHP_EOL;
        echo $m2.PHP_EOL;*/

        foreach($list as $v){
            $t = explode('-',$v['use_time']);
            //print_R($t);

            if(isset($t[0]) && isset($t[1]) && $s1 && $v['id'] != $id ){
                $t1 = (int)$t[0];
                $t2 = (int)$t[1];
                if($m1>= $t1 && $m1<=$t2){
                    //echo 'XX'.PHP_EOL;
                    return 1;
                }
            }
            //print_R($v);
            if(isset($t[0]) && isset($t[1]) && $s2 && $v['id'] != $id){
                $t1 = (int)$t[0];
                $t2 = (int)$t[1];
                if($m2 > $t1 && $m2<=$t2){
                    # echo 'XX2'.PHP_EOL;
                    return 1;
                }
            }
        }
        return 0;

        die();
        print_R($list);
        $new = [];
        $i=0;
        foreach($list as $v){
            $t = explode('-',$v['use_time']);
            $t1 = 0;
            $flag1 = false;
            if(isset($t[0])){
                $t1 = (int)$t[0];
                $flag1 = true;
            }
            $t2 = 0;
            $flag2 = false;
            if(isset($t[1])){
                $t2 = (int)$t[1];
                $flag2 = true;
            }
            if($flag1 && $flag2){
                $a1 = range($t1,$t2);
                foreach($a1 as $vc){
                    $new[$i] = $vc;
                    $i++;
                }
            }
        }
        $t = explode('-',$params['use_time']);
        $m1 = 0;
        $s1 = false;
        if(isset($t[0])){
            $m1 = (int)$t[0];
            $s1 = true;
        }
        print_r($new);
        if($s1){
            $mix = min($new);
            $max = max($new);
            if($m1 >= $mix && $m1<$max){
                return 1;
            }
        }
        return 0;
    }
}
