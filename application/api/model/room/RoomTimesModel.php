<?php
namespace app\api\model\room;

use traits\model\SoftDelete;
use think\Model;

class RoomTimesModel extends Model
{

    // 表名
    protected $name = 'mrbs_room_times';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';


    // 追加属性
    protected $append = [
      //  'state_text'
    ];

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }


    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }
}
