<?php

namespace app\api\model\room;

use app\api\model\MeetModel;
use app\api\model\MeetUserModel;
use traits\model\SoftDelete;
use think\Model;

class RoomEdificeModel extends Model
{

    // 表名
    protected $name = 'mrbs_edifice';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';


    // 追加属性
    protected $append = [
        //  'state_text'
    ];

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }
}
