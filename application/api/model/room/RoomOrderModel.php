<?php
namespace app\api\model\room;

use app\api\controller\Wechat;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetUserModel;
use traits\model\SoftDelete;
use think\Model;

class RoomOrderModel extends Model
{

    // 表名
    protected $name = 'mrbs_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';


    // 追加属性
    protected $append = [
      //  'state_text'
    ];

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }

// 预定
    public static  function saveData($input = [])
    {
        $room = RoomModel::get($input['room_id']);
        if(!$room){
            return ['status'=>0,'msg'=>'会议室不存在'];
        }
        $input['status'] = 0;
        $input['starttime'] = strtotime($input['start_time']);
        $input['endtime'] = strtotime($input['end_time']);
        if ($input['endtime'] < time()) {
            return ['status'=>0,'msg'=>'时间无效'];
        }
        $input['hour'] =  ($input['endtime'] -$input['starttime'] ) / 3600;
        $model = new RoomOrderModel($input);
        $row = $model->allowField(true)->save();
        $res = [];
        if($row){
            $touser =   MeetUserModel::where( "id", "in",$room["manager_id"] )->select();
            $toArr =    array_column($touser, 'username');
            //给管理员发消息
            $news['user_id'] = $room["manager_id"];
            $news['from_id'] = $input['user_id']  ;
            $news['meet_id'] = $input['meet_id'];
            $news['room_id'] = $room['id'];
            $news['link_id'] = $input['meet_id'];
            /*
            $news['title'] = $input['nickname'].'预定会议室';
            $news['content'] = '预定'.$room["title"].'，时间为：'.date("Y-m-d H:i",$input['starttime']).' 至'.date("Y-m-d H:i",$input['endtime']).'，请请前往审核';
            $news['type'] = "room";
            $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
            $news['user'] = implode(',',$toArr);
            $news['start_time'] = trim($input['start_time']);
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'title' =>$news['title'],
                'description' => $news['content'],
                'url' => 'https://huiyi.bigchun.com/api/wechat/jump?type=room&link_id='.$news['link_id']
            ];
            $res =  Wechat::sendMessage($arr,$toArr);
            */
        }
        return ['status'=>1,'msg'=>'预定成功'];
    }


    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }
}
