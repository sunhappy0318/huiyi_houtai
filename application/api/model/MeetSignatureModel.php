<?php

namespace app\api\model;

use app\api\controller\MeetRoom;
use app\api\model\room\RoomModel;
use app\api\model\room\RoomOrderModel;
use app\api\model\room\RoomWeekModel;
use traits\model\SoftDelete;
use think\Model;

class MeetSignatureModel extends Model
{

    use SoftDelete;

    // 表名
    protected $name = 'meet_signature';


    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        //  'state_text'
    ];


    public function getFilesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

}
