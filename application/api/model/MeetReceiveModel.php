<?php
namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class MeetReceiveModel extends Model
{
    use SoftDelete;
    // 表名
    protected $name = 'meet_receive';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        //  'state_text'
    ];

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }

    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }
}
