<?php

namespace app\api\model;

use think\Model;

class MeetM extends Model
{
    protected $table  = 'fa_meet';
    protected $append = [
        'append_topic_submit_off_time'
    ];

    public function getAppendTopicSubmitOffTimeAttr($value, $data)
    {
        $value = $data['topic_submit_off_time'] ?? '';
        if ($value) {
            $value = date('Y-m-d H:i:s', $value);
        }
        return $value;
    }
}
