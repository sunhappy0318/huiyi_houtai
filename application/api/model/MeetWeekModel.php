<?php


namespace app\api\model;


use think\Model;

class MeetWeekModel extends Model
{
    // 表名
    protected $name = 'meet_week';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'department_name',
    ];

    public function getFilesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return '';
    }


    public function getDepartmentNameAttr($var,$data)
    {
        $row = MeetDepartmentModel::get($data['department_id']);
        return $row?$row['name']:'';
    }
}
