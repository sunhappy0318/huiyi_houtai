<?php
namespace app\api\model;

use traits\model\SoftDelete;
use think\Model;

class MeetUserModel extends Model
{

    // 表名
    protected $name = 'user';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';


    // 追加属性
    protected $append = [
      //  'state_text'
    ];

  /*  public function getAvatarAttr($val,$data)
    {
        if(!empty($data['nickname'])){
            return letter_avatar2($data['nickname']);
        }
        return $val;
    }*/

    public static function getUser($user_id){
        $model = MeetUserModel::where( "id", "in",$user_id)->field("id,nickname,username,avatar")->find();
        if($model){
            return $model;
        }
        return [
            'nickname' => '',
            'username' => '',
            'avatar' => '',
        ];
    }

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }


    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }
}
