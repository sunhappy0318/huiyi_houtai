<?php

namespace app\api\model;

use think\Model;
use think\model\relation\BelongsTo;

class CollectorM extends Model
{
    protected $table = 'fa_collector';

    public function profile(): BelongsTo
    {
        return $this->belongsTo(UserM::class, 'user_id', 'id');
    }
}