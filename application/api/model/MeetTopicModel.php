<?php
namespace app\api\model;

use app\api\controller\Wechat;
use app\api\model\room\RoomModel;
use app\api\model\room\RoomOrderModel;
use app\common\library\Auth;
use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;

class MeetTopicModel extends Model
{
    //use SoftDelete;
    // 表名
    protected $name = 'meet_topic';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
      //  'state_text'
    ];

   /* public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }*/

    public function getFilesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            $list = json_decode(    $val,true);
            foreach($list as &$v){
                $img = ['png','jpg','jpeg','bmp','gif'];
                if(empty($v['fullurl'])){
                    return [];
                }
                $ext = pathinfo($v['fullurl'],PATHINFO_EXTENSION);
                $v['is_img'] = in_array($ext,$img)?1:0;
            }
            return $list;
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

    public function getImagesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

    public static function detail($input)
    {
        $info = self::where('id', $input['id'])
            ->field('*')
            ->find();
        $info['images'] = json_decode(   str_replace('&quot;','"', $info['images']),true);
        $info['files'] = json_decode(   str_replace('&quot;','"', $info['files']),true);
        $info['user']  = MeetUserModel::where( "id", "in",$info['user_id']  )->field("id,nickname,username,avatar")->find();
        $info['createtime'] = date("Y-m-d H:i:s",$info['createtime']);

        $check_list =  MeetCheckModel::alias('a')->field(" a.status,a.content,a.files,a.createtime,a.user_id" )
           // ->join(  ' fa_user ', 'a.user_id = fa_user.id', 'left')
            ->where("type = 'topic' and topic_id =" . $info['id']   )->select();
        foreach ( $check_list as $k => &$val) {
            $val['files'] = json_decode(   str_replace('&quot;','"', $val['files']),true);
            $val['status_name'] = $val['status'] ?"同意":"不同意";
            $val['createtime'] = date("Y-m-d H:i:s",$val['createtime']);
            $val['user']  = MeetUserModel::where( "id", "in",$val['user_id']  )->field("id,nickname,username,avatar")->find();

        }
        $info['check_list'] = $check_list;


        $vote_list =  MeetCheckModel::alias('a')->field(" a.status,a.content,a.files,a.createtime,a.user_id" )
           // ->join(  ' fa_user ', 'a.user_id = fa_user.id', 'left')
            ->where("type = 'vote' and topic_id =" . $info['id']   )->select();
        foreach ( $vote_list as $k => &$val) {
            $val['files'] = json_decode(   str_replace('&quot;','"', $val['files']),true);
            $val['status_name'] = $val['status'] ?"同意":"不同意";
            $val['createtime'] = date("Y-m-d H:i:s",$val['createtime']);
            $val['user']  = MeetUserModel::where( "id", "in",$val['user_id']  )->field("id,nickname,username,avatar")->find();
        }
        $info['vote_list'] = $vote_list;
        return $info;
    }


// 保存数据
    public static  function saveTopicData($input = [])
    {
        $meet = MeetModel::get($input['meet_id']);
        if(!$meet){
            return ['status'=>0,'msg'=>'会议不存在'];
        }

            $touser =   MeetUserModel::where( "id", "in",$meet["check_ids"] )->select();
            $toArr =    array_column($touser, 'username');
            //给审批人发消息
            $news['user_id'] = $meet["check_ids"];
            $news['from_id'] = $input['user_id']  ;
            $news['meet_id'] = $meet['id'];
            $news['room_id'] = $meet['room_id'];
            $news['topic_id'] = $input['topic_id'];
            $news['link_id'] = $input['topic_id'];
            $news['title'] = $input['nickname'].'提交了会议议题';
            $news['content'] = '提交了'.$meet["title"].'议题：'.$input["content"].' ，请请前往审核';
            $news['type'] = "topic";
            $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
            $news['user'] = implode(',',$toArr);
            $news['start_time'] = date("Y-m-d H:i:s");
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'title' =>$news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-3-detial/topicDetails/topicDetails?id='.$meet['id'],
            ];
            $res =  Wechat::sendMessage($arr,$toArr);

        return ['status'=>1,'msg'=>'预定成功'];
    }

    public static function getUserlist3($map,$user,$model)
    {
        $list = self::where($map)->order('is_top desc,updatetime desc')
            ->where('status',2)
            ->select();
        if(empty($list)){
            return $list;
        }
        foreach($list as $key=>&$item){
            $item['topic_id'] = $item['id'];
            $item['createtime'] = date('m-d H:i',$item['createtime']);
            $item['user'] = MeetUserModel::where("id", "in", $item['user_id'])->field("id,nickname,username,avatar")->find();
            $item['is_faqi_ren'] = $model['is_faqi'];
            $check_type = 'vote'; //查询参与人表决记录
            $checkList= MeetCheckModel::where("topic_id", "in", $item['id'])
                ->where('type',$check_type)
                ->field("id,status,user_id,content,files,createtime")
                ->order("id desc")
                ->select();
            foreach ( $checkList  as $k => &$val) {
                $suser = \app\common\model\User::get($val['user_id']);
                $username = $suser?$suser['nickname']:'';
                $check_status = $val['status'] == 1?'通过':'驳回';
                $val['check_text'] = $username.$check_status;
                $val['check_status'] = $val['status'];
                $val['nickname'] = $username;
                $val['status_name'] = $val['status'] == 1?"通过":"不通过";
            }
            $item['check'] = $checkList;
            $map = [
                'user_id' => $user['id'],
                'type' =>$check_type,
                'topic_id' => $item['id'],
            ];
            $row = MeetCheckModel::where($map)
                ->field("id")
                ->find();
            //参与人对议题今夕表决 1= 未表决 0= 已表决
            $item['is_check'] = $row?0:1;
            $item['is_show'] = 0;
            if($model['status'] == 8){
                $item['is_show'] = 1;
            }
            $item['status_name'] = getTopicStatusName($item['status']);
        }
        return $list;
    }

    public static function getUserlist($map,$user,$model)
    {
        $list = self::where($map)->order('is_top desc,updatetime desc')
            ->where('status',1)
            ->select();
        if(empty($list)){
            return $list;
        }
        foreach($list as $key=>&$item){
            $item['topic_id'] = $item['id'];
            $item['createtime'] = date('m-d H:i',$item['createtime']);
            $item['user'] = MeetUserModel::where("id", "in", $item['user_id'])->field("id,nickname,username,avatar")->find();
            $item['is_faqi_ren'] = $model['is_faqi'];
            $check_type = 'vote'; //查询参与人表决记录
            $checkList= MeetCheckModel::where("topic_id", "in", $item['id'])
                ->where('type',$check_type)
                ->field("id,status,user_id,content,files,createtime")
                ->order("id desc")
                ->select();
            foreach ( $checkList  as $k => &$val) {
                $suser = \app\common\model\User::get($val['user_id']);
                $username = $suser?$suser['nickname']:'';
                $check_status = $val['status'] == 1?'通过':'驳回';
                $val['check_text'] = $username.$check_status;
                $val['check_status'] = $val['status'];
                $val['nickname'] = $username;
                $val['status_name'] = $val['status'] == 1?"通过":"不通过";
            }
            $item['check'] = $checkList;
            $map = [
                'user_id' => $user['id'],
                'type' =>$check_type,
                'topic_id' => $item['id'],
            ];
            $row = MeetCheckModel::where($map)
                ->field("id")
                ->find();
            //参与人对议题今夕表决 1= 未表决 0= 已表决
            $item['is_check'] = $row?0:1;
            $item['is_show'] = 0;
            if($model['status'] == 8){
                $item['is_show'] = 1;
            }
            $item['status_name'] = getTopicStatusName($item['status']);
        }
        return $list;
    }

    public static function getUserlist2($map)
    {
        $list = self::where($map)
            ->where('status',1)
            ->order('is_top desc,updatetime desc')
            ->select();
        if(empty($list)){
            return $list;
        }
        foreach($list as $key=>&$item){
            $item['topic_id'] = $item['id'];
            $item['createtime'] = date('m-d H:i');
            $item['user'] = MeetUserModel::where("id", "in", $item['user_id'])->field("id,nickname")->find();
          /*  $check_type = 'topic'; //查询参与人表决记录
            $check = MeetCheckModel::where("topic_id", "in", $item['id'])
                ->where('type',$check_type)
                ->field("id,status,user_id,content,files,createtime")
                ->order("id desc")
                ->find();
            $nickname = '';
            if($check){
                $cuser = User::get($check['user_id']);
                $nickname = $cuser?$cuser['nickname']:'';
            }
            $item['check_nickname'] = $nickname;*/

            $check_type = 'vote'; //查询参与人表决记录
            $checkList= MeetCheckModel::where("topic_id", "in", $item['id'])
                ->where('type',$check_type)
                ->field("id,status,user_id,content,createtime")
                ->order("id desc")
                ->select();
            $check_status = '';
            foreach($checkList as $v){
                $suser = User::get($v['user_id']);
                $nickname = $suser?$suser['nickname']:'';
                if($v['status'] == 1){
                    $check_status .= $nickname.' 通过;';
                }else{
                    $check_status .= $nickname.' 不通过 '.$v['content'].';';
                }
            }
            $item['check_status'] = $check_status;
        }
        return $list;
    }
}
