<?php
namespace app\api\model;

use app\common\library\Auth;
use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;

class MeetSummaryModel extends Model
{
    use SoftDelete;
    // 表名
    protected $name = 'meet_summary';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
      //  'state_text'
    ];

    public function getFilesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

    public function getImagesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

    public static function detailByMeetId($id)
    {
        $info = self::where('meet_id', $id)
            ->field('*')
            ->find();
        if(!$info){
            return $info;
        }
        $list = MeetCheckModel::where('summary_id',$info['id'])->select();
        $list = collection($list)->toArray();
        foreach($list as &$v){
            $text = '';
            if($v['status'] == 1){
                $text = '已通过';
            }
            if($v['status'] == 2){
                $text = '待审核';
            }
            if($v['status'] == 0){
                $text = '被驳回';
            }
            $v['status_text'] = $text;
        }
        $user = User::get($info['user_id']);
        if(empty($list)){
            $list = [
                [
                    'status' => 2,
                    'check_text' => '待审核',
                    'status_text' => '待审核',
                    'content' => '',
                    'createtime_text'=>date('Y-m-d H:i',$info['createtime']),
                    'createtime' => date('Y-m-d H:i',$info['createtime'])
                ]
            ];
        }
        if($info['status'] == 2){
            array_push ($list,['status' => 0,
                'check_text' => '已经提交,待审核',
                'status_text' => '已经提交,待审核',
                'content' => '',
                'createtime_text'=>'',
                'createtime' => '']);
        }
        $info['user'] = [
            'avatar' => $user['avatar'],
            'nickname' => $user['nickname']
        ];
        //print_r($list);die();
        $info['check_list'] = $list;
        return $info;
    }

    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        if(empty($info)){
            return $info;
        }
     /*   $info['createtime_text'] = date('Y-m-d H:i',$info['createtime']);
        $text1 = !empty($info['bohui_time'])?date('Y-m-d H:i',$info['bohui_time']):'';
        $text2 = !empty($info['tongguo_time'])?date('Y-m-d H:i',$info['tongguo_time']):'';*/
        //$info['bohui_time_text'] = $text1;
        //$info['tongguo_time_text'] = $text2;
        $list = MeetCheckModel::where('summary_id',$id)->select();
        $list = collection($list)->toArray();
        //print_r($list);die();
        $info['check_list'] = $list;
        return $info;
    }

    public static function getCateByTypeId($type_id)
    {

        $map = [
            'state' => 1,
            'store_type_id'=>$type_id,
        ];
        $cate =  MeetModel::where($map)
            ->field('id as store_cate_id,name,logo_image')
            ->whereNull('deletetime')
            ->select();
        return $cate;
    }

}
