<?php


namespace app\api\model;


use think\Model;

class TopicTopModel extends Model
{
    // 表名
    protected $name = 'meet_topic_top';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public static function saveUser($meet,$user)
    {
        $map = [
            'meet_id' => $meet['id'],
        ];
        if($meet['is_yiti'] == 1 && $meet['is_canhui'] == 0 && $meet['is_faqi'] == 0 && $meet['is_xiaozhang'] == 0){
            $map += [
                'user_id' => $user['id'],
            ];
        }
        $list = self::where($map)->order('is_top desc,updatetime desc')->select();
        if($list){
            return collection($list)->toArray();
        }
        $model = new self();
        $insert_list = [];
        $topic_user_ids = $meet['user_id'].','.$meet['attend_topic_ids'];
        $topic_user_ids = explode(',',$topic_user_ids);
        $topic_user_ids = array_unique($topic_user_ids);
        //print_r($topic_user_ids);die();
        foreach($topic_user_ids as $k=>$v){
            $insert_list[$k] = [
                'meet_id' => $meet['id'],
                'user_id' => $v,
            ];
        }
        $model->saveAll($insert_list);
        $list = self::where($map)->select();
        $list = collection($list)->toArray();
        return $list;

    }

    public static function saveUser2($meet)
    {
        $map = [
            'meet_id' => $meet['id'],
        ];
        $list = self::where($map)->order('is_top desc,updatetime desc')->select();
        if($list){
            return collection($list)->toArray();
        }
        $model = new self();
        $insert_list = [];
        $topic_user_ids = $meet['user_id'].','.$meet['attend_topic_ids'];
        $topic_user_ids = explode(',',$topic_user_ids);
        $topic_user_ids = array_unique($topic_user_ids);
        //print_r($topic_user_ids);die();
        foreach($topic_user_ids as $k=>$v){
            $insert_list[$k] = [
                'meet_id' => $meet['id'],
                'user_id' => $v,
            ];
        }
        $model->saveAll($insert_list);
        $list = self::where($map)->select();
        $list = collection($list)->toArray();
        return $list;

    }
}
