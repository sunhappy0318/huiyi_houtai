<?php

namespace app\api\model;

use think\Model;
use think\model\relation\BelongsTo;

class TopicM extends Model
{
    const CHECKING = 0;
    const CHECKED  = 1;

    const STATUS_NORMAL = 1;

    protected $table = 'fa_meet_topic';

    public function user(): BelongsTo
    {
        return $this->belongsTo(UserM::class, 'user_id', 'id');
    }
}