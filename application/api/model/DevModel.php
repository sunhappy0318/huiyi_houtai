<?php


namespace app\api\model;


class DevModel extends MeetModel
{
// 表名
    protected $name = 'dev';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

}
