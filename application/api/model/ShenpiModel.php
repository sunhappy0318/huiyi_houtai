<?php

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class ShenpiModel extends Model
{
    // 表名
    protected $name = 'meet_shenpi';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 追加属性
    protected $append = [

    ];

     public static function getShenpiByUserId($userId,$date)
    {
        $info = self::where('user_id', $userId)->where('date_string',$date)
            ->field('*')
            ->find();
        return $info;
    }
}
