<?php


namespace app\api\model;


use think\Model;

class SignModel extends Model
{
    // 表名
    protected $name = 'sign';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

}
