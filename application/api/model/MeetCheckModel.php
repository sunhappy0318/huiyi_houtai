<?php

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class MeetCheckModel extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'meet_check';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        //  'state_text'
        'createtime_text',
    ];

    public function getCreatetimeTextAttr($val,$data)
    {
        if(is_numeric($data['createtime'])){
            return date('Y-m-d H:i',$data['createtime']);
        }
        if(is_numeric($data['createtime']) && !empty($data['createtime'])){
            return date('Y-m-d H:i',strtotime($data['createtime']));
        }
        return '';
    }

    public function getFilesAttr($val)
    {
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }

    public static function lists($param)
    {
        /*
 *  `attend_meet_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '参会人员ID',
  `attend_topic_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '议题参与人ID',
  `approver_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '审批人ID',
  `issued_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '签发人ID',
  `check_ids`
*/
        $limit = $param['limit'] ? $param['limit'] : '15';
        $map = [];
        if (!empty($param['status'])) {
            $map = [
                'status' => $param['status'],
            ];
        }
        if (!empty($param['type'])) {
            $map['type'] = $param['type'];
        }
        if (!empty($param['user_id'])) {
            $map['user_id'] = array('in', explode(",", $param['user_id']));
        }
        $list = MeetCheckModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        return $list;
    }

    public static function detail($id)
    {
        $info = MeetTypeModel::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }

    public static function getTimeCheck($type ="meet", $meet_id,  $createtime)
    {
        $list = MeetCheckModel::where('meet_id', $meet_id)->where('type', $type)->field('content,status,createtime')->order("id desc ")->select();
        foreach ($list as $k => &$val) {
            $val['status_name'] = $val['status'] ? '已通过' : '被驳回';
            $val['createtime'] =  date("Y-m-d H:i:s",$val['createtime']);
        }
       $arr = collection($list)->toarray();;
        $arr[count($arr)] =  ['createtime'=>$createtime,"content"=> "",'status_name'=>"提交审核",'status'=>0];

        return $arr;
    }


    public static function checkPermission($type, $userId)
    {

    }


}
