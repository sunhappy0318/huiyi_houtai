<?php

namespace app\api\model;

use app\api\controller\Wechat;
use app\api\model\room\RoomModel;
use think\Model;
use traits\model\SoftDelete;

class MeetNoticeModel extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'meet_notice';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        //  'state_text'
    ];

    public static function lists($param)
    {
        /*
 *  `attend_meet_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '参会人员ID',
  `attend_topic_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '议题参与人ID',
  `approver_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '审批人ID',
  `issued_ids` longtext COLLATE utf8mb4_unicode_ci COMMENT '签发人ID',
  `check_ids`
*/
        $limit = $param['limit'] ? $param['limit'] : '15';
        $map = [];
        if (!empty($param['status'])) {
            $map = [
                'status' => $param['status'],
            ];
        }
        if (!empty($param['type'])) {
            $map['type'] = $param['type'];
        }
        if (!empty($param['user_id'])) {
            $map['user_id'] = array('in', explode(",", $param['user_id']));
        }
        $list = MeetCheckModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        return $list;
    }

    public static function detail($id)
    {
        $info = MeetTypeModel::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }


    public static function sendRoomOrderNotice($input, $type, $data)
    {
        $news = [];
        $room = RoomModel::get($input['room_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $input['meet_id'];
        $news['room_id'] = $input['room_id'];
        $news['link_id'] = $input['meet_id'];
        $meetModel = MeetModel::get($input['meet_id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;
        // 发起人、参与人员、通过的议题参与人员、校长
        $start_time = date("m月d日", strtotime($input['start_time'])) . "  " . getWeekName($input['start_time']) . "  " . date("H:i", strtotime($input['start_time'])) . "-" . date("H:i", strtotime($input['end_time']));

        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

        //给会议室管理员发消息
        $touser = MeetUserModel::where("id", "in", $input['check_room_ids'])->select();
        $toArr = array_column($touser, 'username');
        $news['user_id'] = $input['check_room_ids'];
        $news['user'] = implode(',', $toArr);
        $news['title'] = "审核会议室：" . $input['title'];
        $news['content'] = '' . getTypeName($input['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;
        $modelN = new MeetNoticeModel($news);
        $modelN->allowField(true)->save();
        $arr = [
            'title' => $news['title'],
            'description' => $news['content'],
            'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id'],
            'meet_id' => $input['meet_id']
        ];
        $res = Wechat::sendMessage($arr, $toArr);
        return ['status' => 1, 'msg' => '发起会议成功'];
    }


    public static function sendRoomCancelNotice($input, $type, $data)
    {
        $news = [];
        $room = RoomModel::get($input['room_id']);
        $input['meet_id'] = $input['id'];
        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $input['meet_id'];
        $news['room_id'] = $input['room_id'];
        $news['link_id'] = $input['meet_id'];

        $meetModel = MeetModel::get($input['meet_id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;

        // 发起人、参与人员、通过的议题参与人员、校长
        $start_time = date("m月d日", strtotime($input['start_time'])) . "  " . getWeekName($input['start_time']) . "  " . date("H:i", strtotime($input['start_time'])) . "-" . date("H:i", strtotime($input['end_time']));

        $news['type'] = $type;
        $news['status'] = 0; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        //$news['start_time'] = date("Y-m-d H:i:s");

        //给会议室管理员发消息
        $touser = MeetUserModel::where("id", "in", $input['attend_meet_ids'])->select();
        $toArr = array_column($touser, 'username');
        $news['user_id'] = $input['attend_meet_ids'];
        $news['user'] = implode(',', $toArr);
        $news['title'] = "取消会议室：" . $input['title'];
        $news['content'] = '' . getTypeName($input['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;
        $modelN = new MeetNoticeModel($news);
        $modelN->allowField(true)->save();
        $arr = [
            'meet_id' => $input['meet_id'],
            'title' => $news['title'],
            'description' => $news['content'],
            'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id'],
        ];
        $res = Wechat::sendMessage($arr, $toArr);
        return ['status' => 1, 'msg' => '取消成功'];
    }

    public static function sendRoomCheckNotice($input, $type, $meet)
    {
        $news = [];
        $room = RoomModel::get($meet['room_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $input['meet_id'];
        $news['room_id'] = $meet['room_id'];
        $news['link_id'] = $input['meet_id'];
        $meetModel = MeetModel::get($input['meet_id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;
        // 发起人、参与人员、通过的议题参与人员、校长
        $start_time = date("m月d日", strtotime($meet['start_time'])) . "  " . getWeekName($meet['start_time']) . "  " . date("H:i", strtotime($meet['start_time'])) . "-" . date("H:i", strtotime($meet['end_time']));

        $news['type'] = $type;
        $news['status'] = isset($input['status'])?$input['status']:0; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

        //审核通过 ，发消息给  ：发起人、参与人员、通过的议题参与人员、校长
        if ($input['status'] == 1) {

            $ids_arr = $meetModel['user_id'];
            //给发布人发消息
            //给所有会议参与人发审核通过消息
            $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $meetModel['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "会议审核通过：" . $meet['title'];
            $map = [
                'type' => $type,
                'link_id' => $news['link_id'],
                'status' => 1
            ];
            $row = MeetNoticeModel::where($map)->find();
            if($row){
                $news['title'] = "会议变更通知：" . $meet['title'];
            }
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $input['meet_id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);


            $meet = MeetModel::get($input['meet_id']);
            $news['type'] = 'meet_notice';
            $news['status'] = $meet['status']; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
            $news['start_time'] = date("Y-m-d H:i:s");
            //参会人发通知
            $ids_arr = MeetModel::getAllMeetUserId3($meet['id']);
            //给自己发消息
            $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = implode(',',$ids_arr);
            $news['user'] = implode(',', $toArr);
            $news['title'] = "参加会议提醒：" . $meet['title'];
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;

            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();

            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);

        } else {
            //会议室审核失败
            //给自己发消息
            $touser = MeetUserModel::where("id", "in", $meet['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $meet['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "会议室审核未通过：" . $meet['title'];
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time . " 驳回理由：" . $input['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $input['meet_id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        }
        return ['status' => 1, 'msg' => '审核会议室成功'];
    }


    public static function sendRoomCheckNotice2($input, $type, $meet)
    {
        $news = [];
        $room = RoomModel::get($meet['room_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $meet['id'];
        $news['room_id'] = $meet['room_id'];
        $news['link_id'] = $meet['id'];
        $meetModel = MeetModel::get($meet['id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;
        // 发起人、参与人员、通过的议题参与人员、校长
        $start_time = date("m月d日", strtotime($meet['start_time'])) . "  " . getWeekName($meet['start_time']) . "  " . date("H:i", strtotime($meet['start_time'])) . "-" . date("H:i", strtotime($meet['end_time']));

        $news['type'] = $type;
        $news['status'] = $meet['status']; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");
        //党总支会议，发给所有人
        $ids_arr = MeetModel::getAllMeetUserId2($meet['id']);
        //给自己发消息
        $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
        $toArr = array_column($touser, 'username');
        $news['user_id'] = implode(',',$ids_arr);
        $news['user'] = implode(',', $toArr);
        $news['title'] = "参加会议提醒：" . $meet['title'];
        $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;
        $modelN = new MeetNoticeModel($news);
        $modelN->allowField(true)->save();
        $arr = [
            'meet_id' => $meet['id'],
            'title' => $news['title'],
            'description' => $news['content'],
            'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
        ];
        $res = Wechat::sendMessage($arr, $toArr);

        return ['status' => 1, 'msg' => '发送成功'];
    }
    public static function sendMeetCheckNotice($input, $type, $meet)
    {
        $news = [];
        $room = RoomModel::get($meet['room_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $input['meet_id'];
        $news['room_id'] = $meet['room_id'];
        $news['link_id'] = $input['meet_id'];
        $meetModel = MeetModel::get($input['meet_id']);
        $meet = $meetModel;
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;
        // 发起人、参与人员、通过的议题参与人员、校长
        $start_time = date("m月d日", strtotime($meet['start_time'])) . "  " . getWeekName($meet['start_time']) . "  " . date("H:i", strtotime($meet['start_time'])) . "-" . date("H:i", strtotime($meet['end_time']));

        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

        //审核通过 ，发消息给  ：发起人
        if ($input['status'] == 1) {
            //$ids_arr = MeetModel::getAllMeetUserId($input['meet_id']);
            $ids_arr = $meetModel['user_id'];
            //给发布人发消息
            //给所有会议参与人发审核通过消息
            $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $meetModel['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "会议审核通过：" . $meet['title'];
            $map = [
                'type' => $type,
                'link_id' => $news['link_id'],
                'status' => 1
            ];
            $row = MeetNoticeModel::where($map)->find();
            if($row){
                $news['title'] = "会议变更通知：" . $meet['title'];
            }
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $input['meet_id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);


            $meet = MeetModel::get($input['meet_id']);
            $news['type'] = 'meet_notice';
            $news['status'] = $meet['status']; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
            $news['start_time'] = date("Y-m-d H:i:s");
            //参会人发通知
            $ids_arr = MeetModel::getAllMeetUserId3($meet['id']);
            //给自己发消息
            $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = implode(',',$ids_arr);
            $news['user'] = implode(',', $toArr);
            $news['title'] = "参加会议提醒：" . $meet['title'];
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time;

            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();

            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);

        } else {
            //会议室审核失败  //给发布人发消息
            $touser = MeetUserModel::where("id", "in", $meet['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $meet['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "会议审核未通过：" . $meet['title'];
            $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time . " 驳回理由：" . $input['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $input['meet_id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        }
        return ['status' => 1, 'msg' => '审核会议成功'];
    }

    public static function sendTopicAddNotice($input, $type, $meet)
    {
        $news = [];
        $topic = MeetTopicModel::get($input['topic_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $topic['meet_id'];
        $news['room_id'] = $meet['room_id'];
        $news['topic_id'] = $input['topic_id'];
        $news['link_id'] = $input['topic_id'];

        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

            //给审核人发消息
            $touser = MeetUserModel::where("id", "in", $meet['topic_collector_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $meet['topic_collector_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "提交了议题";
            $news['content'] =  $topic['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $topic['meet_id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/topics/shoujiTopics?id='.$topic['meet_id'],

            ];
            $res = Wechat::sendMessage($arr, $toArr);

        return ['status' => 1, 'msg' => '发布了议题'];
    }

    public static function sendTopicCheckNotice($input, $type, $meet)
    {

        $news = [];
        $topic = MeetTopicModel::get($input['topic_id']);
        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $meet['id'];
        $news['room_id'] = $meet['room_id'];
        $news['topic_id'] = $topic['id'];
        $news['link_id'] = $input['check_id'];

        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");


        //审核通过 ，发消息给  ：发起人
        if ($input['status'] == 1) {
            //给发布人发消息
            $touser = MeetUserModel::where("id", "in", $topic['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $topic['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "议题已通过";
            $news['content'] =  $topic['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/topics/topics?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        } else {
            //会议室审核失败  //给发布人发消息
            $touser = MeetUserModel::where("id", "in", $topic['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $topic['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "议题未通过：" . $topic['content'];
            $news['content'] =   "驳回理由：" . $input['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/topics/topics?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        }
        return ['status' => 1, 'msg' => '审核议题成功'];
    }

    public static function sendSummaryAddNotice($input, $type, $meet)
    {
        $news = [];
        $summary = MeetSummaryModel::get($input['summary_id']);

        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $summary['meet_id'];
        $news['room_id'] = $meet['room_id'];
        $news['summary_id'] = $input['summary_id'];
        $news['link_id'] = $input['summary_id'];

        $meetModel = MeetModel::get($summary['meet_id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;

        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

        //给审核人发消息
        $touser = MeetUserModel::where("id", "in", $meet['issued_ids'])->select();
        $toArr = array_column($touser, 'username');
        $news['user_id'] = $meet['issued_ids'];
        $news['user'] = implode(',', $toArr);
        $news['title'] = "提交了会议纪要";
        $news['content'] =  $summary['content'];
        $modelN = new MeetNoticeModel($news);
        $modelN->allowField(true)->save();
        $arr = [
            'meet_id' => $summary['meet_id'],
            'title' => $news['title'],
            'description' => $news['content'],
            'url' => request()->domain().'/#/pages/tabbar-3-detial/meetingMinutes/meetingMinutes?id='.$summary['meet_id'],
        ];
        $res = Wechat::sendMessage($arr, $toArr);

        return ['status' => 1, 'msg' => '发布了会议纪要'];
    }

    public static function sendSummaryCheckNotice($input, $type, $meet)
    {

        $news = [];
        $summary = MeetSummaryModel::get($input['summary_id']);
        $news['from_id'] = $input['user_id'];
        $news['meet_id'] = $meet['id'];
        $news['room_id'] = $meet['room_id'];
        $news['topic_id'] = $summary['id'];
        $news['link_id'] = $input['check_id'];

        $meetModel = MeetModel::get($meet['id']);
        $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;

        $room = RoomModel::get($meet['room_id']);
        $start_time = date("m月d日", strtotime($meet['start_time'])) . "  " . getWeekName($meet['start_time']) . "  " . date("H:i", strtotime($meet['start_time'])) . "-" . date("H:i", strtotime($meet['end_time']));

        $news['title'] = "会议纪要审核：" . $meet['title'];
        $news['content'] = '' . getTypeName($meet['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " " . $start_time." "  . $summary['content'] ;
        $news['type'] = $type;
        $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");
        $res = [];

        //审核通过 ，发消息给  ：参与人员、通过的议题参与人员、校长
        if ($input['status'] == 1) {
            //给校长发消息
            $touser = MeetUserModel::where("id", "in", $meet['check_ids'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] =  $meet['check_ids'];
            $news['user'] = implode(',', $toArr);
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);
            //给参与人员发消息
            $touser = MeetUserModel::where("id", "in", $meet['attend_meet_ids'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] =  $meet['attend_meet_ids'];
            $news['user'] = implode(',', $toArr);
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);

            //给参与人员发消息
            $touser = MeetUserModel::where("id", "in", $meet['attend_topic_ids'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] =  $meet['attend_topic_ids'];
            $news['user'] = implode(',', $toArr);
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);

            //给发布人发消息
            $touser = MeetUserModel::where("id", "in", $summary['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $summary['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "纪要已通过：" . $summary['content'];
            $news['content'] =  $summary['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingMinutes/meetingMinutes?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);

        } else {
            //会议纪要审核失败  //给发布人发消息
            $touser = MeetUserModel::where("id", "in", $summary['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $summary['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "纪要未通过：" . $summary['content'];
            $news['content'] =   "驳回理由：" . $input['content'];
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $arr = [
                'meet_id' => $meet['id'],
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingMinutes/meetingMinutes?id='.$meet['id'],
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        }
        return ['status' => 1, 'msg' => '审核纪要成功','res'=>$res];
    }


    /*方便调用，咱获取id从前读取
    type: invite 邀请
    */
    public static function sendMeetNotice($input, $type, $data)
    {
        $info = ['status' => 0, 'msg' => '失败'];


        $typeArr = ['room_order' => '预定会议室', 'room_check' => '审核会议室',
            'meet_add' => '添加会议', 'meet_check' => '会议审核', 'meet_edit' => '修改会议', 'meet_delete' => '会议取消',
            'topic_add' => '发起议题', 'topic_edit' => '修改议题', 'topic_check' => '审核议题',
            'summary_add' => '发起纪要', 'summary_edit' => '修改纪要', 'summary_check' => '审核纪要',
            'meet_attend' => '参会消息', 'meet_sign' => '参会签到', 'meet_clock' => '参会提醒'
        ];

        $typeName = getNoticeTypeName($type);
        if (empty($typeName)) {
            return ['status' => 0, 'msg' => '消息类型未定义'];
        }

        switch ($type) {
            case "room_order":
                $info = self::sendRoomOrderNotice($input, $type, $data);
                break;
            case "room_check":
                $info = self::sendRoomCheckNotice($input, $type, $data);
                break;
            case "meet_check":
                $info = self::sendMeetCheckNotice($input, $type, $data);
                break;
            case "topic_add":
                $info = self::sendTopicAddNotice($input, $type, $data);
                break;
            case "topic_check":
                $info = self::sendTopicCheckNotice($input, $type, $data);
                break;
            case "summary_add":
                $info = self::sendSummaryAddNotice($input, $type, $data);
                break;
            case "summary_edit":
                $info = self::sendSummaryAddNotice($input, $type, $data);
                break;
            case "summary_check":
                $info = self::sendSummaryCheckNotice($input, $type, $data);
                break;
            case "meet_delete":
                $info = self::sendRoomCancelNotice($input, $type, $data);
                break;
            case "meet_notice":
                $info = self::sendRoomCheckNotice2($input, $type, $data);
                break;
            case "meet_sign":
                $news = [];
                //给自己发消息
                $touser = MeetUserModel::where("id", "in", $input['user_id'])->select();
                $toArr = array_column($touser, 'username');
                $news['user_id'] = $input['user_id'];
                $news['from_id'] = $input['user_id'];
                $news['meet_id'] = $input['meet_id'];
                //$news['check_id'] = $input['check_id'];
                $news['link_id'] = $input['meet_id'];
                $news['title'] = '签到通知';
                //$news['content'] = $input['title'] . ' ' . ;
                $title = '';
                $meetModel = MeetModel::get($input['meet_id']);
                if(!empty($input['status'])){
                    if($input['status'] == 1){
                        $title = $meetModel['normal_tip'];
                    }
                    if($input['status'] == 2){
                        $title = $meetModel['late_tip'];
                    }
                    if($input['status'] == 3){
                        $title = $meetModel['absence_tip'];
                    }
                }
                if($title){
                    $news['content'] = $title ;
                    $news['type'] = $type;
                    $news['meet_type'] = $meetModel?$meetModel['meet_type']:0;

                    $news['status'] = isset($input['status'])?:0; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
                    $news['user'] = implode(',', $toArr);
                    $news['start_time'] = date("Y-m-d H:i:s");
                    $modelN = new MeetNoticeModel($news);
                    $modelN->allowField(true)->save();
                    $arr = [
                        'meet_id' => $input['meet_id'],
                        'title' => $news['title'],
                        'description' => $title,
                        'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id'],
                    ];
                    $res = Wechat::sendMessage($arr, $toArr);
                }
                $info = ['status' => 1, 'msg' => '签到成功'];
                break;

            default:
                $info = ['status' => 0, 'msg' => '失败'];

        }
        return $info;
    }

}
