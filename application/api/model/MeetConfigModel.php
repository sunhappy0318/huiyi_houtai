<?php


namespace app\api\model;


use think\Cache;
use think\Exception;
use think\Model;

class MeetConfigModel extends Model
{
    // 表名
    protected $name = 'meet_config';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    //获得应用jsapi_ticket
    public static function getAppTicket($access_token)
    {
        $qy_jsapi_ticket = Cache::get('qy_jsapi_ticket2');
        if($qy_jsapi_ticket){
            return $qy_jsapi_ticket;
        }
        $url  = 'https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token='.$access_token.'&type=agent_config';
        $client = new \GuzzleHttp\Client();
        $result = $client->get($url)->getBody()->getContents();
        $api_data = json_decode($result, true);
        //print_r($api_data);die();
        if($api_data['errmsg'] != 'ok'){
            log_print('获取access_token出错');
            log_print($api_data);
            return '';
        }
        Cache::set('qy_jsapi_ticket2',$api_data['ticket'],7100);
        return $api_data['ticket'];
    }

    //获得企业 jsapi_ticket
    public static function getTicket($access_token)
    {
        $qy_jsapi_ticket = Cache::get('qy_jsapi_ticket');
        if($qy_jsapi_ticket){
            return $qy_jsapi_ticket;
        }
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token='.$access_token;
        $client = new \GuzzleHttp\Client();
        $result = $client->get($url)->getBody()->getContents();
        $api_data = json_decode($result, true);
        //print_r($api_data);die();
        if($api_data['errmsg'] != 'ok'){
            log_print('获取access_token出错');
            log_print($api_data);
            return '';
        }
        /*
         * Array
            (
                [errcode] => 0
                [errmsg] => ok
                [ticket] => kgt8ON7yVITDhtdwci0qeUj7W5Dh-1veJvJD_BOZFxACWmgp7sxlGrxmCqjemkxgI4Xw0Z6IJOiYEriZxwunbg
                [expires_in] => 7200
            )
         *
         * */
        Cache::set('qy_jsapi_ticket',$api_data['ticket'],7200);
        return $api_data['ticket'];
    }

    public static function getAccessToken($corpid,$secret)
    {
        $qy_access_token = Cache::get($secret);
        if($qy_access_token){
            $api_data = json_decode($qy_access_token, true);
            return $api_data;
        }
        $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$corpid&corpsecret=$secret";
        $client = new \GuzzleHttp\Client();
        $result = $client->get($url)->getBody()->getContents();
        $api_data = json_decode($result, true);
        //print_r($api_data);die();
        if($api_data['errmsg'] != 'ok'){
            log_print('获取access_token出错');
            log_print($api_data);
            return '';
        }
        /*
                  * Array
         (
             [errcode] => 0
             [errmsg] => ok
             [access_token] => e9qoLaf96xFMOanXG8OwlEPS8moIFBCyp2OAa0fjRePgw2j99nposUE51Wj9mM40oBEledeYv5tkMx6oZivNX19s7F8Fhny2R-0kA7zsBQUqm-C-FD6lWu9-n1-0yyxu7kg-TzG6voGC7v-mr5i-6Kk1HeyP3VnQpX3vyec6zoqaydkPJTBZ1Wv5Nn3BewY8LvbQNNyLv6jpF7NZY2Xq-g
             [expires_in] => 7200
         )

      * */
        Cache::set($secret,$result,7200);
        return $api_data;
    }


    public static function getId($id)
    {
        $row = self::get($id);
        return $row['AgentId'];
    }

    public static function config($path = '')
    {
        $type = self::getCheckPath($path);
        if(is_dev()){
            if($type == 'zuji'){
                $agentId = '1000017';
            }elseif($type == 'dangan') {
                $agentId = '1000015';
            }elseif($type == 'huodong'){
                $agentId = '1000014';
            }elseif($type == 'gongzi'){
                $agentId = '1000016';
            }else{
                $agentId = '1000011';
            }
            $config = self::get(['AgentId' => $agentId]);;
            $corpid = $config['corpid'];
            $Secret = $config['Secret'];
            return [
                'corpid' => $corpid,
                'Secret' => $Secret,
                'AgentId' => $agentId,
                'app_name' => $config['title'],
            ];
        }else{
            if($type == 'zuji'){
                $agentId = '1000106';
            }elseif($type == 'dangan') {
                $agentId = '1000112';
            }elseif($type == 'huodong'){
                $agentId = '1000109';
            }elseif($type == 'gongzi'){
                $agentId = '1000108';
            }else{
                $agentId = '1000105';
            }
            $config = CorpModel::where('AgentId',$agentId)
                ->field('id,app_name,AgentId,open_corpid,permanent_code')
                ->find();
            return [
                'corpid' => $config['open_corpid'],
                'Secret' => $config['permanent_code'],
                'AgentId' => $agentId,
                'app_name' => $config['app_name'],
            ];
        }
    }

    public static function getMsgConfigByMeetId($meet_id){
        $meet = MeetModel::get($meet_id);
        if(is_dev()){
            $agentId = '1000011';
            if($meet && $meet['meet_type'] == 1){
                $agentId = '1000014';
            }
            $config = self::get(['AgentId' => $agentId]);;
            $corpid = $config['corpid'];
            $Secret = $config['Secret'];
            return [
                'corpid' => $corpid,
                'Secret' => $Secret,
                'AgentId' => $agentId,
                'app_name' => $config['title'],
            ];
        }else{
            $agentId = '1000105';
            if($meet && $meet['meet_type'] == 1){
                $agentId = '1000109';
            }
            $config = CorpModel::where('AgentId',$agentId)
                ->field('id,app_name,AgentId,open_corpid,permanent_code')
                ->find();
            return [
                'corpid' => $config['open_corpid'],
                'Secret' => $config['permanent_code'],
                'AgentId' => $agentId,
                'app_name' => $config['app_name'],
            ];
        }


    }

    /**
     * $path
     * 根据前端路径判断，返回不同应用的agentid
     *
     */
    public static function getCheckPath($path)
    {
        //清外足迹
        /*$path1 = [
            'tabbar-5-detial'
        ];*/
        //foreach($path1 as $v){
        if(strpos($path,'tabbar-5-detial') !== false){
            return 'zuji';  //清外足迹
        }
        if(strpos($path,'archives') !== false){
            return 'dangan';  //清外教师档案
        }
        if(strpos($path,'huodong') !== false){
            return 'huodong';  //清外活动
        }
        if(strpos($path,'salary') !== false){
            return 'gongzi';  //清外工资
        }
        //}
        return 'meet'; //会议系统
    }

}
