<?php


namespace app\api\model;


class WorkModel
{
    //获取服务商凭证
    /**
     * POST
     *  {
    "corpid":"xxxxx",
    "provider_secret":"xxx"
    }返回结果：
     *  {
    "provider_access_token":"enLSZ5xxxxxxJRL",
    "expires_in":7200
    }
     */
    public static function get_provider_token()
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token';
        //服务商信息
        $option = [
            'corpid' => 'ww377c91a81493c595',
            'provider_secret' => 'PiLBTsQeZaGTQ0s-T7rSdjukLL7Q7UbtQAWK5a1u2FM',
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $api_data = json_decode($req, true);
        /*
         * Array
            (
                [provider_access_token] => AUvogYMMIm0PBkPEUD
                [expires_in] => 7200
            )
         * */
        //print_R($api_data);die();
        if(!empty($api_data['provider_access_token'])){
          return   $api_data['provider_access_token'];
        }
        log_print('get_provider_token获取失败');
        return '';
    }

    public static function corpid_to_opencorpid($provider_access_token,$corpid)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/corpid_to_opencorpid?provider_access_token='.$provider_access_token;
        //服务商信息
        $option = [
            'corpid' => $corpid,
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $api_data = json_decode($req, true);
        /*｛
         "errcode":0,
         "errmsg":"ok",
         "open_corpid":"AAAAAA"
        ｝*/
        print_R($api_data);
        if(!empty($api_data['open_corpid'])){
            return   $api_data['open_corpid'];
        }
        log_print('corpid_to_opencorpid获取失败');
        return '';
    }

    //获取企业永久授权码
    public static function get_permanent_code($suite_access_token,$auth_code,$SuiteId)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token='.$suite_access_token;
        $option = [
            'auth_code' => $auth_code,
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $api_data = json_decode($req, true);
        log_print($api_data);
        if(!empty($api_data['auth_corp_info'])){
            $row = CorpModel::where('SuiteId',$SuiteId)->find();
            if($row){
                $insert = [
                    'corpid' => $api_data['auth_corp_info']['corpid'],
                    'corp_name' => $api_data['auth_corp_info']['corp_name'],
                    'permanent_code' => $api_data['permanent_code'],
                    'corp_text' => json_encode($api_data,JSON_UNESCAPED_UNICODE),
                ];
                CorpModel::where('SuiteId',$SuiteId)->update($insert,['id' => $row['id']]);
            }
        }
    }

    //获取预授权码
    /**
     * 代开发应用用不上，待代开发应用是在 后台重置secret 和安装回调时通知的
     * @param $suite_access_token
     * @return string
     */
    public static function get_pre_auth_code($suite_access_token)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token='.$suite_access_token;
        $url .= '&debug=1';
        //echo $suite_access_token.PHP_EOL;
        //echo $url;die();
        $client = new \GuzzleHttp\Client();
        $result = $client->get($url)->getBody()->getContents();
        $api_data = json_decode($result, true);
        print_r($api_data);
        echo '22';
        //die();
        if(!empty($api_data['pre_auth_code'])){
            return $api_data['pre_auth_code'];
        }
        log_print('预授权获取错误');
        return '';
    }

    //该API用于获取第三方应用凭证（suite_access_token）。
    public static function get_suite_token($suite_id,$suite_secret,$suite_ticket)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token';
        $option = [
            'suite_id' => $suite_id,
            'suite_secret' => $suite_secret,
            'suite_ticket' => $suite_ticket,
        ];
        //print_r($option);
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $api_data = json_decode($req, true);
        //log_print($api_data);
        //echo 'suite_access_token';
        //print_R($api_data);
        //die();
        if(!empty($api_data['suite_access_token'])){
            return $api_data['suite_access_token'];
        }else{
            log_print($api_data);
            //print_r($api_data);
        }
        return '';
    }

    public static function get_corp_token($suite_access_token,$suite_id)
    {
        $url =  'https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token='.$suite_access_token.'&debug=1';
        $row = CorpModel::where("SuiteId",$suite_id)->find();
        $option = [
            'auth_corpid' => $row['open_corpid'],
            'permanent_code' => $row['permanent_code'],
        ];
        //print_r($option);
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $api_data = json_decode($req, true);
        //log_print($api_data);
        echo 'get_corp_token';
        print_R($api_data);
        //die();
        /*
         * {
            "errcode":0 ,
            "errmsg":"ok" ,
            "access_token": "xxxxxx",
            "expires_in": 7200
        }*/
        if(!empty($api_data['access_token'])){
            return $api_data['access_token'];
        }else{
            log_print($api_data);
            print_r($api_data);
        }
        return '';
    }
}
