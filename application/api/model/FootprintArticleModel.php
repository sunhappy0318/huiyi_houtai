<?php

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class FootprintArticleModel extends Model
{
    use SoftDelete;

    protected $name = 'footprint_article';

    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    public static function getList($params, $time, $limit)
    {
        $list = self::alias('a')
            ->join(['fa_user'=>'b'], 'a.user_id = b.id', 'left')
            ->where($params)->order('a.id', 'desc');

        if (!empty($time) && isset($params['a.status']) && $params['a.status'] == 1) {
            $list->where('a.send_time', 'between time', $time);
        }

        return $list->field('a.*, b.avatar, b.nickname')->paginate($limit)->toArray();
    }

    public function getCreateTimeAttr($value, $data)
    {
        return date('Y-m-d H:i', $value);
    }

    public function getUpdateTimeAttr($value, $data)
    {
        return date('Y-m-d H:i', $value);
    }

    public function getDiagramAttr($value, $data)
    {
        if (is_array($value)) {
            return $value;
        }

        $value = json_decode($value, true);
        if (empty($value)) return [];
        $res = [];
        if ($value) {
            foreach ($value as $pic) {
                if (empty($pic)) continue;
                if (substr($pic,0,4 ) != 'http'){
                    $res[] = request()->domain() . $pic;
                }else{
                    $res[] = $pic;
                }

            }
        }

        return $res;
    }

    public function getVideoAttr($value, $data)
    {
        if ($value) {
            $videoSuffix = ['mp4','avi','mpeg','wmv','mov','flv','avchd','webm','wkv'];
            $suffix = pathinfo($value, PATHINFO_EXTENSION);
            if (!in_array($suffix,$videoSuffix)) return '';
            if (substr($value,0,4 ) != 'http')
                return request()->domain(). $value;

            return $value;
        }
    }

    public function getVideoFirstAttr($value, $data)
    {
        if ($value)
            return request()->domain(). $value;
    }
}
