<?php
namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class MeetTypeModel extends Model
{
    // 表名
    protected $name = 'meet_type';


    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }
}
