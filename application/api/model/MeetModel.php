<?php

namespace app\api\model;

use app\api\controller\MeetRoom;
use app\api\controller\Wechat;
use app\api\model\room\RoomModel;
use app\api\model\room\RoomOrderModel;
use app\api\model\room\RoomWeekModel;
use app\common\library\Auth;
use traits\model\SoftDelete;
use think\Model;

class MeetModel extends Model
{

    //use SoftDelete;

    // 表名
    protected $name = 'meet';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        //  'state_text'
        'role_type',
        'is_faqi',//发起人
        'is_shenhe', //审核人
        'is_canhui', //参会人
        'is_yiti', //议题参与人
        'is_qianfa', //签发人
        'is_xiaozhang', //校长
    ];

    //获取所有参会人的id
    public static function getAllMeetUserId($id)
    {
        $meet = self::get($id);
        if(empty($meet)){
            return [];
        }
        $user_ids = '';
        $user_ids .= $meet['user_id'];
        $user_ids .= ','.$meet['check_ids'];
        $user_ids .= ','.$meet['attend_meet_ids'];
        $user_ids .= ','.$meet['issued_ids'];
        if(!empty($meet['attend_topic_ids']) && ($meet['type'] == 1 || $meet['type'] == 2)){
            $user_ids .= ','.$meet['attend_topic_ids'];
        }
        $ids_arr = explode(',',$user_ids);
        $ids_arr = array_unique($ids_arr);
        return $ids_arr;
    }

    //获取所有参会人的id
    public static function getAllMeetUserId2($id)
    {
        $meet = self::get($id);
        if(empty($meet)){
            return [];
        }
        $user_ids = '';
        $user_ids .= $meet['check_ids'];
        $user_ids .= ','.$meet['attend_meet_ids'];
        $user_ids .= ','.$meet['issued_ids'];
        //echo $user_ids;

        if(!empty($meet['attend_topic_ids']) && ($meet['type'] == 1 || $meet['type'] == 2)){
            //只有发布过议题的参与人才能收到通知
            $map = [
                'meet_id'=>$id,
                'status' => 1,
            ];
            $topicObj = MeetTopicModel::where($map)->field('id,user_id')->select();
            $userids_arr = array_column($topicObj,'user_id');
            $user_str = implode($userids_arr,',');
            if($user_str){
                $user_ids .= ','.$user_str;
            }
        }
        $ids_arr = explode(',',$user_ids);
        $ids_arr = array_unique($ids_arr);
        return $ids_arr;
    }

    //获取所有参会人的id
    public static function getAllMeetUserId3($id)
    {
        $meet = self::get($id);
        if(empty($meet)){
            return [];
        }
        $user_ids = '';
        $user_ids .= $meet['check_ids'];
        $user_ids .= ','.$meet['attend_meet_ids'];
        $user_ids .= ','.$meet['issued_ids'];
        if(!empty($meet['attend_topic_ids']) && ($meet['type'] == 1 || $meet['type'] == 2)){
            $user_ids .= ','.$meet['attend_topic_ids'];
        }
        $ids_arr = explode(',',$user_ids);
        $ids_arr = array_unique($ids_arr);
        return $ids_arr;
    }

    public function getIsFaqiAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if($user_id == $info['user_id']){
            return 1; //发起人
        }
        if($user['is_config'] == 1){
            return 1;
        }
        return 0;
    }

    //审核人
    public function getIsShenheAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if(!isset($info['check_room_ids'])){
            return 0;
        }
        $arr = explode(',',$info['check_room_ids']);
        if(in_array($user_id,$arr)){
            return 1; //审批人
        }
        return 0;
    }

    //参会人
    public function getIsCanhuiAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if(!isset($info['check_room_ids'])){
            return 0;
        }
        $arr = explode(',',$info['attend_meet_ids']);
        if(in_array($user_id,$arr)){
            return 1; //参与人
        }
        return 0;
    }

    //议题参与人
    public function getIsYitiAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if(!isset($info['check_room_ids'])){
            return 0;
        }
        $arr = explode(',',$info['attend_topic_ids']);
        if(in_array($user_id,$arr)){
            return 1; //议题参与人
        }
        return 0;
    }

    //签发人
    public function getIsQianfaAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if(!isset($info['check_room_ids'])){
            return 0;
        }
        $arr = explode(',',$info['issued_ids']);
        if(in_array($user_id,$arr)){
            return 1; //签发人
        }
        return 0;
    }

    //是否是校长
    public function getIsXiaozhangAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        if(!isset($info['check_room_ids'])){
            return 0;
        }
        $arr = explode(',',$info['check_ids']);
        if(in_array($user_id,$arr)){
            return 1;
        }
        return 0;
    }

    //
    public function getRoleTypeAttr($val,$info)
    {
        $auth = Auth::instance();
        $user = $auth->getUserinfo();
        $user_id = $user['id'];
        $rule_type = 2;  //1 发起人 2=参与人 3=管理员  4=校长 5= 议题参与人 6=签发人
      /*  if( empty(  $info['attend_meet_ids'])){
            return 0;
        }*/
        if($user_id == $info['user_id']){
            return 1; //发起人
        }
        if(!isset($info['check_room_ids'])){
            return 2;
        }
        $arr = explode(',',$info['check_room_ids']);
        if(in_array($user_id,$arr)){
            return 3; //审批人
        }
       /* $arr = explode(',',$info['approver_ids']);
        if(in_array($user_id,$arr)){
            return 3; //审批人
        }*/
        $arr = explode(',',$info['check_ids']);
        if(in_array($user_id,$arr)){
            return 4; //校长
        }
        $arr = explode(',',$info['attend_topic_ids']);
        if(in_array($user_id,$arr)){
            return 5; //议题参与人
        }
        $arr = explode(',',$info['issued_ids']);
        if(in_array($user_id,$arr)){
            return 6; //签发人
        }
        $arr = explode(',',$info['attend_meet_ids']);
        if(in_array($user_id,$arr)){
            return 2; //参与人
        }
        return $rule_type;
    }

    public static function roleType2($user_id,$info)
    {
        $rule_type = 2;  //1 发起人 2=参与人 3=管理员  4=校长 5= 议题参与人 6=签发人
        if($user_id == $info['user_id']){
            return 1;
        }
        $arr = explode(',',$info['attend_meet_ids']);
        if(in_array($user_id,$arr)){
            return 2; //参与人
        }
        $arr = explode(',',$info['check_room_ids']);
        if(in_array($user_id,$arr)){
            return 3; //审批人
        }
      /*  $arr = explode(',',$info['approver_ids']);
        if(in_array($user_id,$arr)){
            return 3; //审批人
        }*/
        $arr = explode(',',$info['check_ids']);
        if(in_array($user_id,$arr)){
            return 4; //校长
        }
        $arr = explode(',',$info['attend_topic_ids']);
        if(in_array($user_id,$arr)){
            return 5; //议题参与人
        }
        $arr = explode(',',$info['issued_ids']);
        if(in_array($user_id,$arr)){
            return 6; //签发人
        }
        return $rule_type;
    }

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }

  /*  public function getFilesAttr($val)
    {
        if(!empty($val) && strpos($val,'&qu') === false){
            return json_decode(    $val,true);
        }
        if(!empty($val)){
            return json_decode(   str_replace('&quot;','"', $val),true);
        }
        return [];
    }*/

    //获取开始签到时间
    public static function getSignTime($start_time,$normal_time)
    {
        $normal_time  =explode("-", $normal_time);
        if(empty($normal_time[0])){
            $normal_time[0] = '';

        }
        if(empty($normal_time[1])){
            $normal_time[1] = '';
        }
        $time1 =date( "Y-m-d", strtotime($start_time)) ." ".$normal_time[0];
        $time2 =date( "Y-m-d", strtotime($start_time ))." ".$normal_time[1];
        return  ['time1'=>$time1,'time2'=>$time2];
    }

    public static function getSignList($item)
    {
        //状态:1=已经通过,2=迟到 ,3=缺席，4=请假，0=草稿
        $list1  =  MeetSignModel::where("meet_id", "in", $item['id'])->where("status =1 ")->field("id,user_id,createtime ")->select();
        $list2  =  MeetSignModel::where("meet_id", "in", $item['id'])->where("status =2 ")->field("id,user_id,createtime ")->select();
        $list3  =  MeetSignModel::where("meet_id", "in", $item['id'])->where("status =3 ")->field("id,user_id,createtime ")->select();
        $list4  =  MeetSignModel::where("meet_id", "in", $item['id'])->where("status =4 ")->field("id,user_id,createtime ")->select();


         $totalArr =  ['total_num'=>count(explode(",",$item['attend_meet_ids'])) ,
            'nomal_num'=>count($list1) ,
             'late_num'=>count($list2) ,
             'absent_num'=>count($list3) ,
            'leave_num'=>count($list4)   ];
        $totalArr['unknown_num'] =  $totalArr['total_num'] - $totalArr['nomal_num'] - $totalArr['late_num'] -$totalArr['leave_num'] - $totalArr['absent_num'] ;

        $totalArr['str'] = '签到 '.($totalArr['nomal_num']+$totalArr['late_num']).' | 请假 '.$totalArr['leave_num'].' | 未反馈 '.($totalArr['unknown_num'] );
        return $totalArr;
    }


    //不同角色展示不同按钮
    public static function checkActionRole($item,$user_id,$type)
    {
        $info = ['status'=>0,'msg'=>'权限不足'];
        switch ($type)  {
            case "self":
                if (in_array($user_id, explode(",", $item['user_id']))) {
                    $info = ['status'=>1,'msg'=>'是会议发起人'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议发起人'];
                }
                break;
            case "check_room":
                if (in_array($user_id, explode(",", $item['check_room_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议室管理人员'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议室管理人员'];
                }
                break;
            case "check":
                if (in_array($user_id, explode(",", $item['check_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议初判人'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议初判人'];
                }
                break;
            case "attend_meet":
                if (in_array($user_id, explode(",", $item['attend_meet_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议参会人员'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议参会人员'];
                }
                break;
            case "attend_topic":
                if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议议题参与人'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议议题参与人'];
                }
                break;
            case "approver":
                if (in_array($user_id, explode(",", $item['approver_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议审批人'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议审批人'];
                }
                break;
            case "issued":
                if (in_array($user_id, explode(",", $item['issued_ids']))) {
                    $info = ['status'=>1,'msg'=>'你是会议签发人'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议签发人'];
                }
                break;

            case "about":
                $newIds = $item['user_id'].",".$item['check_room_ids'].",".$item['check_ids'].",".$item['attend_meet_ids'].",".$item['attend_topic_ids'].",".$item['approver_ids'].",".$item['issued_ids'];
                if (in_array($user_id, explode(",", $newIds))) {
                    $info = ['status'=>1,'msg'=>'是会议相关人员'];
                }else{
                    $info = ['status'=>0,'msg'=>'你不是会议相关人员'];
                }
                break;

            default:
                $info = ['status'=>0,'msg'=>'你不是会议相关人员'];

        }
        return $info;
    }

    //不同角色展示不同按钮
    public static function getActionRole($item,$user_id)
    {
                $info  = ['role'=>'','txt'=>''];
              // case "self":
                if (in_array($user_id, explode(",", $item['user_id']))) {
                    $info['role'] =  $info['role'] .",".'self';
                    $info['txt'] =  $info['txt'] .','.'是会议发起人';
                }
                // "check_room":
                if (in_array($user_id, explode(",", $item['check_room_ids']))) {
                    $info['role'] =  $info['role'] .",".'check_room';
                    $info['txt'] =  $info['txt'] .','.'你是会议室管理人员';
                }
               // "check":
                if (in_array($user_id, explode(",", $item['check_ids']))) {
                    $info['role'] =  $info['role'] .",".'check';
                    $info['txt'] =  $info['txt'] .','.'你是会议初判人';
                }
                // "attend_meet":
                if (in_array($user_id, explode(",", $item['attend_meet_ids']))) {
                    $info['role'] =  $info['role'] .",".'attend_meet';
                    $info['txt'] =  $info['txt'] .','.'你是会议参会人员';
                }
               // "attend_topic":
                if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                    $info['role'] =  $info['role'] .",".'attend_topic';
                    $info['txt'] =  $info['txt'] .','.'你是会议议题参与人';
                }
               // "approver":
                if (in_array($user_id, explode(",", $item['approver_ids']))) {
                    $info['role'] =  $info['role'] .",".'approver';
                    $info['txt'] =  $info['txt'] .','.'你是会议审批人';
                }
                // "issued":
                if (in_array($user_id, explode(",", $item['issued_ids']))) {
                    $info['role'] =  $info['role'] .",".'issued';
                    $info['txt'] =  $info['txt'] .','.'你是会议签发人';
                }

        $info['role'] =  implode(',',array_filter(explode(",",$info['role'])))   ;
        $info['txt'] =   implode(',',array_filter(explode(",",$info['txt'])))   ;

        return $info;
    }

    //不同角色展示不同按钮
    public static function getActionName($item,$user_id)
    {
        //状态:1=已经通过,2=待审批1（审核会议室） ,3=待审批2（校长审核审核会议）,21=会议室驳回,31=校长驳回，9=会议结束 0=草稿
        $action = [];
        if($item['status'] == 0){

        }else if($item['status'] == 1){

            //自己
            if (in_array($user_id, explode(",", $item['user_id']))) {
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "取消会议";
                if($item['sign_way'] == 2) {
                    $action['is_qr_meet'] = 1;
                    $action['is_qr_meet_str'] = "签到二维码";
                }
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_sign_meet'] = 1;
                $action['is_sign_meet_str'] = "参会记录";
                //会议开始了
                if(strtotime($item['start_time']) < time()) {
                    $action['is_end_meet'] = 1;
                    $action['is_end_meet_str'] = "结束会议";
                }
            }
            if (in_array($user_id, explode(",", $item['attend_meet_ids']))) {
                //会议开始了
                if(strtotime($item['start_time']) > time()) {
                    $action['is_ask_leave'] = 1;
                    $action['is_ask_leave_str'] = "请假";
                }
                $signTime =MeetModel::getSignTime($item['start_time'],$item['normal_time']);
                if(strtotime($signTime['time1']) < time()) {
                    $action['is_ask_sign'] = 1;
                    $action['is_ask_sign_str'] = "签到";
                }
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                if( $item['have_goods'] == 1) {
                    $action['is_receive'] = 1;
                    $action['is_receive_str'] = "领取物品";
                }
            }
            if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                //$action['is_publish_topic'] = 1;
                //$action['is_publish_topic_str'] = "上传议题";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
            }
        }else if($item['status'] == 2){

            //假如是自己
            if (in_array($user_id, explode(",", $item['user_id']))) {
                $action['is_del_check'] = 1;
                $action['is_del_check_str'] = "取消审核";
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "取消会议";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_edit_meet'] = 1;
                $action['is_edit_meet_str'] = "修改会议";
            }else if (in_array($user_id, explode(",", $item['check_room_ids']))) {
                $action['is_pass_room'] = 1;
                $action['is_pass_room_str'] = "通过";
                $action['is_reject_room'] = 1;
                $action['is_reject_room_str'] = "驳回";
            }
        }else if($item['status'] == 21){
            //假如是自己
            if (in_array($user_id, explode(",", $item['user_id']))) {
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "取消会议";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_edit_meet'] = 1;
                $action['is_edit_meet_str'] = "修改会议";
            }else if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                $action['is_publish_topic'] = 1;
                $action['is_publish_topic_str'] = "上传议题";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
            }
        }else if($item['status'] == 3){

            //假如是自己
            if (in_array($user_id, explode(",", $item['user_id']))) {
                $action['is_del_check'] = 1;
                $action['is_del_check_str'] = "取消审核";
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "取消会议";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_edit_meet'] = 1;
                $action['is_edit_meet_str'] = "修改会议";
            }else if (in_array($user_id, explode(",", $item['check_ids']))) {
                $action['is_pass_meet'] = 1;
                $action['is_pass_meet_str'] = "通过";
                $action['is_reject_meet'] = 1;
                $action['is_reject_meet_str'] = "驳回";
            }else if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                $action['is_publish_topic'] = 1;
                $action['is_publish_topic_str'] = "上传议题";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
            }
        }else if($item['status'] == 31){

            //假如是自己
            if (in_array($user_id, explode(",", $item['user_id']))) {
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "取消会议";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_edit_meet'] = 1;
                $action['is_edit_meet_str'] = "修改会议";
            }else if (in_array($user_id, explode(",", $item['attend_topic_ids']))) {
                $action['is_publish_topic'] = 1;
                $action['is_publish_topic_str'] = "上传议题";
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
            }
        }else if($item['status'] == 9){
            if (in_array($user_id, explode(",", $item['attend_meet_ids']))) {
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_look_summary'] = 1;
                $action['is_look_summary_str'] = "下载会议纲要";
            }
            if (in_array($user_id, explode(",", $item['approver_ids']))) {
                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_look_summary'] = 1;
                $action['is_look_summary_str'] = "下载会议纲要";
            }
            //假如是自己
            if (in_array($user_id, explode(",", $item['user_id']))) {

                $action['is_look_topic'] = 1;
                $action['is_look_topic_str'] = "查看议题";
                $action['is_clone_meet'] = 1;
                $action['is_clone_meet_str'] = "克隆会议";
                $action['is_del_meet'] = 1;
                $action['is_del_meet_str'] = "上传会议纪要";
            }

        }

        return $action;
    }


    //不同角色展示不同按钮
    public static function getAdminActionName($item,$user_id)
    {
        //状态:1=已经通过,2=待审批1（审核会议室） ,3=待审批2（校长审核审核会议）,21=会议室驳回,31=校长驳回，9=会议结束 0=草稿
        $action = [];

        if($item['status'] == 0){

        }else if($item['status'] == 1){

        }else if($item['status'] == 2) {

            if (in_array($user_id, explode(",", $item['check_room_ids']))) {
                $action['is_pass_room'] = 1;
                $action['is_pass_room_str'] = "通过";
                $action['is_reject_room'] = 1;
                $action['is_reject_room_str'] = "驳回";
            }
        }else if($item['status'] == 3){

           if (in_array($user_id, explode(",", $item['check_ids']))) {
              //  $action['is_pass_meet'] = 1;
             //   $action['is_pass_meet_str'] = "通过";
             //  $action['is_reject_meet'] = 1;
             //  $action['is_reject_meet_str'] = "驳回";
                $action['is_check_topic'] = 1;
               $action['is_check_topic_str'] = "议题详情";
            }
        }

        return $action;
    }

    //不同角色展示不同按钮
    public static function getAdminTopicActionName($item,$user_id)
    {
        //状态:1=已经通过,2=待审批1（审核会议室） ,3=待审批2（校长审核审核会议）,21=会议室驳回,31=校长驳回，9=会议结束 0=草稿
        $action = [];
        if($item['status'] == 0){

        }else if($item['status'] == 1){

        }else if($item['status'] == 2) {

            if (in_array($user_id, explode(",", $item['check_room_ids']))) {
                $action['is_pass_room'] = 1;
                $action['is_pass_room_str'] = "通过";
                $action['is_reject_room'] = 1;
                $action['is_reject_room_str'] = "驳回";
            }
        }else if($item['status'] == 3){
            if (in_array($user_id, explode(",", $item['check_ids']))) {
                //  $action['is_pass_meet'] = 1;
                //   $action['is_pass_meet_str'] = "通过";
                //  $action['is_reject_meet'] = 1;
                //  $action['is_reject_meet_str'] = "驳回";
                $action['is_check_topic'] = 1;
                $action['is_check_topic_str'] = "议题详情";
            }
        }

        return $action;
    }



    public static function detail($input)
    {
        $info = self::where('id', $input['id'])
            ->field('*')
            ->find();
        if (!$info) {
            return ['status' => 0, 'msg' => '会议不存在'];
        }

        //会议相关人员
        /*$checkRole = MeetModel::checkActionRole($info,$input['user_id'],'about');
        if($checkRole['status'] == 0 ){
            return $checkRole;
        }*/
       //1 发起人 2=参与人 3=管理员  4=校长 5= 议题参与人 6=签发人
        $rule_type = self::roleType2($input['user_id'],$info);
        $info['role_type'] = $rule_type;

        $info['type_name'] = getTypeName($info['type']);
        if($info['type'] == 2){
            $info['type'] = 1;
        }
        if($info['type'] == 6){
            $info['type'] = 5;
        }
        $info['status_name'] = getStatusName($info['status'],$info['start_time']);
        $start_time = strtotime($info['start_time']);
        $info['time']  =  date("m月d日", $start_time) ."  ". getWeekName($info['start_time']) ."  ".date("H:i", $start_time )."-". date("H:i", strtotime($info['end_time']));

        $info['room'] = RoomModel::where("id", "in", $info['room_id'])->field("id,title,content,manager_id ")->find();
        $info['user'] = MeetUserModel::where("id", "in", $info['user_id'])->field("id,nickname,username,avatar")->find();
        $info['status_name'] = getStatusName($info['status'],$info['start_time']);
        $info['check_room_ids_user'] =  MeetUserModel::where("id", "in", $info['check_room_ids'])->field("id,nickname,username,avatar")->select();
        $info['check_ids_user'] = MeetUserModel::where("id", "in", $info['approver_ids'])->field("id,nickname,username,avatar")->select();
        $attend_meet_ids_user = MeetUserModel::where("id", "in", $info['attend_meet_ids'])->field("id,nickname,wx_userid,username,avatar")->select();

        $info['attend_meet_ids_user'] = $attend_meet_ids_user;
        $wx_ids = array_column($attend_meet_ids_user,'wx_userid');
        $wx_ids = implode(',',$wx_ids);
        $info['attend_meet_wx_ids'] = $wx_ids;
        $attend_topic_ids_user = MeetUserModel::where("id", "in", $info['attend_topic_ids'])->field("id,nickname,wx_userid,username,avatar")->select();
        $info['attend_topic_ids_user'] = $attend_topic_ids_user;

        $topics_wx_ids =  array_column($attend_topic_ids_user,'wx_userid');
        $topics_wx_ids = implode(',',$topics_wx_ids);
        $info['attend_topic_wx_ids'] = $topics_wx_ids;

        //$info['approver_ids_user'] = MeetUserModel::where("id", "in", $info['approver_ids'])->field("id,nickname,username,avatar")->select();
        $info['issued_ids_user'] = MeetUserModel::where("id", "in", $info['issued_ids'])->field("id,nickname,username,avatar")->select();


        /*$signTime =MeetModel::getSignTime($info['start_time'],$info['normal_time']);
        if(strtotime($signTime['time1']) < time()) {
            $info['sign_list'] = MeetModel::getSignList($info);
        }*/

        $info['role'] = MeetModel::getActionRole($info, $input['user_id']);
        $info['action'] = MeetModel::getActionName($info, $input['user_id']);

        $files = [];
        if(!empty($info['files']) && strpos($info['files'],'&qu') === false){
            $files = json_decode($info['files'],true);
        }
        if(!empty($info['files'])){
            $files = json_decode(str_replace('&quot;','"', $info['files']),true);
        }
        if(empty($files)){
            $files = '';
        }
        $info['files'] = $files;

        $info['meet_check'] = '';
        if($info['status']== 21){
            $map = [
                'meet_id' => $info['id'],
                'status' => 0,
            ];
            $meet_check = MeetCheckModel::where($map)->order('id desc')->find();
            $info['meet_check'] = $meet_check?$meet_check['content']:'';
        }
        $auth = Auth::instance();
        $user = $auth->getUser();
        $map = [
            'user_id' => $user['id'],
            'meet_id' => $info['id'],
        ];
        $model = MeetSignModel::where($map)->find();

        $info['is_sign'] = $model?1:0;
        $map = [
            'user_id' => $user['id'],
            'meet_id' => $info['id'],
        ];
        $record = MeetRecordModel::where($map)->find();
        $info['is_record'] = $record?1:0;

        $map = [
            'meet_id' => $info['id'],
            'user_id' => $user['id'],
        ];
        $topic = MeetTopicModel::where($map)->find();
        $info['is_topic'] = $topic?1:0;
        //$info['sign_way'] =  $info['sign_way'] == 1?"页面签到":"二维码签到"; //1 页面签到 2 二维码签到
        //$info['have_goods'] = $info['have_goods'] == 1?"有":"没有";

        $info['is_collector'] = $user['id'] == $info['topic_collector_id'] ? 1 : 0;

        $qiandao = MeetSignModel::where(['meet_id' => $info['id']])
            ->where('status','in',[1,2])
            ->count();

        $ids = explode(',',$info['attend_meet_ids']);
        $num = count($ids);
        $t = strtotime($info['start_time']);
        $t = date('Y-m-d',$t);
        $qingajia = 0;
        foreach ($attend_meet_ids_user as $v){
           $row = ShenpiModel::getShenpiByUserId($v['wx_userid'],$t);
           if($row){
               $swhere = [
                   'user_id' => $v['id'],
                   'meet_id' => $info['id']
               ];
               $msign = MeetSignModel::where($swhere)->find();
               if(!$msign){
                   $d = [
                       'user_id' => $v['id'],
                       'meet_id' => $info['id'],
                       'nickname' => $v['nickname'],
                       'status' => 4,
                   ];
                   MeetSignModel::create($d);
               }
               $qingajia ++ ;
           }
        }
        $late_time = explode('-',$info['late_time']);
        if(!empty($late_time[1])){
            $lateTime = $t.' '.$late_time[1];
            $lateTime = strtotime($lateTime); //最晚签到时间
        }else{
            $lateTime = strtotime($info['end_time']); //没有最晚签到时间，读取会议结束时间
        }
        if(time()>$lateTime){
            $queqin = $num-$qiandao-$qingajia;
        }else{
            $queqin = 0 ;    //如果最晚签到时间还没到，就不计算缺勤人数
        }

        $info['qiandao'] = $qiandao;
        $info['qingajia'] = $qingajia;
        $info['queqin'] = $queqin;

        $plan_off_time = $info['plan_off_time']?date('Y-m-d H:i:s',$info['plan_off_time']):'';
        $info['plan_off_time_text'] = $plan_off_time;

        unset($info['weigh']);
        unset($info['image']);
        unset($info['images']);
        unset($info['params']);
        return ['status' => 1, 'msg' => '成功', 'data' => $info];
    }

//检查时间是否有效
    public static function checkTime($input = [])
    {
        $room = RoomModel::get($input['room_id']);
        if (!$room) {
            return ['status' => 0, 'msg' => '会议室不存在'];
        }
        $time = strtotime($input['start_time']);
        $time2 = strtotime($input['end_time']);
        if ($time < time()) {
            return ['status' => 0, 'msg' => '开始时间无效'];
        }
        if ($time2 < time()) {
            return ['status' => 0, 'msg' => '结束时间无效'];
        }
        $weekarray = array("日", "一", "二", "三", "四", "五", "六");
        $w = date("w", strtotime($input['start_time']));
        $week = RoomWeekModel::where("room_id =" . $input['room_id'] . " and week= " . $w)->count();
        if ($week > 0) {

            //在内面，左包含，右包含,全包含,等于，左等于，右等于
            $d = MeetModel::where(['room_id' => $input['room_id']])
                ->where(" status!=-1")->where(" (
                 ( UNIX_TIMESTAMP(start_time) <=  '" . $time . "' and  UNIX_TIMESTAMP(end_time) <=  '" . $time2 . "' and  UNIX_TIMESTAMP(end_time) >  '" . $time . "' )
                 or  ( UNIX_TIMESTAMP(start_time) <=  '" . $time . "' and  UNIX_TIMESTAMP(end_time) >=  '" . $time2 . "' )
                 or  ( UNIX_TIMESTAMP(start_time) =  '" . $time . "' and  UNIX_TIMESTAMP(end_time) =  '" . $time2 . "' )
                
                 or  ( UNIX_TIMESTAMP(start_time) >=  '" . $time . "' and  UNIX_TIMESTAMP(end_time) <=  '" . $time2 . "' )
                 or  ( UNIX_TIMESTAMP(start_time) >=  '" . $time . "' and  UNIX_TIMESTAMP(end_time) >=  '" . $time2 . "' and  UNIX_TIMESTAMP(start_time) <  '" . $time2 . "' )
                 )  ")->find();
            if (!empty($d)) {
                $info = ['status' => 0, 'msg' => $d['title'] . '已经占用该会议室', 'data' => $room, 'order' => $d];
            } else {
                $info = ['status' => 1, 'msg' => '可以预定', 'data' => $room];
            }

        } else {
            $info = ['status' => 0, 'msg' => "星期" . $weekarray[$w] . '未开放', 'data' => $room, 'order' => []];
        }
        return $info;
    }

    public static function edit($input)
    {
        //恢复状态
        $model = MeetModel::get($input['id']);
        if (!$model) {
            return ['status' => 0, "msg" => '会议不存在'];
        } elseif ($model['status'] == 1) {
          //  $input['status'] = 9;
        } elseif ($model['status'] == 21) {
            $input['status'] = 2;
        } elseif ($model['status'] == 31) {
            $input['status'] = 3;
        }

        if (empty($input['room_id'])) {
            return ['status' => 0, "msg" => '会议室不存在'];
        }
        //修改时间，就要从新检查一下
        if (!empty($input['end_time']) || !empty($input['room_id'])) {
            $input["room_id"] = !empty($input["room_id"]) ? $input["room_id"] : $model['room_id'];
            $input["start_time"] = !empty($input["start_time"]) ? $input["start_time"] : $model['start_time'];
            $input["end_time"] = !empty($input["end_time"]) ? $input["end_time"] : $model['end_time'];
            $info = MeetModel::checkTime(['room_id' => $input["room_id"], 'start_time' => $input["start_time"], 'end_time' => $input["end_time"],]);
            if ($info['status'] == 0) {
                //假如预定的不是自己的
                if (!empty($info['order'])) {
                    if ($info['order']['id'] != $input['id']) {
                        return $info;
                    }
                } else {
                    return $info;
                }
            }
        }
        $input['check_room_ids'] = $model['check_room_ids'];
        $input['type_id'] = $model['type_id'];


        if (  isset($input['status'] ) && $input['status'] == 0) {
            $input['status'] = 0;
        }else{
            $input['status'] = 2;
        }
        $model = new MeetModel();
        $row = $model->allowField(true)->save($input, ['id' => $input['id'], 'user_id' => $input["user_id"]]);
        if ($row) {
            if ( $input['status'] ==2) {
                $input['meet_id'] = $input['id'];
                unset($input['id']);
                //给另一张表增加数据
                $info = RoomOrderModel::saveData($input);
                $info = MeetNoticeModel::sendMeetNotice($input, "room_order", []);
            }
            return ['status' => 1, "msg" => '修改成功'];
        } else {
            return ['status' => 0, "msg" => '修改失败'];
        }

    }

    public static function edit_status($input)
    {
        //恢复状态
        $meet = MeetModel::get($input['id']);
        if (!$meet) {
            return ['status' => 0, "msg" => '会议不存在'];
        }
        if ($meet['user_id'] != $input["user_id"]) {
            return ['status' => 0, "msg" => '你不能修改'];
        }

        $model = new MeetModel();
        $row = $model->allowField(true)->save($input, ['id' => $input['id'], 'user_id' => $input["user_id"]]);
        if ($row) {
            return ['status' => 1, "msg" => '修改成功'];
        } else {
            return ['status' => 0, "msg" => '修改失败'];
        }

    }

    //提醒发起人开会半小时前设置 议题顺序
    public static function setTopicTips($meet_id)
    {
        $model = MeetModel::get($meet_id);

        //判断是否再开户的半小时内
        $end = strtotime($model['start_time']);
        $start = $end - 1800;
        if(time()>$start && time()<$end){
            $news = [];
            $news['from_id'] = $model['user_id'];
            $news['meet_id'] = $model['id'];
            $news['room_id'] = $model['room_id'];
            //$news['topic_id'] = $topic['id'];
            $news['link_id'] = $model['id'];

            $news['type'] = 'meet_topic_sort';
            $news['status'] = 1; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
            $news['start_time'] = date("Y-m-d H:i:s");
            $map = [
                'type' => 'meet_topic_sort',
                'meet_id' => $meet_id,
            ];
            $row = MeetNoticeModel::where($map)->find();
            if($row){
                log_print($meet_id.'-已经发送了会议提醒记录');
                return false;
            }
            //给发布人发消息
            //给所有会议参与人发审核通过消息
            $touser = MeetUserModel::where("id", $model['user_id'])->select();
            $toArr = array_column($touser, 'username');
            $news['user_id'] = $model['user_id'];
            $news['user'] = implode(',', $toArr);
            $news['title'] = "议题顺序提醒：" ;
            $news['content'] = '会议需要设置议题顺序';
            $modelN = new MeetNoticeModel($news);
            $modelN->allowField(true)->save();
            $dev = DevModel::get(4);
            $arr = [
                'meet_id' => $meet_id,
                'title' => $news['title'],
                'description' => $news['content'],
                'url' => $dev['domain'].'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id']
            ];
            $res = Wechat::sendMessage($arr, $toArr);
        }
    }

    //提前15分钟发送会议提醒
    public static function setTips15($meet_id)
    {
        $item = MeetModel::get($meet_id);

        //判断是否再开户的半小时内
        $end = strtotime($item['start_time']);
        $start = $end - 1850;
        if(time()>$start && time()<$end){
            $notice = MeetNoticeModel::where( ['meet_id'=>$item['id'],'type'=>'attend_meet'])->find();
            //没有记录说明没有发过消息
            if(empty($notice)){
                $room =  RoomModel::get($item['room_id']);
                //发给所有人
                $ids_arr = MeetModel::getAllMeetUserId($item['id']);
                //给自己发消息
                $touser = MeetUserModel::where("id", "in", $ids_arr)->select();
                $toArr = array_column($touser, 'username');
                //给议题作者发消息
                $news['user_id'] = $item["attend_meet_ids"];
                $news['from_id'] = $item['user_id'];
                $news['meet_id'] = $item['id'];
                $news['room_id'] = $item['room_id'];
                $news['link_id'] = $item['id'];

                $news['title'] = "开会提醒：".  $item['title'];
                $start_time  =  date("m月d日", strtotime($item['start_time'])) ."  ". getWeekName($item['start_time']) ."  ".date("H:i", strtotime($item['start_time']))."-". date("H:i", strtotime($item['end_time']));

                $news['content'] = ''.getTypeName($item['type_id']).' | 地点：'.$room["title"].$room["floor"].$room["number"]  ." ".$start_time;

                $news['type'] = "attend_meet";
                $news['status'] = 1; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
                $news['user'] = implode(',', $toArr);
                $news['start_time'] = date("Y-m-d H:i:s");

                //给发布者发消息
                $arr = [
                    'meet_id' => $item['id'],
                    'title' => $news['title'],
                    'description' => $news['content'],
                    'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id'],
                ];
                $modelN = new MeetNoticeModel($news);
                $modelN->allowField(true)->save();
                Wechat::sendMessage($arr, $toArr);
            }
        }
    }

}
