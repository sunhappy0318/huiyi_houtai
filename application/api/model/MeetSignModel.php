<?php
namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class MeetSignModel extends Model
{

    // 表名
    protected $name = 'meet_sign';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        //  'state_text'
    ];

    public function getLogoImageAttr($val)
    {
        return cdnurl($val);
    }

    public static function detail($id)
    {
        $info = self::where('id', $id)
            ->field('*')
            ->find();
        return $info;
    }



    public static function add($input)
    {
        $meet = MeetModel::get($input['meet_id']);
        if (!$meet) {
            return ['status' => 0, "msg" => '会议不存在'];
        }

        /*$checkRole = MeetModel::checkActionRole($meet,$input['user_id'],'attend_meet');
        if($checkRole['status'] == 0 ){
            return $checkRole;
        }*/
        $input['title'] = "签到成功";
        if(!empty($meet['late_time'])){
            /*$meet['late_time']  =explode("-", $meet['late_time']);
            $meet['normal_time']  =explode("-", $meet['normal_time']);

            $time3 =date( "Y-m-d", strtotime($meet['start_time'] )) ." ".$meet['late_time'][0];
            $time4 =date( "Y-m-d", strtotime($meet['start_time'] ))." ".$meet['late_time'][1];

            $time1 =date( "Y-m-d", strtotime($meet['start_time'] )) ." ".$meet['normal_time'][0];
            $time2 =date( "Y-m-d", strtotime($meet['start_time'] ))." ".$meet['normal_time'][1];*/

            /*if(strtotime($time1) > time() ){
                return ['status' => 0, "msg" => '未到签到时间','time'=>$time1];
            }*/
           /* if(strtotime($time2) > time() ){
                $input['status'] = 1;
                $input['title'] =$meet['normal_tip']  ;

            }elseif (strtotime($time3) < time() &&  strtotime($time4) > time()  ){
                $input['status'] = 2;
                $input['title'] = $meet['late_tip'];

            }elseif ( strtotime($time4) < time()  ){
                $input['status'] = 3;
                $input['title'] = $meet['absence_tip'];
            }*/
        }

        $input['status'] = 1;
        $model = new MeetSignModel($input);
        $res = $model->where(['meet_id' => $input['meet_id'], 'user_id' => $input['user_id']])->find();
        if (!$res) {
            $row = $model->allowField(true)->save();
            $input['check_id'] =  $model->getLastInsID();
            $info = ['status' =>1,'msg' => '签到成功'];
            $info = MeetNoticeModel::sendMeetNotice($input,"meet_sign",[]);
        } else {
            $info = ['status'=>0,'msg'=>'已经签到'];
        }
        return $info;
    }
}
