<?php

namespace app\api\command;

use app\api\controller\Wechat;
use app\api\model\MeetModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetUserModel;
use think\console\Command;
use think\console\Input;
use think\console\Output;


class Timer extends Command
{
    protected function configure()
    {
        $this->setName('timer')->setDescription('timer');
    }

    protected function execute(Input $input, Output $output)
    {
        $map = [
            'status' => 1
        ];
       $list = MeetModel::where($map)
           ->field('id,type,status,start_time,end_time')
           ->select();
       foreach($list as $v){
           if($v['type'] == 1){
               MeetModel::setTopicTips($v['id']);
           }
           MeetModel::setTips15($v['id']); //会议提前30分钟通知
           $date = time();
           $t1 = strtotime($v['start_time']);
           $t2 = strtotime($v['end_time']);
           if($date > $t1 && $date<$t2){
               $meet = MeetModel::get($v['id']);
               $meet->status = 8;
               $meet->save();
           }
           if($date > $t2){
               $meet = MeetModel::get($v['id']);
               $meet->status = 9;
               $meet->save();
           }
       }

        $map = [
            'status' => 8
        ];
        $list = MeetModel::where($map)
            ->field('id,status,start_time,end_time')
            ->where('end_time','<', date('Y-m-d H:i:s'))
            ->select();
        foreach($list as $v){
            $meet = MeetModel::get($v['id']);
            $meet->status = 9;
            $meet->save();
        }
    }
}
