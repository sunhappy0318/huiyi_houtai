<?php

/**
 * 写入日志
 *
 * @param [type] $values
 * @param string $dir
 * @return void
 */
function log2txt($values, $dir = 'test')
{
    // 如果数据是数组则转换成字符
    if (is_array($values)) {
        $values = print_r($values, true);
    } else if(is_object($values)){
        $values = json_encode($values,JSON_FORCE_OBJECT);
    }

    // 日志内容
    $content = '[' . date('Y-m-d H:i:s') . ']' . PHP_EOL . $values . PHP_EOL . PHP_EOL;
    try {
        // 文件路径
        $filePath = $dir . '/logs/';
        // 路径不存在则创建
        !is_dir($filePath) && mkdir($filePath, 0755, true);
        // 写入文件
        return file_put_contents($filePath . date('Y-m-d') . '.log', $content . PHP_EOL . PHP_EOL, FILE_APPEND);
    } catch (\Exception $e) {
        return false;
    }
}


/*检查上传的JSON*/
function getCheckFiles($files,$check = false)
{
    $info = ['status'=>1,'msg'=>'合法'];
    if ($check) {
        if (empty($files)) {
            $info = ['status' => 0, 'msg' => '必须上传'];
        }
    }
    if (!empty($files)) {
        $file = json_decode(str_replace('&quot;', '"', $files), true);
        if (empty($file['fullurl'])) {
            $info = ['status'=>0,'msg'=>'上传路径不对'];
        }
        if (empty($file['filename'])) {
            $info = ['status'=>0,'msg'=>'上传名称不对'];
        }
    }

    return $info;
}

/*检查上传的图片JSON*/
function getCheckImages($images,$check = false)
{
    $info = ['status'=>1,'msg'=>'合法'];
    if ($check) {
        if (empty($images)) {
            $info = ['status' => 0, 'msg' => '必须上传'];
        }
    }
    if (!empty($files)) {
        $imagesList = json_decode(str_replace('&quot;', '"',$images), true);
       foreach ($imagesList as $key=>$item) {
           if (empty($file['fullurl'])) {
               $info = ['status' => 0, 'msg' => '图片上传路径不对'];
               break;
           }
           if (empty($file['filename'])) {
               $info = ['status' => 0, 'msg' => '图片上传名称不对'];
               break;
           }
       }
    }

    return $info;
}


function getNoticeTypeName($type)
{
    $typeArr  =    ['room_order'=>'预定会议室' ,'room_check'=>'审核会议室' ,
        'meet_add'=>'添加会议' ,'meet_check'=>'会议审核' ,'meet_edit'=>'修改会议' ,'meet_delete'=>'会议取消' ,
        'topic_add'=>'发起议题','topic_edit'=>'修改议题' ,'topic_check'=>'审核议题' ,
        'summary_add'=>'发起纪要','summary_edit'=>'修改纪要' ,'summary_check'=>'审核纪要' ,
        'meet_attend'=>'参会消息', 'meet_sign'=>'参会签到', 'meet_clock'=>'参会提醒'
    ];

    return  $typeArr;
}


function getTypeName($id)
{
    $type = [
        '1' => '党总支会议',
        '2' => '校长办公会议',
        '3' => '行政会议',
        '4' => '全体教职工会议',
        '5' => '其它会议',
        '6' => '清外活动',
    ];
    if(array_key_exists($id,$type)){
        return $type[$id];
    }
    return '';
}

function getWeekName($time)
{
    $weekarray = array("日", "一", "二", "三", "四", "五", "六");
    $w = date("w", strtotime($time));
   return  "星期" . $weekarray[$w];
}


//1待审批  2审核不通过 3待进行 4进行中 5已结束  0已取消
function getStatusName($status = 0,$start_time)
{
    $str ="";
    if($status == 1){
        $str = ['txt'=>"进行中",'index'=>4];
        if(strtotime($start_time) > time()){
         $str = ['txt'=>"待进行",'index'=>3];
        }
    }elseif($status == 2 || $status == 3 ){
        $str = ['txt'=>"待审批",'index'=>1];
    }elseif($status == 21 || $status == 31){
        $str = ['txt'=>"被驳回",'index'=>2];
    }elseif($status == 9){
        $str = ['txt'=>"已结束",'index'=>5];
    }elseif($status == 0){
        $str = ['txt'=>"已取消",'index'=>0];
    }
    return $str;
}


function getTopicStatusName($status = 0)
{
    //状态:1=已经通过,2=待审批,3=已驳回，0=草稿
    $str ="";
    if($status == 1){
        $str = ['txt'=>"已通过",'index'=>1];
    }elseif($status == 2){
        $str = ['txt'=>"待审批",'index'=>2];
    }elseif($status == 3){
        $str = ['txt'=>"已驳回",'index'=>3];
    }else{
        $str = ['txt'=>"-",'index'=>0];
    }
    return $str;
}

function getSignStatusText($status = 0)
{
    //状态:状态:1=已经通过,2=迟到 ,3=缺席，4=请假，0=草稿
    $str ="";
    if($status == 1){
       return "已签到";
    }elseif($status == 2){
        return "迟到";
    }elseif($status == 3){
        return "缺席";
    }elseif($status == 4){
        return "请假";
    }else{
        return '';
    }

}


function getSignStatus($status = 0)
{
    //状态:状态:1=已经通过,2=迟到 ,3=缺席，4=请假，0=草稿
    $str ="";
    if($status == 1){
        $str = ['txt'=>"已签到",'index'=>1];
    }elseif($status == 2){
        $str = ['txt'=>"迟到",'index'=>2];
    }elseif($status == 3){
        $str = ['txt'=>"缺席",'index'=>3];
    }elseif($status == 4){
        $str = ['txt'=>"请假",'index'=>4];
    }else{
        $str = ['txt'=>"-",'index'=>0];
    }
    return $str;
}


function return_number($i)
{
    if($i == 1){
        return '一';
    }
    if($i == 2){
        return '二';
    }
    if($i == 3){
        return '三';
    }
    if($i == 4){
        return '四';
    }
    if($i == 5){
        return '五';
    }
    if($i == 6){
        return '六';
    }
    if($i == 7){
        return '日';
    }
}
