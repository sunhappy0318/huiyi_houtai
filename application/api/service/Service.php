<?php

namespace app\api\service;

use addons\third\model\Third;
use app\common\model\User;
use fast\Random;
use think\Db;
use think\Exception;

/**
 * 第三方登录服务类
 *
 */
class Service
{

    /**
     * 第三方登录
     * @param string $platform 平台
     * @param array  $params   参数
     * @param array  $extend   会员扩展信息
     * @param int    $keeptime 有效时长
     * @return boolean
     */
    public static function connect($platform, $params = [], $extend = [], $keeptime = 0)
    {
        $time = time();
        $values = [
            'platform'      => $platform,
            'group_id'        => $params['group_id'],
            'openid'        => $params['openid'],
            'openname'      => $params['nickname'],
            'access_token'  => $params['access_token'],
            'refresh_token' => $params['refresh_token'],
            'expires_in'    => $params['expires_in'],
            'logintime'     => $time,
            'expiretime'    => $time + $params['expires_in'],
        ];
        $extend = [
            'nickname' => $params['openname'],
            'username' => $params['openname'],
            'avatar' => $params['avatar'],
        ];
        $values = array_merge($values, $params);

        $auth = \app\common\library\Auth::instance();
        $auth->logout();
        $auth->keeptime($keeptime);
        //是否有自己的
        $third = Third::get(['platform' => $platform, 'openid' => $params['openid']], 'user');

        if ($third) {
            if (!$third->user) {
                $third->delete();
            } else {
                $third->allowField(true)->save($values);
                // 写入登录Cookies和Token
                $user = $auth->getUser();
                // 更新会员资料
                $user = User::get($third->user_id);
                $fields = ['avatar' => $params['avatar'], 'email' => $params['biz_mail']];
                if ($params['group_id']) {
                    $fields['group_id'] = $params['group_id'];
                }
                if ($params['department_id']) {
                    $fields['department_id'] = $params['department_id'];
                }
                $user->save($fields);
                return $auth->direct($third->user_id);
            }
        }

        //存在unionid就需要判断是否需要生成新记录
        if (isset($params['unionid']) && !empty($params['unionid'])) {
            $third = Third::get(['platform' => $platform, 'unionid' => $params['unionid']], 'user');
            if ($third) {
                if (!$third->user) {
                    $third->delete();
                } else {
                    // 保存第三方信息
                    $values['user_id'] = $third->user_id;
                    $third = Third::create($values, true);
                    // 写入登录Cookies和Token
                    return $auth->direct($third->user_id);
                }
            }
        }

        if ($auth->id) {
            if (!$third) {
                $values['user_id'] = $auth->id;
                Third::create($values, true);
            }
            $user = $auth->getUser();
        } else {

            // 先随机一个用户名,随后再变更为u+数字id
            $domain = request()->host();
            Db::startTrans();
            try {
                // 默认注册一个会员
                $result = $auth->register($params['openname'], $params['userid'], $params['biz_mail'], $params['mobile'], $extend);
                if (!$result) {
                    $user = (new User())->where(['username'=>$params['userid'],'mobile'=>$params['mobile'],])->find();
                   // print_r($user->id);
                    // 保存第三方信息
                    $values['user_id'] = $user->id;
                    Third::create($values, true);
                    Db::commit();
                    // 写入登录Cookies和Token
                    return $auth->direct($user->id);
                    //throw new Exception($auth->getError());
                }
                $user = $auth->getUser();
                $fields = [
                    'wx_userid' => $params['userid'],
                    'email' => $params['biz_mail']
                ];
              /*  if ($params['nickname']) {
                    $fields['nickname'] = $params['nickname'];
                }*/
                if ($params['group_id']) {
                    $fields['group_id'] = $params['group_id'];
                }
                if ($params['department_id']) {
                    $fields['department_id'] = $params['department_id'];
                }

                if ($params['avatar']) {
                    $fields['avatar'] = function_exists("xss_clean") ? xss_clean(strip_tags($params['avatar'])) : strip_tags($params['avatar']);
                }


                // 更新会员资料
                $user = User::get($user->id);
                $user->save($fields);
                // 保存第三方信息
                $values['user_id'] = $user->id;
                Third::create($values, true);
                Db::commit();
            } catch (\Exception $e) {
                print_r($e->getMessage());
                Db::rollback();
                $auth->logout();
                return false;
            }
        }
        // 写入登录Cookies和Token
        return $auth->direct($user->id);
    }


    public static function isBindThird($platform, $openid, $apptype = '', $unionid = '')
    {
        $conddtions = [
            'platform' => $platform,
            'openid'   => $openid
        ];
        if ($apptype) {
            $conddtions['apptype'] = $apptype;
        }
        $third = Third::get($conddtions, 'user');
        //第三方存在
        if ($third) {
            //用户失效
            if (!$third->user) {
                $third->delete();
                return false;
            }
            return true;
        }
        if ($unionid) {
            $third = Third::get(['platform' => $platform, 'unionid' => $unionid], 'user');
            if ($third) {
                //
                if (!$third->user) {
                    $third->delete();
                    return false;
                }
                return true;
            }
        }

        return false;
    }
}
