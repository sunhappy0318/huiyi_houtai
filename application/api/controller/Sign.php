<?php


namespace app\api\controller;


use app\api\model\SignModel;
use app\api\model\SignUserModel;
use app\common\controller\Api;
use think\Db;

class Sign extends Api
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['index','lists'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function addSign()
    {
        $user = $this->auth->getUser();
        $map = [
            'user_id' => $user['id'],
        ];
        $row = SignModel::where($map)->find();
        if(!empty($row)){
             $this->error('您已签到，无需重复操作');
        }
        $data = [
            'user_id' => $user['id'],
        ];
        $model = SignUserModel::where('phone',$user['mobile'])->find();
        if($model){
            $model->user_id = $user['id'];
            $model->signtime = time();
            $model->save();
            $is_sign = 1;
        }else{
            log_print('用户'.$user['id'].'手机号'.$user['mobile'].'不在签到列表');
            $is_sign = 0;
        }
        $data['is_sign'] = $is_sign;
        SignModel::create($data);
        $this->success('签到成功','');
    }

    public function lists()
    {
        $sign_type = input('sign_type');
        if($sign_type == 1){
            //签到用户表
            $list = SignUserModel::whereNotNull('signtime')
                ->field('id,user_id,phone,name,signtime')
                ->order('signtime asc')
                ->select();
            $list = collection($list)->toArray();
            foreach($list as &$v){
                $v['signtime_text'] = date('H:i',$v['signtime']);
                $user = \app\common\model\User::get($v['user_id']);
                $v['avatar'] = $user?$user['avatar']:'';
            }
            $count1 = SignUserModel::whereNotNull('signtime')
                ->field('id')
                ->count();

            //非签到用户表
            $map = [
                'is_sign' => 0,
            ];
            $list3 = SignModel::where($map)->select();
            $count3 = SignModel::where($map)->count();
            $new_list = [];
            foreach($list3 as $k=>$v1){
                $new_list[$k]['id'] = $v1['id'];
                $new_list[$k]['user_id'] = $v1['user_id'];
                $user = \app\common\model\User::get($v1['user_id']);
                $new_list[$k]['phone'] = $user?$user['mobile']:'';
                $new_list[$k]['name'] = $user?$user['nickname']:'';
                $new_list[$k]['avatar'] = $user?$user['avatar']:'';
                $new_list[$k]['signtime'] = date('H:i',$v1['createtime']);
                $new_list[$k]['signtime_text'] = date('H:i',$v1['createtime']);
            }
            $count2 =SignUserModel::whereNull('signtime')
                ->field('id')
                ->count();
            $list = array_merge_recursive($list,$new_list);

        }else{
            $list = SignUserModel::whereNull('signtime')
                ->field('id,user_id,phone,name,signtime')
                ->select();
            $count1 = SignUserModel::whereNotNull('signtime')
                ->field('id')
                ->count();
            $map = [
                'is_sign' => 0,
            ];
            $count3 = SignModel::where($map)->count();
            $count2 = SignUserModel::whereNull('signtime')
                ->field('id')
                ->count();
        }
        $data = [
            'count1' => $count1+$count3,
            'count2' => $count2,
            'list' => $list,
        ];
        $this->success('成功',$data);
    }

}
