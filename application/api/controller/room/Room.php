<?php

namespace app\api\controller\room;

use app\api\model\MeetModel;
use app\api\model\MeetTypeModel;

use app\common\controller\Api;
use think\Db;

class Room extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map['status'] = input('status');
        }
        if (!empty(input('user_id'))) {
            $map['user_id'] = input('user_id');
        }
        $list = MeetModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }

    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetModel::detail($id);
        $this->success('成功', $info);
    }

    // 克隆
    public function clone()
    {
        $id = input('id');
        $input = MeetModel::get($id)->toArray();
        unset($input['id']);
        unset($input['start_time']);
        unset($input['files']);
        $input['user_id'] = $this->auth->id;
        $input['status'] = 2;
        $model = new MeetModel($input);
        $row = $model->allowField(true)->save();
        $this->success('克隆成功', $row);
    }

    public function add()
    {
        $user = $this->auth->getUser();
        $input = input();
        $input['user_id'] = $this->auth->id;
        $input['status'] = 2;
        $model = new MeetModel($input);
        $row = $model->allowField(true)->save();
        $this->success('添加成功', $row);
    }


    /*
     * 重新提交
     * */
    public function edit()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $input['status'] = 2;
        $info = MeetModel::edit($input);
        if ($info['status'] == 1) {
            $this->success('成功', $info['msg']);
        } else {
            $this->success('成功', $info['msg']);
        }
    }

    /*
     * 修改基本的三项信息
     * */
    public function edit_info()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $data = ['room_id' => $input["room_id"],'start_time' => $input["start_time"],'id' => $input["id"],'user_id' => $input["user_id"],];
        $info = MeetModel::edit($data);
        if ($info['status'] == 1) {
            $this->success('成功', $info['msg']);
        } else {
            $this->success('成功', $info['msg']);
        }
    }


    public function edit_status()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $model = MeetModel::get( $input['id']);
        if (in_array($input["status"], [ 2,3,9,0])) {
            $data = ['status' => $input["status"] ,'id' => $input["id"],'user_id' => $input["user_id"],];
            $info = MeetModel::edit($data);
            if ($info['status'] == 1) {
                $this->success(  $info['msg']);
            } else {
                $this->error(  $info['msg']);
            }
        }  else  {
            $this->error('修改状态不对');
        }
    }

    public function delete()
    {
        $input = input();
        $model = new MeetModel();
        // $row = $model->where(['id' => $input['id'],'user_id' =>  $this->auth->id])->delete();
        $row = MeetModel::destroy($input['id']);
        if ($row) {
            $this->success('删除成功', $row);
        } else {
            $this->error('删除失败');
        }
    }

    /*会议其他*/
    public function types()
    {
        $list = MeetTypeModel::where("")
            ->field('*')
            ->select();
        $this->success('成功', $list);
    }
}
