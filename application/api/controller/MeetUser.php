<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetSignModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\common\controller\Api;
use PhpOffice\PhpSpreadsheet\IOFactory;
use think\Db;
use think\Response;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class MeetUser extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['info'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $input = input();
        $limit = input('limit') ? input('limit') : '15';
            $list = MeetUserModel::where("avatar !='' ")->field("id,nickname,username,avatar")
                ->order("id desc")
            ->paginate(3000)->each(function ($item, $key) use (&$input) {

                    return $item;
                })
                ->toArray();

        $this->success('成功', $list);
    }



    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetUserModel::detail($id);
        $this->success('成功', $info);
    }

}
