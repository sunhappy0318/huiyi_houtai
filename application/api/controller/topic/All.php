<?php

namespace app\api\controller\topic;

use app\api\controller\BC;
use app\api\model\MeetM;
use app\api\model\MeetModel;
use app\api\model\MeetTopicModel;
use app\api\model\TopicM;
use app\api\model\TopicTopModel;

class All extends BC
{
    public function index()
    {
        $this->check([
            'meet_ids' => 'array'
        ]);

        $input = input();

        $where_meet = ['topic_collector_id' => $this->auth->id];
        if (isset($input['meet_ids'])) {
            $where_meet['id'] = ['in', $input['meet_ids']];
        }
        $where_topic = [
            'meet_id'   => ['in', MeetM::where($where_meet)->column('id')],
            'pre_check' => TopicM::CHECKING
        ];

        $all = TopicM::with([
            'user'
        ])->where($where_topic)->select();

        $this->success('', get_defined_vars());
    }

    public function newLists()
    {
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if (!$model) {
            $this->error('会议不存在');
        }
        $loginUser = $this->auth->getUserinfo();
        $map = [
            'meet_id' => input('meet_id'),
            'pre_check' => 0,
        ];
        $list = MeetTopicModel::where($map)
            ->order('is_top desc,updatetime desc')
            ->where('status',2)
            ->select();
        $list = collection($list)->toArray();
        foreach($list as &$v){
            $user = \app\common\model\User::where('id',$v['user_id'])->field('id,nickname,avatar')->find();
            $v['nickname'] = $user?$user['nickname']:'';
            $v['avatar'] = $user?$user['avatar']:'';
        }
        $this->success('成功',$list);
    }
}
