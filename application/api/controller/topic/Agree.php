<?php

namespace app\api\controller\topic;

use app\api\controller\BC;
use app\api\model\TopicM;

class Agree extends BC
{
    public function index()
    {
        $this->check([
            'ids' => 'require|array'
        ]);

        $input = input();

        TopicM::update([
            'status' => TopicM::STATUS_NORMAL
        ], [
            'id' => ['in', $input['ids']]
        ]);

        $this->success('', get_defined_vars());
    }
}