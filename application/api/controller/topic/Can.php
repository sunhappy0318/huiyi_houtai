<?php

namespace app\api\controller\topic;

use app\api\controller\BC;
use app\api\model\MeetM;

class Can extends BC
{
    public function index()
    {
        $this->check([
            'meet_id' => 'require'
        ]);

        $input = input();

        $meet = MeetM::where([
            'id' => $input['meet_id']
        ])->find();
        if (!$meet) $this->error('meet_id不存在');

        $can = time() < $meet['topic_submit_off_time'] ? 1 : 0;

        $this->success('', get_defined_vars());
    }
}
