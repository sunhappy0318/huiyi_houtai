<?php

namespace app\api\controller\topic;

use app\api\controller\BC;
use app\api\controller\Wechat;
use app\api\model\MeetConfigModel;
use app\api\model\MeetM;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetUserModel;
use app\api\model\TopicM;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\TextCard;

class Pre extends BC
{
    protected $noNeedLogin = 'index';

    public function index()
    {
        $this->check([
            'ids' => 'require|array'
        ]);
        $input = input();
        TopicM::update([
            'pre_check' => TopicM::CHECKED
        ], [
            'id' => ['in', $input['ids']]
        ]);
        log_print($input['ids']);
        $id = $input['ids'][0];
        $topic              = TopicM::find($id);
        $meet               = MeetM::find($topic['meet_id']);
        $news               = [];
        $news['from_id']    = $this->auth->id;
        $news['meet_id']    = $meet['id'];
        $news['room_id']    = $meet['room_id'];
        $news['topic_id']   = $topic['id'];
        $news['link_id']    = $topic['id'];
        $news['type']       = 'topic_xiaozhang';
        $news['status']     = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
        $news['start_time'] = date("Y-m-d H:i:s");

        $touser          = MeetUserModel::where("id", "in", $meet['check_ids'])->select();
        $toArr           = array_column($touser, 'username');
        $news['user_id'] = $meet['check_ids'];
        $news['user']    = implode(',', $toArr);
        $news['title']   = "收集人推送议题";
        $news['content'] = "请进行审核";

        $notice = new MeetNoticeModel($news);
        $notice->allowField(true)->save();

        $res = Wechat::sendMessage([
            'meet_id'     => $topic['meet_id'],
            'title'       => $news['title'],
            'description' => $news['content'],
            'url'         => request()->domain() . '/#/pages/tabbar-3-detial/topicDetails/topicDetails?id=' . $topic['meet_id'],

        ], $toArr);
        $this->success('', get_defined_vars());
    }
}
