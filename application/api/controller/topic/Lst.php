<?php

namespace app\api\controller\topic;

use app\api\controller\BC;
use app\api\model\MeetM;
use app\api\model\TopicM;

class Lst extends BC
{
    public function index()
    {
        $this->check([
            'page'      => 'require|integer',
            'pre_check' => 'require|integer'
        ]);

        $input = input();

        $meet_ids = MeetM::where([
            'topic_collector_id' => $this->auth->id
        ])->column('id');

        $list = TopicM::where([
            'meet_id'   => ['in', $meet_ids],
            'pre_check' => $input['pre_check']
        ])->paginate(10, '', ['page' => $input['page']]);

        $this->success('', get_defined_vars());
    }
}