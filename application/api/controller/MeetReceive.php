<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetReceiveModel;

use app\common\controller\Api;
use think\Db;

class MeetReceive extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if(!$model){
            $this->error('会议不存在');
        }
        $checkRole = MeetModel::checkActionRole($model,$this->auth->id,'self');
        if($checkRole['status'] == 0 ){
            $this->error($checkRole['msg']);
        }
        $map = [];
        $map = [ 'meet_id' => input('meet_id'), ];
        if (!empty(input('user_id'))) {
            $map[  'user_id'] =  input('user_id');
        }
        $list = MeetReceiveModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }

    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetReceiveModel::detail($id);
        $this->success('成功', $info);
    }


    /*
     * check_room_ids` varchar(255) DEFAULT NULL COMMENT '会议室管理人员ID',
  `check_ids` varchar(255) DEFAULT NULL COMMENT '初判人IDS',
  `attend_meet_ids` text COMMENT '参会人员ID',
  `attend_topic_ids` text COMMENT '议题参与人',
  `approver_ids` text COMMENT '审批人ID',
  `issued_ids` text COMMENT '签发人iD',
     * */
    public function add()
    {
        $input = input();
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if(!$model){
            $this->error('会议不存在');
        }
        if( $model['have_goods'] == 0){
            $this->error('会议不存在物品领取');
        }
        $checkRole = MeetModel::checkActionRole($model,$this->auth->id,'attend_meet');
        if($checkRole['status'] == 0 ){
            $this->error($checkRole['msg']);
        }
        $data  = ['meet_id'=>$input['meet_id'],'user_id'   => $this->auth->id,'num'=> intval( $input['num'])];
        $model = new MeetReceiveModel($data );
        $res = $model->where(['meet_id' => $input['meet_id'],'user_id' =>  $this->auth->id])->find();
        if(!$res){
            $row = $model->allowField(true)->save();
            $this->success('领取成功' );
        } else{
            $this->error('已经领取' );
        }
    }

    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        $model = new MeetReceiveModel();
        $row = $model->allowField(true)->save($input,['id' => $input['id'],'user_id' => $this->auth->id]);
        $this->success('修改成功',$row);
    }

    public function delete()
    {
        $input = input();
        $model = new MeetReceiveModel();
        $row = $model->where(['id' => $input['id'],'user_id' =>  $this->auth->id])->delete();
        if($row){
            $this->success('删除成功',$row);
        } else{
            $this->error('删除失败' );
        }
    }

}
