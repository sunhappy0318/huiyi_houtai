<?php


namespace app\api\controller;


use app\api\model\MeetTypeModel;
use app\api\service\Format;
use app\common\controller\Api;

class Typemeet extends Api
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = [''];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    protected $type = [
        '1' => '党总支会议',
        '2' => '校长办公会议',
        '3' => '行政会议',
        '4' => '全体教职工会议',
        '5' => '其它会议',
        '6' => '清外活动'
    ];

    public function optionsChoose()
    {
        $data = [
            'typelists' => [
                ['id' => 1,'name' => '党总支会议'],
                ['id' => 2,'name' => '校长办公会议'],
                ['id' => 3,'name' => '行政会议'],
                ['id' => 4,'name' => '全体教职工会议'],
                ['id' => 5,'name' => '其它会议'],
            ],
            'sign_type' => [
                ['id' => 0,'name' => '无'],
                ['id' => 1,'name' => '页面签到'],
                ['id' => 2,'name' => '二维码签到'],
            ],
            'meet_type' => [
                ['id' => 0,'name' => '清外会议系统'],
                ['id' => 1,'name' => '听评课系统'],
            ],
        ];
        $this->success('成功',$data);
    }

    public function add(){
        $input = input();

        if(empty($input['title'])){
            $this->error('请输入会议名称');
        }
        if(empty($input['type'])){
            $this->error('请选择会议类型');
        }
        /*if(empty($input['sign_type'])){
            $this->error('请选择签到方式');
        }*/
        if(empty($input['attend_meet_ids'])){
            $this->error('请选择参会人');
        }

        if(!empty($input['shouji_ids'])){
            $input['shouji_ids'] = Format::formatInput($input['shouji_ids']);
        }

        if(!empty($input['faqi_ids'])){
            $input['faqi_ids'] = Format::formatInput($input['faqi_ids']);
        }

        $input['attend_meet_ids'] = Format::formatInput($input['attend_meet_ids']);

        if(!empty($input['attend_topic_ids'])){
            $input['attend_topic_ids'] = Format::formatInput($input['attend_topic_ids']);
        }
      /*  if(!empty($input['approver_ids'])){
            $input['approver_ids'] = Format::formatInput($input['approver_ids']);
        }*/
        if(!empty($input['issued_ids'])){
            $input['issued_ids'] = Format::formatInput($input['issued_ids'],$field = 'issued_ids');
        }
        if(!empty($input['check_ids'])){
            $input['check_ids'] = Format::formatInput($input['check_ids'],$field = 'check_ids');
        }
        $id = (int)input('id');
        $row = MeetTypeModel::get($id);
        if($row){
            $model = new MeetTypeModel();
            $model->allowField(true)->save($input,['id' => $row['id']]);
        } else{
            $model = new MeetTypeModel($input);
            $model->allowField(true)->save();
        }
        $this->success('保存成功');
    }

    public function detail()
    {
        $id = (int)input('id');
        $val = MeetTypeModel::get($id);
        //print_R($val->toArray());die();
        if($val){
            $val = $val->toArray();
            $val['type_text'] = $this->type[$val['type']];
            $val['sign_type_text'] = $val['sign_type'] == 1?'页面签到':'二维码签到';
            if($val['sign_type'] == 0){
                $val['sign_type_text'] = '无';
            }


            $val['faqi_ids'] = Format::getUserById($val['faqi_ids']);
            $val['attend_meet_ids'] = Format::getUserById($val['attend_meet_ids']);
            $val['attend_topic_ids'] = Format::getUserById($val['attend_topic_ids']);
            $val['approver_ids'] = Format::getUserById($val['approver_ids']);
            $val['issued_ids'] = Format::getUserById($val['issued_ids']);
            $val['check_ids'] = Format::getUserById($val['check_ids']);
            $val['shouji_ids'] = Format::getUserById($val['shouji_ids']);
        }
        $this->success('成功',$val);
    }

    public function lists()
    {
        $list = MeetTypeModel::where("")->paginate()->toArray();
        //$list = collection($list)->toArray();
        foreach($list['data'] as &$val){
            $type = $this->type[$val['type']];
            $sign_type = $val['sign_type'] == 1?'页面签到':'二维码签到';
            if($val['sign_type'] == 0){
                $sign_type = '无';
            }
            $val['desc'] = $type.' | '.$sign_type;
            $val['faqi_ids'] = Format::getUserById($val['faqi_ids']);
            $val['attend_meet_ids'] = Format::getUserById($val['attend_meet_ids']);
        }
        $this->success('成功',$list);
    }

    public function clear()
    {
        $id = (int)input('id');
        $val = MeetTypeModel::get($id);
        if($val){
            $val->faqi_ids = '';
            $val->attend_meet_ids = '';
            $val->attend_topic_ids = '';
            $val->issued_ids = '';
            $val->check_ids = '';
            $val->save();
        }
        $this->success('清除成功','');
    }

}
