<?php


namespace app\api\controller;

use app\api\model\MeetCheckModel;
use app\api\model\MeetModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetSummaryModel;
use app\api\model\MeetTopicModel;
use app\api\model\MeetTypeModel;

use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\common\controller\Api;
use think\Db;

class MeetSummary extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        $map['meet_id'] =  input('meet_id');
        $meet = MeetModel::get($map['meet_id']);
        if(!$meet){
            $this->error('会议不存在');
        }
        $list = MeetSummaryModel::where($map)
            ->field("id,meet_id,user_id,status,images,content,files,createtime")
            ->whereNull('deletetime')
            ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$meet ) {
                $item['user']  = MeetUserModel::where( "id", "in",$item['user_id']  )
                    ->field("id,nickname,username,avatar")
                    ->find();
                $item['createtime'] = date("Y-m-d H:i:s",$item['createtime']);

                $checkList= MeetCheckModel::where("summary_id", "in", $item['id'])
                    ->where('type','summary')
                    ->field("status,user_id,content,files,createtime")
                    ->order("id desc")
                    ->select();
                $user = $this->auth->getUser();
                foreach ( $checkList  as $k => &$val) {
                    $suser = \app\common\model\User::get($val['user_id']);
                    $username = $suser?$suser['nickname']:'';
                    $check_status = $val['status'] == 1?'通过':'驳回';
                    $val['check_text'] = $username.$check_status;
                    $val['check_status'] = $val['status'];
                    $val['nickname'] = $username;
                    $val['status_name'] = $val['status'] == 1?"通过":"不通过";
                }
                $item['check'] = $checkList;
                $map = [
                    'user_id' => $user['id'],
                    'type' =>'summary',
                    'summary_id' => $item['id'],
                    'status' => ['<',2],
                ];
                $row = MeetCheckModel::where($map)
                    ->field("id")
                    ->find();
                //参与人对议题今夕表决 1= 未表决 0= 已表决
                $item['is_check'] = $row?0:1;
                return $item;
            })
            ->toArray();
        $this->success('成功', $list);
    }

    // 详情
    public function detail()
    {
        if(!empty(input('meet_id'))){
            $id = input('meet_id');
            $info = MeetSummaryModel::detailByMeetId($id);
            $this->success('成功', $info);
        }
        $id = input('id');
        $info = MeetSummaryModel::detail($id);
        $this->success('成功', $info);
    }

    public function add()
    {
        $user = $this->auth->getUser();
        $input = input();
        $checkFile  = getCheckFiles(input('files'));
        if (empty($checkFile['status'])) {
            $this->error($checkFile['msg']);
        }
        if(empty($input['files'])){
             $this->error('请上传资料');
        }
        $meet_id = input('meet_id');
        $meet = MeetModel::get($meet_id);
        if(!$meet){
            $this->error('会议不存在');
        }
        $meet->is_jiyao = 1;
        $meet->save();
      /*  elseif ( $meet['status'] != 9 ){
            $this->error('会议还没有结束');
        }*/
      /*  if(!empty(input('status'))){
            $input['status'] = 1;
        }*/

       if (empty($input['content'])) {
            //$this->error('请填写纪要内容');
        }
        if(!empty($input['images'])){
            $input['images'] = htmlspecialchars_decode($input['images']);
        }
        if(!empty($input['files'])){
            $input['files'] = htmlspecialchars_decode($input['files']);
        }
        $input['status'] = 2;
        $input['nickname'] = $user['nickname'];

        $res = MeetSummaryModel::where(['meet_id' => $input['meet_id']])->find();
        if ($res) {
            $model = new MeetSummaryModel();
            $row = $model->allowField(true)->save($input,['id'=> $res['id']]);
            $input['summary_id'] = $res['id'];
            $input['user_id'] = $this->auth->id;
            $info = MeetNoticeModel::sendMeetNotice($input,"summary_add",$meet);
            $this->success($info['msg'], $info);
        } else {
            $input['user_id'] = $this->auth->id;
            $model = new MeetSummaryModel($input);
            $row = $model->allowField(true)->save();
            $input['summary_id'] = $model->getLastInsID();
            $info = MeetNoticeModel::sendMeetNotice($input,"summary_add",$meet);
            $this->success($info['msg'], $info);
        }
    }

    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];
        $summary_id = input('id');
        $summary = MeetSummaryModel::get($summary_id);
        if(!$summary){
            $this->error('纪要不存在');
        }
        $meet = MeetModel::get($summary['meet_id']);
        if(!$meet){
            $this->error('会议不存在');
        }
        $model = new MeetSummaryModel();
        $row = $model->allowField(true)->save($input,['id' => $input['id'],'user_id' => $this->auth->id]);
        $input['summary_id'] = $summary_id;
        $info = MeetNoticeModel::sendMeetNotice($input,"summary_edit",$meet);
        $this->success('修改成功',$row);
    }

    public function delete()
    {
        $input = input();
        $model = new MeetSummaryModel();
        $row = $model->where(['id' => $input['id'],'user_id' =>  $this->auth->id])->delete();
        if($row){
            $this->success('删除成功',$row);
        } else{
            $this->error('删除失败' );
        }
    }

}
