<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetSignatureModel;
use app\api\model\MeetSignModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\api\model\SignGoodsModel;
use app\common\controller\Api;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use think\Db;
use think\Response;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class MeetSign extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['info','success','qrcode','excel','wupin'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function qrcode()
    {
        $id = input('meet_id');
        $info = MeetModel::get($id);
        if (!$info) {
            $this->error('会议不存在');
        }
        $url = request()->domain().'/#/pages/signin/signin?meet_id='.$id;
        $qrCode = $this->build($url);
        $qrCodeUrl = $this->serverUrl() . '/uploads/qrcode/' . $qrCode;
        $this->success('返回成功', [
            "title" => $info['title'],
            'qr_url' => $qrCodeUrl,
        ]);
    }
    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $meet_id = input('meet_id') ? input('meet_id') : '0';
        $meet = MeetModel::get($meet_id);
        if (!$meet) {
            $this->error('会议不存在');
        }
       $attend_meet_ids =  $meet['attend_meet_ids'];
        //0=全部,1=签到 ,2=请假，3=缺勤
        if ( input('status') == 1) {
            $user_ids = MeetSignModel::where('status in(1)')
                ->where(['meet_id'=> $meet_id])
                ->column('user_id');
            $list = MeetUserModel::where( 'id','in',$user_ids)
                ->field("id,nickname,username,avatar")
                ->order("id desc")
                ->paginate($limit)->each(function ($item, $key) use (&$meet) {
                    $item['meet_id'] = $meet['id'];
                    $sign = MeetSignModel::where( "meet_id", "in",$meet['id']  )
                        ->where(['user_id'=>$item['id']])
                        ->field("id,title,createtime,status")
                        ->find();
                    $start = strtotime($meet['start_time']);
                    $is_late = 0;
                    if($sign){
                        if($sign['createtime'] > $start){
                            $is_late = 1;
                        }
                    }
                    $item['is_late'] = $is_late; //1=迟到 0=正常
                    $title = $is_late==1?'迟到签到':'签到成功';
                    $item['sign_id'] = $sign?$sign['id']:'';
                    $item['title'] = $title;
                    $item['createtime'] = date("Y-m-d H:i",  $sign['createtime']);
                    return $item;
                })
                ->toArray();
          }else if ( input('status') == 2) {
            $user_ids = MeetSignModel::where('status in(4)')
                ->where(['meet_id'=> $meet_id])
                ->column('user_id');
               $list = MeetUserModel::where('id','in',$user_ids)
                   ->field("id,nickname,username,avatar")
                   ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$meet) {
                       $item['meet_id'] = $meet['id'];
                       $sign = MeetSignModel::where("meet_id", "in", $meet['id'])->where(['user_id' => $item['id']])->field("id,title,createtime,status")->find();
                       $item['is_late'] = 1; //1=迟到 0=正常
                       $item['sign_id'] = $sign?$sign['id']:'';
                       $item['title'] = '请假';
                       $item['createtime'] = '';

                       return $item;
                   })
                   ->toArray();
        }else if ( input('status') == 3) {
            $user_ids = MeetSignModel::where('status in(3)')
                ->where(['meet_id'=> $meet_id])
                ->column('user_id');
            $t = strtotime($meet['start_time']);
            $t = date('Y-m-d',$t);
            $late_time = explode('-',$meet['normal_time']);
            if(!empty($late_time[1])){
                $lateTime = $t.' '.$late_time[1];
                $lateTime = strtotime($lateTime); //最晚签到时间
            }else{
                $lateTime = strtotime($meet['start_time']);
            }
            if(time() > $lateTime){
                $uids = [];
                $m = 0;
                //判断是否超过签到时间
                $userids = MeetModel::getAllMeetUserId($meet_id);
                foreach($userids as $v){
                    $mp = [
                        'meet_id' => $meet_id,
                        'user_id' => $v,
                    ];
                    $row = MeetSignModel::where($mp)->find();
                    if(empty($row)){
                        $uids[$m] = $v;
                        $m++;
                    }
                }
                $user_ids = array_merge($user_ids,$uids);
            }
            //$attend_meet_arr = explode(",",$attend_meet_ids);
            //$diff = array_diff($attend_meet_arr,$user_ids  );
           // print_r($diff);
            $list = MeetUserModel::where( 'id','in', $user_ids)
                ->field("id,nickname,username,avatar")
                ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$meet) {
                    $item['meet_id'] = $meet['id'];
                    $sign = MeetSignModel::where( "meet_id", "in",$meet['id']  )->where(['user_id'=>$item['id']])->field("id,title,createtime,status")->find();
                    $item['is_late'] = 1; //1=迟到 0=正常
                    $item['sign_id'] = $sign?$sign['id']:'';
                    $item['title'] = '缺勤';
                    $item['createtime'] = '';
                    return $item;
                })
                ->toArray();
        }else{
            $user_ids= explode(',',$attend_meet_ids);
            $userids = MeetModel::getAllMeetUserId($meet_id);
            $list = MeetUserModel::where('id','in', $userids)
                ->field("id,nickname,username,avatar")
                ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$meet) {
                    $item['meet_id'] = $meet['id'];
                    $sign = MeetSignModel::where( "meet_id", "in",$meet['id']  )
                        ->where(['user_id'=>$item['id']])
                        ->field("id,title,createtime,status")
                        ->find();
                    $item['is_late'] = 1; //1=迟到 0=正常
                    $item['sign_id'] = '';
                    $item['title'] = '';
                    $item['createtime'] = '';
                    return $item;
                })
                ->toArray();
        }

        $this->success('成功', $list);
    }


    // 数据导出
    public function excel()
    {
        $map = [];
        if(empty(input('meet_id'))){
             $this->error('meet_id必传');
        }
        $meet_id = input('meet_id');
        $map['meet_id'] = $meet_id;
        $list = MeetSignModel::where($map)->select();
        $list = collection($list)->toArray();
        $meet = (new MeetModel())->where(['id' => $meet_id])->cache(true)->find();
        $attend_meet_ids = $meet['attend_meet_ids'];
        $attend_meet_arr = explode(',',$attend_meet_ids); //所有参会人
        $meet_user = \app\common\model\User::where('id','in',$attend_meet_arr)
            ->field('id,nickname')
            ->select();
        $meet_user = collection($meet_user)->toArray();
        foreach($meet_user as $k=>$v){
            foreach($list as $vv){
                if($vv['user_id'] == $v['id']){
                    $meet_user[$k]['user_data'] = $vv;
                }
            }
        }
        //print_R($meet_user);
        $newExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();  //创建一个新的excel文档
        $objSheet = $newExcel->getActiveSheet();
        $objSheet->setTitle('签到人员');  //获取当前操作sheet的对象
        $newExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '姓名')
            //->setCellValue('B1', '会议')
            ->setCellValue('B1', '状态')
            ->setCellValue('C1', '签到时间')
            ->setCellValue('D1', '会议时间')
            ->setCellValue('E1', '备注')
            ->setCellValue('F1', '手写签名');
        $meet_time = $meet?date('Y-m-d',strtotime($meet['start_time'])):'';
        foreach ($meet_user as $k => $val) {
            $k = $k + 2;
            if(!empty($val['user_data'])){
                $statusName = $val['user_data']['status'] == 1 ? "按时参会" : ($val['user_data']['status'] == 2 ? "迟到参会" : "缺勤");
                if($statusName == '缺勤'){
                    $sign_time = '';
                } else{
                    $sign_time = date("m月d日 H:i", $val['user_data']['createtime']);
                }
                $path = $val['user_data']['file'];
            }else{
                $statusName =  "缺勤";
                $sign_time = '';
                $path = '';
            }
            $newExcel->getActiveSheet()->setCellValue('A' . $k, $val['nickname']?$val['nickname']:'')
                //->setCellValue('B' . $k, $meet?$meet['title']:"")
                ->setCellValue('B' . $k, $statusName)
                ->setCellValue('C' . $k, $sign_time)
                ->setCellValue('D' . $k, $meet_time)
                ->setCellValue('E' . $k,'');
            if($path){
                $path = strstr($path,'/uploads');
                if(file_exists('.'.$path)){
                    $objDrawing = new Drawing();
                    $objDrawing->setResizeProportional(false);
                    $objDrawing->setPath('.'.$path);//这里是相对路径
                    $objDrawing->setHeight(30);//照片高度
                    $objDrawing->setWidth(30);
                    $objDrawing->setCoordinates('F'.$k);
                    $objDrawing->setWorksheet($objSheet);
                }else{
                    log_print($meet_id.'-导出图片不存在');
                }
            }
        }
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($newExcel, 'Xlsx');
        $filename = 'sign'.$meet_id.'.Xlsx';
        $path = '/uploads/'.date('Ymd');
        if (!file_exists('.'.$path)) {
            mkdir('.'.$path,0777, true);
        }
        $filePath = $path.'/' . $filename;
        $writer->save('.'.$filePath);
        $url = request()->domain().$filePath;
        $size = filesize('./'.$filePath);
        $file_name = $filename;
        $data = [
            'filesize' => $size,
            'filename' => $file_name,
            "fullurl" => $url,
        ];
        $this->success('成功',$data);
    }

    public function wupin()
    {
        if(empty(input('meet_id'))){
            $this->error('meet_id必传');
        }
       /* $filePath = 'wupin196.Xlsx';
        $size = filesize('./'.$filePath);
        $data = array (
            'filesize' => $size,
            'filename' => 'wupin196.Xlsx',
            'fullurl' => request()->domain().'/'.$filePath,
        );*/
        if(empty(input('meet_id'))){
            $this->error('meet_id必传');
        }
        $meet_id = input('meet_id');
        $map['meet_id'] = $meet_id;
        $List = SignGoodsModel::where($map)->select();
        $newExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();  //创建一个新的excel文档
        $newExcel->getActiveSheet()->setTitle('签到人员');  //获取当前操作sheet的对象
        $newExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '用户')
            ->setCellValue('B1', '会议')
            ->setCellValue('C1', '签名地址')
            ->setCellValue('D1', '会议时间')
            ->setCellValue('E1', '签到时间');
        $meet = (new MeetModel())->where(['id' => $meet_id])->cache(true)->find();
        foreach ($List as $k => $val) {
            $k = $k + 2;
            //$user = (new MeetUserModel())->where(['id' => $val['user_id']])->find();
            //$statusName = $val['status'] == 1 ? "正常" : ($val['status'] == 2 ? "迟到" : "缺席");
            $newExcel->getActiveSheet()->setCellValue('A' . $k, $val['nickname'])
                ->setCellValue('B' . $k, $val['meet_name'])
                ->setCellValue('C' . $k, $val['file'])
                ->setCellValue('D' . $k, $meet?$meet['start_time']:'')
                ->setCellValue('E' . $k, date("Y-m-d H:i:s", $val['createtime']));
        }
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($newExcel, 'Xlsx');
        $filename = 'wupin'.$meet_id.'.Xlsx';
        $path = '/uploads/'.date('Ymd');
        if (!file_exists('.'.$path)) {
            mkdir('.'.$path,0777, true);
        }
        $filePath = $path.'/' . $filename;
        $writer->save('.'.$filePath);
        $url = request()->domain().$filePath;
        $size = filesize('./'.$filePath);
        $file_name = $filename;
        $data = [
            'filesize' => $size,
            'filename' => $file_name,
            "fullurl" => $url,
        ];
        $this->success('成功',$data);
    }

//公共文件，用来传入xls并下载
    public function downloadExcel($newExcel, $filename, $format)
    {

        ob_end_clean();
        // $format只能为 Xlsx 或 Xls
        if ($format == 'Xlsx') {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        } elseif ($format == 'Xls') {
            header('Content-Type: application/vnd.ms-excel');
        }

        header("Content-Disposition: attachment;filename="
            . $filename . date('Y-m-d') . '.' . strtolower($format));
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($newExcel, $format);

        $objWriter->save('php://output');

        //通过php保存在本地的时候需要用到
        //$objWriter->save($dir.'/demo.xlsx');

        //以下为需要用到IE时候设置
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
//         If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        exit;
    }

    // 二维码信息
    public function qr_info()
    {
        $id = input('meet_id');
        $info = MeetModel::get($id);
        if (!$info) {
            $this->error('会议不存在');
        }
        $time = $this->getTime();
        $url = $this->serverUrl() . '/api/meet_sign/add?sign_way=' . $info['sign_way'] . '&time=' . $time . '&meet_id=' . $id;
        //生成二维码 传生成二维码的文本内容
        $qrCode = $this->build($url);
        $qrCodeUrl = $this->serverUrl() . '/uploads/qrcode/' . $qrCode;
        $this->success('返回成功', ["title" => $info['title'],"start_time" => $info['start_time']  ,"time" => $time, "hi" => date("H:i"), "url" => $url, 'qrCodeUrl' => $qrCodeUrl]);
    }


    // 签到成功
    public function ok()
    {
        $user = $this->auth->getUserinfo();
        // print_r($user);
  /*      print <<<EOT

     <div class="slidecont">hi，{$user['username']}</div>
     <div class="newcontainter">
       <H3> 签到 操作成功！</H3>
     </div>
      <a href="" title=" " target="_blank">返回</a>
 EOT;*/
    }

    // 签到详情
    public function info()
    {
        $input = input();
        $input['user_id'] = $this->auth->id;
        $info = MeetSignModel::add($input);
        if ($info['status'] == 1) {
            header("Location: https://huiyi.bigchun.com/api/meet_sign/ok");
        } else {
            $this->error(  $info['msg']);
        }
    }

    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetSignModel::detail($id);
        $this->success('成功', $info);
    }

    public function add()
    {
        $input = input();
        if(empty($input['meet_id'])){
             $this->error('meet_id必传');
        }
        $meet = MeetModel::get($input['meet_id']);
        if(!$meet){
             $this->error('会议不存在');
        }
        $t = strtotime($meet['start_time']);
        $t = date('Y-m-d',$t);
        $start_time = explode('-',$meet['normal_time']);
        $input['status'] = 1;
        if(!empty($start_time[0])){
            $startTime = $t.' '.$start_time[0];
            $startTime = strtotime($startTime); //正常签到时间
        }else{
            $startTime = strtotime($meet['start_time']); //没有最晚签到时间，读取会议结束时间
        }
        if(time() < $startTime){
             $this->error('不到签到时间，无法签到');
        }
        $input['user_id'] = $this->auth->id;
        $res = MeetSignModel::where(['meet_id' => $input['meet_id'], 'user_id' => $input['user_id']])->find();
        if(!empty($input['qiming_id'])){
            $file = MeetSignatureModel::get($input['qiming_id']);
            if(!empty($file)){
                $input['file'] = $file['files']['fullurl'];
            }
        }
        if($res){
            $this->error('您已签到');
        }
        $input['status'] = 1;
        //迟到签到时间
        $late_time = explode('-',$meet['late_time']);
        if(!empty($late_time[0])){
            $lateTime = $t.' '.$late_time[0];
            $lateTime = strtotime($lateTime);
            $end = $t.' '.$start_time[1];
            $end = strtotime($end);
            if(time() > $lateTime && time()<$end){
                $input['status'] = 2;
            }
        }else{
            $end = strtotime($meet['end_time']); //没有最晚签到时间，读取会议结束时间
        }
        //计算缺勤
        if(time() > $end){
            $input['status'] = 3;
        }
        $user = $this->auth->getUser();
        $input['nickname'] = $user['nickname'];
        $info = MeetSignModel::add($input);
        if ($info['status'] == 1) {
            $this->success(  $info['msg'],$info);
        } else {
            $this->error(  $info['msg'],$info);
        }
    }

    //物品签到
    public function addSignGoods()
    {
        $input = input();
        $input['user_id'] = $this->auth->id;
        $meet = MeetModel::get($input['meet_id']);
        if(empty($meet)){
             $this->error('会议不存在');
        }
        $user = $this->auth->getUser();
        $res = SignGoodsModel::where(['meet_id' => $input['meet_id'], 'user_id' => $input['user_id']])->find();
        if($res){
            $this->error('您已签到');
        }
        if(!empty($input['qiming_id'])){
            $file = MeetSignatureModel::get($input['qiming_id']);
            if(!empty($file)){
                $input['file'] = $file['files']['fullurl'];
            }
        }
        $input['meet_name'] = $meet['title'];
        $input['nickname'] = $user['nickname'];
        $model = new SignGoodsModel($input);
        $row = $model->allowField(true)->save();
        $this->success('成功',$row);
    }



    public function ask_leave()
    {
        $input = input();
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if (!$model) {
            $this->error('会议不存在');
        }

        $checkRole = MeetModel::checkActionRole($model,$this->auth->id,'attend_meet');
        if($checkRole['status'] == 0 ){
            $this->error($checkRole['msg']);
        }

        $input['status'] = 4;
        $input['user_id'] = $this->auth->id;
        $model = new MeetSignModel($input);
        $res = $model->where(['meet_id' => $input['meet_id'], 'user_id' => $this->auth->id])->find();
        if (!$res) {
            $row = $model->allowField(true)->save();
            $this->success('请假成功');
        } else {
            $this->error('已经请假');
        }
    }

    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        //$input['user_id'] = $user['id'];
       /* $row = MeetSignModel::get($input['id']);*/
        if(empty($input['meet_id'])){
             $this->error('meet_id必传');
        }
        $map = [
            'user_id' => $input['user_id'],
            'meet_id' => $input['meet_id'],
        ];
        $row = MeetSignModel::where($map)->find();
        if(empty($row)){
            $model = new MeetSignModel($input);
            $row = $model->allowField(true)->save();
            $this->success('设置成功', $row);
        }else{
            $model = new MeetSignModel();
            /* if (!empty(input('createtime'))) {
                 $input['createtime'] = strtotime($input['createtime']);
             }*/
            $input['createtime'] = time();
            $row = $model->allowField(true)->save($input, ['id' => $row['id']]);
            $this->success('设置成功', $row);
        }

    }

    public function delete()
    {
        $input = input();
        $model = new MeetSignModel();
        $row = $model->where(['id' => $input['id'], 'user_id' => $this->auth->id])->delete();
        if ($row) {
            $this->success('删除成功', $row);
        } else {
            $this->error('删除失败');
        }
    }

    public function getTime()
    {

        $time = floor(date("i") / 15) . "-" . date("H");
        return $time;
    }

    public function serverUrl()
    {
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        return $http_type . $_SERVER['HTTP_HOST'];
    }

    // 生成二维码
    public function build($text)
    {
        $config = get_addon_config('qrcode');
        $params = $this->request->get();
        $params = array_intersect_key($params, array_flip(['text', 'size', 'padding', 'errorlevel', 'foreground', 'background', 'logo', 'logosize', 'logopath', 'label', 'labelfontsize', 'labelalignment']));

        $params['text'] = $text;
        $params['label'] = '';
        $qrCode = \addons\qrcode\library\Service::qrcode($params);

        $mimetype = $config['format'] == 'png' ? 'image/png' : 'image/svg+xml';

        $response = Response::create()->header("Content-Type", $mimetype);

        // 直接显示二维码
        // header('Content-Type: ' . $qrCode->getContentType());
        // $response->content($qrCode->writeString());

        $code_name = "";
        // 写入到文件
        // if ($config['writefile']) {
        $qrcodePath = ROOT_PATH . 'public/uploads/qrcode/';
        if (!is_dir($qrcodePath)) {
            @mkdir($qrcodePath);
        }
        if (is_really_writable($qrcodePath)) {
            $filePath = $qrcodePath . md5(implode('', $params)) . '.' . $config['format'];
            $qrCode->writeFile($filePath);
            $code_name = md5(implode('', $params)) . '.' . $config['format'];
        }
        //  }

        return $code_name;
    }

}
