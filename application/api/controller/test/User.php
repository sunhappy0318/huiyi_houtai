<?php

namespace app\api\controller\test;

use app\api\controller\BC;
use app\api\model\UserM;
use app\common\library\Token;

class User extends BC
{
    public function index()
    {
        $this->check([
            'token' => 'require'
        ]);

        $input = input();

        $user = Token::get($input['token']);
        $user = UserM::find($user['user_id']);

        $this->success('', get_defined_vars());
    }
}