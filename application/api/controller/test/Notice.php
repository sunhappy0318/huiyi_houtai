<?php

namespace app\api\controller\test;

use app\api\controller\BC;
use app\api\controller\Wechat;
use app\api\model\MeetConfigModel;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\TextCard;

class Notice extends BC
{
    public function index()
    {
        $config = MeetConfigModel::getMsgConfigByMeetId(534);
        $aid    = $config['AgentId'];
        $cid    = $config['corpid'];
        $secret = $config['Secret'];
        $res    = Factory::work([
            'corp_id'  => $cid,
            'secret'   => $secret,
            'agent_id' => $aid,
        ])->messenger->message(new TextCard([
            'meet_id'     => 1,
            'title'       => 2,
            'description' => 3,
            'url'         => 'https://www.baidu.com',
        ]))->ofAgent($aid)->toUser(['LangDongLei','mr.jiao'])->send();

        $this->success('', get_defined_vars());
    }
}
