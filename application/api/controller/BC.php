<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Validate as V;

class BC extends Api
{

    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function check($rule)
    {
        $validate = new V();
        $res      = $validate->check(input(), $rule);
        if (!$res) {
            $this->error($validate->getError());
        }
    }
}
