<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetTypeModel;
use app\common\controller\Api;
use think\Db;

class MeetType extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map = [
                'status' => input('status'),
            ];
        }
        if (!empty(input('user_id'))) {
            $map[  'user_id'] =  input('user_id');
        }
        $user = $this->auth->getUser();
        $list = MeetTypeModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }

    public function types()
    {
        $meet_type = empty(input('meet_type'))?0:1;
        $where = 'find_in_set(' . $this->auth->id. ',faqi_ids)';
        $list1 =  MeetTypeModel::where($where)
            ->where('meet_type',$meet_type)
            ->field("id,type,title")
            ->select();

        $list2 = MeetTypeModel::where("faqi_ids",['=', null], ['=',''], 'or')
            ->where('meet_type',$meet_type)
            ->field("id,type,title")
            ->select();
        //拼接提示字符串
        $list = array_merge($list1,$list2);
        $msg = '';
        if(empty($list)){
            $msg = '暂无可选会议类型！';
        } else {
            foreach ($list as $v) {
                $msg .= $v['title'].'、';
            }
            $msg .= '仅有限定人员可以发起！';
        }
        $this->success($msg, $list);
    }

    /*会议大类型*/
//    public function types()
//    {
//        $meet_type = empty(input('meet_type'))?0:1;
//        $where = 'find_in_set(' . $this->auth->id. ',faqi_ids)';
//        $list1 =  MeetTypeModel::where($where)
//            ->where('meet_type',$meet_type)
//            ->field("id,type,title")
//            ->select();
//        $list2 = MeetTypeModel::where("")
//            ->whereNull('faqi_ids')
//            ->where('meet_type',$meet_type)
//            ->field("id,type,title")
//            ->select();
//
//        $list = array_merge($list1,$list2);
//        $this->success('成功', $list);
//    }

    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetTypeModel::detail($id);
        $this->success('成功', $info);
    }

    public function add()
    {
        $input = input();
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if(!$model){
            $this->error('会议不存在');
        }
        if(!empty(input('status'))){
            $input['status'] = 1;
        }
        $model = new MeetTypeModel($input);
        $row = $model->allowField(true)->save();
            $this->success('添加成功',$row);
    }

    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        $model = new MeetTypeModel();
        $row = $model->allowField(true)->save($input,['id' => $input['id'],'user_id' => $this->auth->id]);
        $this->success('修改成功',$row);
    }

    public function delete()
    {
        $input = input();
        $model = new MeetTypeModel();
        $row = $model->where(['id' => $input['id'],'user_id' =>  $this->auth->id])->delete();
        if($row){
            $this->success('删除成功',$row);
        } else{
            $this->error('删除失败' );
        }
    }

}
