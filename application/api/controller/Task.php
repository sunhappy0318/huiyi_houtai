<?php


namespace app\api\controller;


use addons\shopro\library\Wechat;
use app\api\model\MeetModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetTypeModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\api\model\StoreOrderModel;
use app\api\model\StoreShopModel;
use app\common\controller\Api;
use think\Request;
use think\Session;

class Task extends Api
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['index','cate','sync_data'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function index()
    {
       // log2txt(date("Y-m-d H:i:S")."==");
        /* 会议开始前10分钟 给参会人员发消息  60*20  最好是 前 20分钟发，假如失败了，还可以发一次 ，因为定时任务 10分钟一次 */
        $res = [];
        $list = MeetModel::where(" (  UNIX_TIMESTAMP(start_time)  > '" . time() . "' and  UNIX_TIMESTAMP(start_time)  < '" . (time() + 60*20) . "'  )   ")->select();

        foreach ($list as $k=>&$item){
            $notice = MeetNoticeModel::where( ['meet_id'=>$item['id'],'type'=>'attend_meet'])->find();
            //没有记录说明没有发过消息
            if(empty($notice)){
            //给发布者发消息
            $touser = MeetUserModel::where("id", "in", $item["attend_meet_ids"])->select();
            $toArr = array_column($touser, 'username');

                $room =  RoomModel::get($item['room_id']);
                //给议题作者发消息
                $news['user_id'] = $item["attend_meet_ids"];
                $news['from_id'] = $item['user_id'];
                $news['meet_id'] = $item['id'];
                $news['room_id'] = $item['room_id'];
                $news['link_id'] = $item['id'];

                $news['title'] = "开会提醒：".  $item['title'];
                $start_time  =  date("m月d日", strtotime($item['start_time'])) ."  ". getWeekName($item['start_time']) ."  ".date("H:i", strtotime($item['start_time']))."-". date("H:i", strtotime($item['end_time']));

                $news['content'] = ''.getTypeName($item['type_id']).' | 地点：'.$room["title"].$room["floor"].$room["number"]  ." <br/>".$start_time;

                $news['type'] = "attend_meet";
                $news['status'] = 2; //状态:1=审批通过,2=待审批，3.审批驳回，0=删除
                $news['user'] = implode(',', $toArr);
                $news['start_time'] = date("Y-m-d H:i:s");

                //给发布者发消息
                $arr = [
                    'title' => $news['title'],
                    'description' => $news['content'],
                    'url' => request()->domain().'/#/pages/tabbar-1-detial/meetingDetails/meetingDetails?id='.$news['link_id'],
                ];
                $res = \app\api\controller\Wechat::sendMessage($arr, $toArr);

                if($res['errcode'] == 0 ){
                    $modelN = new MeetNoticeModel($news);
                   $modelN->allowField(true)->save();
                }

            }
        }

        $this->success('成功',$res);
    }


    public   function sync_data()
    {
        $Wechat = new  \app\api\controller\Wechat();
        $Wechat->department();
        $res = $Wechat->users();
        $this->success('成功',$res);
    }
}
