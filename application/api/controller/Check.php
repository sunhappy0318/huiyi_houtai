<?php


namespace app\api\controller;


use app\api\model\MeetUserModel;
use app\api\model\TeacherAuthModel;
use app\api\service\Format;
use app\common\controller\Api;
use app\common\model\UserTeacher;
use think\Db;

class Check extends Api
{
    protected $noNeedLogin = ['test', 'test1'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function lists()
    {
       /* $userList = UserTeacher::where("")->select();
        foreach($userList as $v){
            $puser = \app\common\model\User::where('mobile',$v['pmobile'])->find();
            $suser = \app\common\model\User::where('mobile',$v['mobile'])->find();

            TeacherAuthModel::where()->find();
        }*/
        $list = TeacherAuthModel::where("")->paginate()->toArray();
        foreach($list['data'] as &$val){
            $users = MeetUserModel::where('id','in',explode(',',$val['user_ids']))->field('wx_userid as id,nickname,avatar')->select();
            $val['user_ids']= implode(',',array_column($users,'id'));
            $val['user_ids_user'] = $users;
            $users = MeetUserModel::where('id','in',explode(',',$val['user_auth_ids']))->field('wx_userid as id,nickname,avatar')->select();
            $val['user_auth_ids']= implode(',',array_column($users,'id'));
            $val['user_auth_ids_user'] = $users;
        }
        $this->success('成功',$list);

    }

    public function add()
    {
        $input = input();
        if(empty(input('group_name'))){
             $this->error('group_name必传');
        }
        if(empty(input('user_ids'))){
            $this->error('user_ids必传');
        }
        if(empty(input('user_auth_ids'))){
            $this->error('user_auth_ids必传');
        }
        $is_all = input('is_all')==1?1:0;
        $id = input('id');
        $input['user_ids'] = Format::formatInput($input['user_ids']);
        $input['user_ids'] = Format::getUserByWxid($input['user_ids']);
        $input['user_auth_ids'] = Format::formatInput($input['user_auth_ids']);
        $input['user_auth_ids'] = Format::getUserByWxid($input['user_auth_ids']);
        $row = TeacherAuthModel::get($id);
        if($row){
            $model = new TeacherAuthModel();
            $model->allowField(true)->save($input,['id' => $id]);
            $auth_id = $row['id'];
        }else{
            $model = new TeacherAuthModel($input);
            $model->allowField(true)->save();
            $auth_id = $model->id;
        }
        // 教师档案数据同步//fa_user_teacher
        $plist = \app\common\model\User::where('id','in',explode(',', $input['user_ids']))
            ->field('id,mobile')
            ->select();
        $sonList =  \app\common\model\User::where('id','in',explode(',', $input['user_auth_ids']))
            ->field('id,mobile')
            ->select();
        $mobileStr = '';
        foreach($sonList as $v){
            if(!empty($v['mobile'])){
                $mobileStr .= $v['mobile'].',';
            }
        }
        $mobileStr = trim($mobileStr,',');

        foreach($plist as $v){
            if($mobileStr && $v['mobile']){
                $params = [
                    'is_all' => $is_all,
                    'mobile' => $mobileStr,
                    'auth_id'=>$auth_id,
                ];
                $result = (new \app\common\model\UserTeacher)->auth($v['mobile'],$params);
            }
        }

       $this->success('保存成功','');
    }

    public function detail()
    {
        $id = input('id');
        if(empty($id)){
             $this->error('id必传');
        }
        $model = TeacherAuthModel::get($id);
        $users = MeetUserModel::where('id','in',explode(',',$model['user_ids']))->field('wx_userid as id,nickname,avatar')->select();
        $model->user_ids = implode(',',array_column($users,'id'));
        $model->user_ids_user = $users;
        $users = MeetUserModel::where('id','in',explode(',',$model['user_auth_ids']))->field('wx_userid as id,nickname,avatar')->select();
        $model->user_auth_ids = implode(',',array_column($users,'id'));
        $model->user_auth_ids_user = $users;
        $this->success('成功',$model);
    }

    public function del()
    {
        $id = input('id');
        $row = TeacherAuthModel::where('id',$id)->delete();
        Db::table('fa_user_teacher')->where('auth_id',$id)->delete();
        $this->success('成功',$row);
    }

    public function zujiDetail()
    {
        $row = Db::table('fa_footprint_examine_user')->where('deletetime', 'EXP','IS NULL')->field('user_id')->select();
        $user_ids = array_column($row,'user_id');
        $user = MeetUserModel::where('id','in',$user_ids)
            ->field('wx_userid as id,nickname,avatar')
            ->select();
        $this->success('成功',$user);
    }

    public function saveZuji()
    {
        //$id = input('id');
        $input = input();
        $input['user_ids'] = Format::formatInput($input['user_ids']);
        $input['user_ids'] = Format::getUserByWxid($input['user_ids']);
        $user_ids_arr = explode(',',$input['user_ids']);
        //print_R($user_ids_arr);
        Db::table('fa_footprint_examine_user')->where('id > 0')->delete();
        $list = [];
        foreach ($user_ids_arr as $k=>$v){
            $list[$k]['user_id'] = $v;
            $list[$k]['createtime'] = time();
            $list[$k]['updatetime'] = time();
        }
        Db::table('fa_footprint_examine_user')->insertAll($list);
        $this->success('保存成功','');
    }
}
