<?php

namespace app\api\controller;

use app\api\model\DevModel;
use app\api\model\MeetConfigModel;
use app\api\model\MeetModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetPlanModel;
use app\api\model\MeetSignatureModel;
use app\api\model\MeetSignModel;
use app\api\model\MeetUserModel;
use app\api\service\Format;
use app\common\controller\Api;
use EasyWeChat\Factory;

/**
 * 示例接口
 */
class Demo extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['test', 'test1'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['test2'];

    /**
     * 测试方法
     *
     * @ApiTitle    (测试名称)
     * @ApiSummary  (测试描述信息)
     * @ApiMethod   (POST)
     * @ApiRoute    (/api/demo/test/id/{id}/name/{name})
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="integer", required=true, description="会员ID")
     * @ApiParams   (name="name", type="string", required=true, description="用户名")
     * @ApiParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据")
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据返回")
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功'
        })
     */
    public function test()
    {
        MeetModel::getAllMeetUserId2(466);

        die();
        $input = MeetSignModel::get(157);

        $info = MeetNoticeModel::sendMeetNotice($input,"meet_sign",[]);
        die();
        $str = '[{"ToUserName":"ww377c91a81493c595","FromUserName":"sys","CreateTime":"1665511374","MsgType":"event","Event":"sys_approval_change","AgentID":"3010040","ApprovalInfo":{"SpNo":"202210120004","SpName":"请假","SpStatus":"1","TemplateId":"3Tmn1z9yDRPyMf9t2Le1DY8saMk72xNZ4WcPrqu9","ApplyTime":"1665511361","Applyer":{"UserId":"RenYong","Party":"1"},"SpRecord":[{"SpStatus":"2","ApproverAttr":"1","Details":{"Approver":{"UserId":"17637502443"},"Speech":[],"SpStatus":"2","SpTime":"1665511373"}},{"SpStatus":"2","ApproverAttr":"1","Details":{"Approver":{"UserId":"17637502443"},"Speech":[],"SpStatus":"2","SpTime":"1665511373"}},{"SpStatus":"2","ApproverAttr":"1","Details":{"Approver":{"UserId":"17637502443"},"Speech":[],"SpStatus":"2","SpTime":"1665511373"}},{"SpStatus":"1","ApproverAttr":"1","Details":{"Approver":{"UserId":"18820268653"},"Speech":[],"SpStatus":"1","SpTime":"0"}}],"StatuChangeEvent":"2"}}]';
        $arr = json_decode($str,true);
        print_r($arr);
        die();
        MeetModel::setTips15(422);
        die();
        $meet_id = 262;
        $meet = MeetModel::get($meet_id);

        $user_ids = MeetSignModel::where('status in(3)')
            ->where(['meet_id'=> $meet_id])
            ->column('user_id');
        print_r($user_ids);
        $t = strtotime($meet['start_time']);
        $t = date('Y-m-d',$t);
        $late_time = explode('-',$meet['normal_time']);
        if(!empty($late_time[1])){
            $lateTime = $t.' '.$late_time[1];
            echo $lateTime;
            $lateTime = strtotime($lateTime); //最晚签到时间
        }else{
            $lateTime = strtotime($meet['start_time']);
        }
        if(time() > $lateTime){
            $uids = [];
            $m = 0;
            //判断是否超过签到时间
            $userids = MeetModel::getAllMeetUserId($meet_id);
            foreach($userids as $v){
                $mp = [
                    'meet_id' => $meet_id,
                    'user_id' => $v,
                ];
                $row = MeetSignModel::where($mp)->find();
                if(empty($row)){
                    $uids[$m] = $v;
                    $m++;
                }
            }
            $user_ids = array_merge($user_ids,$uids);
        }
        print_r($user_ids);
        die();
        $paht = pathinfo('https://huiyi.bigchun.com/uploads/20220913/b9b56b981f14ca40825592223ad50264.png');
        $paht = 'https://huiyi.bigchun.com/uploads/20220913/b9b56b981f14ca40825592223ad50264.png';
        echo strstr($paht,'/uploads');



        die();
       /* $map = [
            'meet_id' => input('meet_id'),
            'cangsuo' => $v['cangsuo'],
            'date_text' => $v['date_text'],
            'use_time' => $v['use_time'],
        ];*/
      /*  $map = [
            'meet_id' => 401,
            'department_id' => $input['department_id'],
            'user_id' => $user['id'],
            'date_text' => $input['date_text'],
            'use_time' => $input['use_time'],
        ];*/
        $map = [
            'meet_id' => 409,
            'cangsuo' => '传达室',
            'date_text' => '2022-09-30',
            'use_time' => '00:00-00:30',
        ];
        echo $row = MeetPlanModel::isChongtu($map,115);

        die();
        $dev = DevModel::get(4);
        echo $dev['domain'];die();
        die();
        MeetModel::setTopicTips(391);
        die();
        $meet = MeetModel::get(392);
        //党总支会议，发给所有人
        $user_ids = '';

        $user_ids .= $meet['user_id'];
        $user_ids .= ','.$meet['check_ids'];
        $user_ids .= ','.$meet['attend_meet_ids'];
        $user_ids .= ','.$meet['issued_ids'];
        if(!empty($meet['attend_topic_ids']) && $meet['type'] == 1){
            $user_ids .= ','.$meet['attend_topic_ids'];
        }
        $ids_arr = explode(',',$user_ids);
        print_r($ids_arr);
        print_r(array_unique($ids_arr));

        die();
      /*  $config = MeetConfigModel::getMsgConfigByMeetId(318);
        print_r($config);
        die();
       $str = 'ChunChangZhang,SunXueChun01,GaoLianJie,HaHa';*/
        $input['attend_meet_ids'] = 'wo9YL4EQAAJI-fSbg-niXJHnJWMkvw_A,wo9YL4EQAAZmKONLqF1fjVvr7dgT8VLg,wo9YL4EQAAuEubuWA2RIe17Y25o4A3Xg,wo9YL4EQAAK0xE-K8U4ZDZtsR5PxjurQ,wo9YL4EQAAzLHDTHnUQ1EeNW0uZAz_pQ,wo9YL4EQAACGanPo0LkSUPilsi5qy2EQ';
        $input['attend_meet_ids'] = Format::formatInput($input['attend_meet_ids']);
        $input['attend_meet_ids'] = Format::getUserByWxid($input['attend_meet_ids']);
       print_r( $input['attend_meet_ids']);
      die();

        $str = '56,55,54,52,51,50,41,39,37,36,35,30,20,11,10,9,8,7,6,5,4,3,2,1';
        $str = '56';
        /* $list = \app\common\model\User::where('wx_userid','in',explode(',',$str))->column('id');
        print_r($list);*/
        echo Format::formatInput($str);

        die();
        //给会议室管理员发消息
        //$touser = MeetUserModel::where("id", "in", $input['check_room_ids'])->select();
        $toArr = [
            //'wo9YL4EQAAZmKONLqF1fjVvr7dgT8VLg',
            '18820268653'
        ];
      /*  $news['user_id'] = $input['check_room_ids'];
        $news['user'] = implode(',', $toArr);
        $news['title'] = "审核会议室：" . $input['title'];
        $news['content'] = '' . getTypeName($input['type_id']) . ' | 地点：' . $room["title"] . $room["floor"] . $room["number"] . " <br/>" . $start_time;*/
        $arr = [
            'title' => '会议标题',
            'description' => '会议内容',
            'url' => request()->domain().'/#/pages/tabbar/tabbar-4/tabbar-4',
        ];
        $res = Wechat::sendMessage($arr, $toArr);
       /* $input = MeetModel::get();
        $info = MeetNoticeModel::sendMeetNotice($input,"room_order",[]);*/
        /*$file = MeetSignatureModel::get(115);
        if(!empty($file)){

            $input['file'] = $file['files']['fullurl'];
            print_r($input);
        }
        die();
        $config = [

        ];*/
        /*$app = Factory::openWork($config);
        $app->offsetGet();*/
        //$this->success('返回成功', $this->request->param());
    }

    /**
     * 无需登录的接口
     *
     */
    public function test1()
    {
        $this->success('返回成功', ['action' => 'test1']);
    }

    /**
     * 需要登录的接口
     *
     */
    public function test2()
    {
        $this->success('返回成功', ['action' => 'test2']);
    }

    /**
     * 需要登录且需要验证有相应组的权限
     *
     */
    public function test3()
    {
        $this->success('返回成功', ['action' => 'test3']);
    }

}
