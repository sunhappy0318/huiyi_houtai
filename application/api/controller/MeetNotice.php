<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetCheckModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetReceiveModel;
use app\api\model\MeetRoomModel;
use app\api\model\MeetSummaryModel;
use app\api\model\MeetTopicModel;
use app\api\model\MeetTypeModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\api\model\room\RoomOrderModel;
use app\api\model\room\RoomTimesModel;
use app\api\model\room\RoomWeekModel;
use app\api\model\TopicM;
use app\common\controller\Api;
use think\Db;

class MeetNotice extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    /**
     * 通知列表，查找关于自己的消息
     */
    public function lists()
    {
        $limit     = input('limit') ? input('limit') : '15';
        $day       = input('day') ? input('day') : '0';
        $title     = input('title') ? input('title') : '';
        $search    = !empty($title) ? " title like '%" . $title . "%' " : "";
        $meet_type = input('meet_type') == 1 ? 1 : 0;
        $map       = ""; //0--all，1--today，2--yestoday 3---week
        if ($day == 1) {
            $map = " createtime > " . strtotime(date("Y-m-d"));
        } else if ($day == 2) {
            $map = " createtime > " . strtotime(date("Y-m-d", strtotime("-1 day")));
        } else if ($day == 3) {
            $map = " createtime > " . strtotime(date("Y-m-d", strtotime("-7 day")));
        }
        $list = MeetNoticeModel::where($map)
            ->field("link_id,type,notice_type,title,content,from_id,createtime,meet_id,status")
            ->where($search)
            ->where('meet_type', $meet_type)
            ->where('find_in_set(' . $this->auth->id . ',user_id)')
            ->whereNull('deletetime')
            ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$map) {
                $res                = MeetUserModel::where("id", "in", $item['from_id'])->field("id,nickname,username,avatar")->find();
                $item['start_time'] = date("m-d H:i", $item['createtime']) . ' ';
                $item['createtime'] = date("m-d H:i", $item['createtime']);
                $item['user']       = $res;
                if ($item['type'] == "invite") {
                    $item['str'] = $item['user']['username'] . "邀请你参加会议";
                }
                if ($item['type'] == "room") {
                    $item['str'] = $item['user']['username'] . "邀请你审核会议室";
                }
                if ($item['type'] == "meet") {
                    $item['str'] = $item['user']['username'] . "邀请你审核会议";
                }
                if ($item['type'] == "topic") {
                    $item['str'] = $item['user']['username'] . "邀请你审核会议议题";
                }
                if ($item['type'] == "summary") {
                    $item['str'] = $item['user']['username'] . "邀请你审核会议纪要";
                }
                $item['bohui_content'] = '';
                if (strpos($item['content'], '<br/>驳回理由') !== false) {
                    $item['bohui_content'] = strstr($item['content'], '<br/>驳回理由');
                    $item['content']       = strstr($item['content'], '<br/>驳回理由', true);
                }
                //if(strpos($item['content'])){}
                $item['content']       = str_replace('<br/>', '', $item['content']);
                $item['bohui_content'] = str_replace('<br/>', '', $item['bohui_content']);

                $meet      = MeetModel::get($item['meet_id']);
                $meet_time = '';
                if ($meet) {
                    $meet_time = date('m月d日', strtotime($meet['start_time']));
                    $meet_time .= ' 周' . return_number(date('N', strtotime($meet['start_time'])));
                    $s         = date('H:i', strtotime($meet['start_time']));
                    $e         = date('H:i', strtotime($meet['end_time']));
                    $meet_time .= ' ' . $s . '-' . $e;
                }
                $item['meet_time'] = $meet_time;
                unset($item['from_id']);
                return $item;
            })
            ->toArray();
        $this->success('成功', $list);
    }


    /** 待进行列表
     不同的会员都可以看到，只是不同的身份展示不同而已
     */
    public function lists_wait()
    {
        $limit = input('limit') ? input('limit') : '15';
        $day = input('day') ? input('day') : '0';
        $title = input('title') ? input('title') : '';
        $search = !empty($title ) ? " title like '%".$title."%' " :"";
        $map = ""; //0--all，1--today，2--yestoday 3---week
        if($day ==  1){
            $map = " createtime > ".strtotime(date("Y-m-d"));
        }else if($day ==  2){
            $map = " createtime > ".strtotime(date("Y-m-d",strtotime("-1 day")));
        }else if($day ==  3){
            $map = " createtime > ".strtotime(date("Y-m-d",strtotime("-7 day")));
        }
        $meet_type = input('meet_type') == 1?1:0;
        //状态:1=已经通过, 2=待审核 ,3= 驳回
       /* if(input('status') == 1){
            $status['status'] = ['in',"1"];
        }else if(input('status') == 2){
            $status['status'] = ['in',"2,3"];
        }else if(input('status') == 3){
            $status['status'] = ['in',"21,31"];
        }else{
            $status['status'] = ['in', "2,3"];
        }*/
        /*'find_in_set(' . $this->auth->id. ',user_id)
            or find_in_set(' . $this->auth->id. ',issued_ids)
            or find_in_set(' . $this->auth->id. ',check_room_ids)
            or find_in_set(' . $this->auth->id. ',attend_meet_ids)
            or find_in_set(' . $this->auth->id. ',attend_topic_ids)
            or find_in_set(' . $this->auth->id. ',check_ids) '*/
        $status['status'] = ['in',"1"];
        $list = MeetModel::where($map)
            ->where($status)
            ->where($search)
            ->where('meet_type',$meet_type)
            ->where('find_in_set(' . $this->auth->id. ',user_id) 
            or find_in_set(' . $this->auth->id. ',issued_ids) 
            or find_in_set(' . $this->auth->id. ',attend_meet_ids) 
            or find_in_set(' . $this->auth->id. ',attend_topic_ids) 
            or find_in_set(' . $this->auth->id. ',check_ids) ')
            ->whereNull('deletetime')
            ->field("id,user_id,title,room_id,type_id,start_time,end_time,status,issued_ids,check_room_ids,attend_meet_ids,attend_topic_ids,check_ids,approver_ids")
            ->order("id desc") //attend_meet_ids,approver_ids,check_ids,attend_topic_ids
            ->paginate($limit)->each(function ($item, $key) use (&$map) {
                $item['action'] = MeetModel::getAdminActionName($item,$this->auth->id);
                $item['room'] = RoomModel::where( "id", "in",$item['room_id']  )
                    ->field("id,title,floor,people")
                    ->find();
                $item['user'] =  MeetUserModel::where( "id", "in",$item['user_id']  )
                    ->field("id,nickname,username,avatar")
                    ->find();
                $item['status_name'] = getStatusName($item['status'],$item['start_time']);

                $item['type_name'] = getTypeName($item['type_id']);
                $item['time']  =  date("m月d日", strtotime($item['start_time'])) ."  ". getWeekName($item['start_time']) ."  ".date("H:i", strtotime($item['start_time']))."-". date("H:i", strtotime($item['end_time']));


                unset($item['type_id'] );
                unset($item['room_id'] );
                return $item;
            })
            ->toArray();
        $this->success('成功', $list);
    }


    // 待审批的列表 （会议室管理员  会议初判人  和 会议审核人员  可以查看到）
    public function lists_check()
    {
        $limit = input('limit') ? input('limit') : '15';
        //$day = input('day') ? input('day') : '0';
        //$title = input('title') ? input('title') : '';
        //$search = !empty($title ) ? " title like '%".$title."%' " :"";

        /*$map = "";
        if($day ==  1){
            $map = " createtime > ".strtotime(date("Y-m-d"));
        }else if($day ==  2){
            $map = " createtime > ".strtotime(date("Y-m-d",strtotime("-1 day")));
        }else if($day ==  3){
            $map = " createtime > ".strtotime(date("Y-m-d",strtotime("-7 day")));
        }*/
        $meet_type = input('meet_type') == 1?1:0;
        //$user = $this->auth->getUser();
        //$where = "";
        $status = "";
        if (input('status') == 1) {
            $status = ' ( find_in_set(' . $this->auth->id. ',check_room_ids) and status in (1,3,31,8,9) ) or ( find_in_set(' . $this->auth->id. ',issued_ids)  and is_jiyao = 2 )  or ( find_in_set(' . $this->auth->id. ',check_ids) and is_yiti = 2 )';
        } else if (input('status') == 2) {
            $status = ' ( find_in_set(' . $this->auth->id. ',check_room_ids) and status=2 ) or ( find_in_set(' . $this->auth->id. ',issued_ids)  and is_jiyao = 1 )  or ( find_in_set(' . $this->auth->id. ',check_ids) and is_yiti = 1 )';
        } else if (input('status') == 3) {
            $status = ' ( find_in_set(' . $this->auth->id. ',check_room_ids) and status=21 ) or ( find_in_set(' . $this->auth->id. ',issued_ids)  and is_jiyao = 3 )  or ( find_in_set(' . $this->auth->id. ',check_ids) and is_yiti = 3 )';
        }
        /*
         * ' find_in_set(' . $this->auth->id. ',check_room_ids)
            or find_in_set(' . $this->auth->id. ',approver_ids)
            or find_in_set(' . $this->auth->id. ',issued_ids)
            or find_in_set(' . $this->auth->id. ',check_ids) '*/
        //echo $status;die();

        $list = MeetModel::where($status)
            ->where('meet_type', $meet_type)
            ->field("id,user_id,title,room_id,type_id,start_time,end_time,status,issued_ids,check_room_ids,attend_meet_ids,attend_topic_ids,check_ids,approver_ids,is_yiti as is_yiti_status,status,is_jiyao")
            ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$map) {
                $item['room']        = RoomModel::where("id", "in", $item['room_id'])->field("id,title,floor,people")->find();
                $item['status_name'] = getStatusName($item['status'], $item['start_time']);
                $item['type_name']   = getTypeName($item['type_id']);
                $item['time']        = date("m月d日", strtotime($item['start_time'])) . "  " . getWeekName($item['start_time']) . "  " . date("H:i", strtotime($item['start_time'])) . "-" . date("H:i", strtotime($item['end_time']));
                $item['topic']       = TopicM::where('meet_id', $item['id'])->select();
                return $item;
            })->toArray();

        # 目的是不给校长看那些没有预审过的(pre_check=0)议题
        $new_data = array_filter($list['data'], function ($item) {
            if (empty($item['topic'])) {
                return false;
            }
            if ($this->auth->id == $item['check_ids']) {
                foreach ($item['topic'] as $value) {
                    $flag = false;
                    if ($value['pre_check'] == 1) {
                        $flag = true;
                    }
                    return $flag;
                }
            }
            return true;
        });
        usort($new_data, function ($a, $b) {
            return $a['id'] > $b['id'] ? -1 : 1;
        });
        $list['data'] = $new_data;

        $this->success('成功', $list);
    }


    // 待进行列表
    public function lists_wait2()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        $map['status'] = 2;
        $list = MeetModel::where($map)->where('find_in_set(' . $this->auth->id. ',attend_meet_ids)')->whereNull('deletetime')
            ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$map) {
                $res =   MeetUserModel::where( "id", "in",$item['user_id']  )->field("id,nickname,username,avatar")->find();
                $item['user'] = $res;
                return $item;
            })
            ->toArray();
        $this->success('成功', $list);
    }


    // 待审批的列表
    public function lists_check2()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        $map['status'] = ['in',"2,21,3,31"];
        $list = MeetModel::where($map)->where('find_in_set(' . $this->auth->id. ',attend_meet_ids)')->whereNull('deletetime')
            ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$map) {
                $res =   MeetUserModel::where( "id", "in",$item['user_id']  )->field("id,nickname,username,avatar")->find();
                $item['user'] = $res?$res:[];
                return $item;
            })
            ->toArray();
        $this->success('成功', $list);
    }

}
