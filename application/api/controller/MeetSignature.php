<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetCheckModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetReceiveModel;
use app\api\model\MeetRoomModel;
use app\api\model\MeetSignatureModel;
use app\api\model\MeetSignModel;
use app\api\model\MeetSummaryModel;
use app\api\model\MeetTopicModel;
use app\api\model\MeetTypeModel;
use app\api\model\MeetUserModel;
use app\common\controller\Api;
use think\Db;

class MeetSignature extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    // 我的签章列表
    public function lists()
    {
            $limit = input('limit') ? input('limit') : '15';
            $map = [];
            if (!empty(input('status'))) {
                $map = [
                    'status' => input('status'),
                ];
            }
           $map['user_id'] =   $this->auth->id;
            //$map = "";
            /*$list = MeetSignatureModel::where($map)->field("id,files")
                ->whereNull('deletetime')
                ->order("id desc")
            ->paginate($limit)->each(function ($item, $key) use (&$meet) {
                    $item['files'] = json_decode(str_replace('&quot;', '"', $item['files']), true);
                    return $item;
                })
                ->toArray();*/
        $list = MeetSignatureModel::where($map)
            ->field("id,files")
            ->whereNull('deletetime')
            ->limit(3)
            ->order('id desc')
            ->select();
            $this->success('成功', $list);
        }


    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetSignatureModel::detail($id);
        $this->success('成功', $info);
    }

    public function add()
    {
        $input = input();

      /*  $checkFile  = getCheckFiles(input('files'),true);
        if (empty($checkFile['status'])) {
            $this->error($checkFile['msg']);
        }*/
        if(empty($input['files'])){
             $this->error('签名图片必传');
        }
        if (!empty($input['files'])) {
            $input['files'] = htmlspecialchars_decode($input['files']);
        }
        $input['user_id'] = $this->auth->id;
        $model = new MeetSignatureModel($input);
        $row = $model->allowField(true)->save();
        $this->success('添加成功', $model);
    }


/*
 * 电子签名
 * */
    public function sign_meet()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $meet = MeetModel::get($input['meet_id']);
        if (!$meet) {
            $this->error('会议不存在');
        }
        $input['type'] = "sign";
        $input['user_id'] = $this->auth->id;

        $check_ids = explode(",", $meet['attend_topic_ids']);
        if (!in_array($this->auth->id, $check_ids)) {
            $this->error('你没有权限签章');
        }
        $model = new MeetCheckModel($input);
        $res = $model->where(['type' => 'sign','meet_id' => $input['meet_id'], 'user_id' => $this->auth->id])->find();
        if (!$res) {
            $row = $model->allowField(true)->save();
            $this->success('签章成功');
        } else {
            $this->error('已经签章');
        }
    }




    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        $model = new MeetSignatureModel();
        $row = $model->allowField(true)->save($input, ['id' => $input['id'], 'user_id' => $this->auth->id]);
        $this->success('修改成功', $row);
    }

    public function del()
    {
        $input = input();
        $model = new MeetSignatureModel();
        $row = $model->where(['id' => $input['id'], 'user_id' => $this->auth->id])->delete();
        if ($row) {
            $this->success('删除成功', $row);
        } else {
            $this->error('删除失败');
        }
    }

}
