<?php


namespace app\api\controller;


use app\api\model\FootprintArticleModel;
use app\api\model\MeetModel;
use app\api\model\MeetReceiveModel;
use app\api\model\MeetRecordModel;
use app\api\model\MeetUserModel;
use app\common\controller\Api;

class Record extends Api
{
    protected $noNeedLogin = [''];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    //会议记录列表
    public function lists()
    {
        $meet_id = input('meet_id');
        if(!$meet_id){
            $this->error('meet_id必传');
        }
        $map = ['meet_id' => $meet_id];
        //$map ='';
        $res = MeetRecordModel::where($map)->paginate(100)->toArray();
        foreach($res['data'] as &$v){
            $v['user'] = MeetUserModel::getUser($v['user_id']);
        }
        $this->success('成功',$res);
    }

    public function detail()
    {
        $user = $this->auth->getUser();
        $input = input();
        $meet_id = input('meet_id');
        $meet = MeetModel::get($meet_id);
        if(!$meet){
            $this->error('会议不存在');
        }
        $res = MeetRecordModel::where(['meet_id' => $input['meet_id'],'user_id' => $user['id']])->find();
        if(empty($res)){
            $this->success('记录详情',[]);
        }
        $res = $res->toArray();
        $res['user'] = [
            'avatar' => $user['avatar'],
            'nickname' => $user['nickname']
        ];
        $this->success('记录详情',$res);
    }

    public function add()
    {
        $user = $this->auth->getUser();
        $input = input();

        $meet_id = input('meet_id');
        $meet = MeetModel::get($meet_id);
        if(!$meet){
            $this->error('会议不存在');
        }
        if (empty($input['title'])) {
            $this->error('请填写标题');
        }
        if (empty($input['content'])) {
            $this->error('请填写内容');
        }
        if(!empty($input['images'])){
            $input['images'] = htmlspecialchars_decode($input['images']);
        }
        $res = MeetRecordModel::where(['meet_id' => $input['meet_id'],'user_id' => $user['id']])->find();
        if ($res) {
            $model = new MeetRecordModel();
            $row = $model->allowField(true)->save($input,['id'=> $res['id']]);
        } else {
            $input['user_id'] = $this->auth->id;
            $model = new MeetRecordModel($input);
            $row = $model->allowField(true)->save();
            $res = $model;
        }
        if($input['is_sys'] == 1){
            $foot = FootprintArticleModel::where('record_id',$res['id'])->find();
            $images = json_decode($input['images'],true);
            $img = [];
            foreach($images as $k=>$v){
                $img[$k] = $v['fullurl'];
            }
            if($foot){
                $upadte = [
                    'title' => $input['title'],
                    'content' => $input['content'],
                    'status' => 0,
                    'diagram' => json_encode($img,JSON_UNESCAPED_UNICODE),
                ];
                $model2 = new FootprintArticleModel();
                $model2->allowField(true)->save($upadte, ['id' => $foot['id']]);
            }else{
                $upadte = [
                    'title' => $input['title'],
                    'content' => $input['content'],
                    'status' => 0,
                    'record_id' => $res['id'],
                    'user_id' => $user['id'],
                    'diagram' => json_encode($img,JSON_UNESCAPED_UNICODE),
                ];
                $model2 = new FootprintArticleModel($upadte);
                $model2->allowField(true)->save();
            }
        }
        $this->success('保存成功');
    }
}
