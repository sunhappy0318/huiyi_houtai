<?php


namespace app\api\controller;


use app\api\model\CorpModel;
use app\api\model\MeetConfigModel;
use app\api\model\WorkModel;
use app\common\controller\Api;
use app\common\library\WXBizMsgCrypt;
use fast\Random;

class Notify extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    //会议模板回调
    public function meet()
    {
        $receiveid = "ww377c91a81493c595";
        $token = "22cxk5syNm1hopzv";
        $encodingAesKey = "LQsNjHYVosRRbsZiAwa3yVsVPd4iC1kKRI1P9Qi7Dnl";
        $postStr = file_get_contents("php://input");
        if(!empty($postStr)){
            $receiveid = 'dkd5562c6d77493873';
        }
        //$receiveid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';
        //log_print(input());
        if(!empty($postStr)){
            //刷新suite_ticket
            return CorpModel::DecryptMsgSave($receiveid,$token,$encodingAesKey,$postStr);
        }
        return CorpModel::verify($receiveid,$token,$encodingAesKey,'meet----');
    }

    //open_corpid
    public function test()
    {
        //转换open_corpid
        /*$provider_access_token = WorkModel::get_provider_token();
        $corpid = 'wwa7b1de1fc0574d54';
        $row = WorkModel::corpid_to_opencorpid($provider_access_token,$corpid);*/

        // 获取永久授权
        /*$row = CorpModel::where("SuiteId",'dkd5562c6d77493873')->find();
        $auth_code = 'qKrmwiYtNOs_D_bAfoPallDtUR5C2Cc4Xps2ZHa7RRccQMucYteIp_k2Zlggta2l1iJtZRqH-ulj3fwSIeD-mRRFq7GjHBlJrDvydY5k0aU';
        $suite_access_token = WorkModel::get_suite_token($row['SuiteId'],$row['suite_secret'],$row['suite_ticket']);
        WorkModel::get_permanent_code($suite_access_token,$auth_code,$row['SuiteId']);*/
        $provider_access_token = WorkModel::get_provider_token();
        $corpid = 'wwa7b1de1fc0574d54';
        $row = WorkModel::corpid_to_opencorpid($provider_access_token,$corpid);
        print_r($row);
    }

    //清外足迹 模板回调
    public function qingwai()
    {
        $receiveid = "ww377c91a81493c595";
        $token = "22cxk5syNm1hopzv";
        $encodingAesKey = "LQsNjHYVosRRbsZiAwa3yVsVPd4iC1kKRI1P9Qi7Dnl";
        $postStr = file_get_contents("php://input");
        if(!empty($postStr)){
            $receiveid = 'dk23a82931e1d80158';
        }
        //$receiveid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';  //代开发应用，扫码授权之后配置回调要验证，提交上线的时候调用
        //log_print(input());
        if(!empty($postStr)){
            //刷新suite_ticket
            return CorpModel::DecryptMsgSave($receiveid,$token,$encodingAesKey,$postStr);
        }
        return CorpModel::verify($receiveid,$token,$encodingAesKey,'qingwai----');
    }

    //清外教师成长档案
    public function dangan()
    {
        $receiveid = "ww377c91a81493c595";
        $token = "22cxk5syNm1hopzv";
        $encodingAesKey = "LQsNjHYVosRRbsZiAwa3yVsVPd4iC1kKRI1P9Qi7Dnl";
        $postStr = file_get_contents("php://input");
        if(!empty($postStr)){
            $receiveid = 'dk7720843a21547e51';
        }
        //$receiveid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';
        //log_print(input());
        if(!empty($postStr)){
            //刷新suite_ticket
            return CorpModel::DecryptMsgSave($receiveid,$token,$encodingAesKey,$postStr);
        }
        return CorpModel::verify($receiveid,$token,$encodingAesKey,'meet----');
    }

    //清外工资
    public function gongzi()
    {
        $receiveid = "ww377c91a81493c595";
        $token = "22cxk5syNm1hopzv";
        $encodingAesKey = "LQsNjHYVosRRbsZiAwa3yVsVPd4iC1kKRI1P9Qi7Dnl";
        $postStr = file_get_contents("php://input");
        if(!empty($postStr)){
            $receiveid = 'dkc2215b629ee9855c';
        }
        //$receiveid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';
        //log_print(input());
        if(!empty($postStr)){
            //刷新suite_ticket
            return CorpModel::DecryptMsgSave($receiveid,$token,$encodingAesKey,$postStr);
        }
        return CorpModel::verify($receiveid,$token,$encodingAesKey,'gongzi----');
    }

    //清外活动系统
    public function huodong()
    {
        $receiveid = "ww377c91a81493c595";
        $token = "22cxk5syNm1hopzv";
        $encodingAesKey = "LQsNjHYVosRRbsZiAwa3yVsVPd4iC1kKRI1P9Qi7Dnl";
        $postStr = file_get_contents("php://input");
        if(!empty($postStr)){
            $receiveid = 'dkd965a9186b1de72c';
        }
        //$receiveid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';  //代开发应用，扫码授权之后配置回调要验证，提交上线的时候调用
        //log_print(input());
        if(!empty($postStr)){
            //刷新suite_ticket
            return CorpModel::DecryptMsgSave($receiveid,$token,$encodingAesKey,$postStr);
        }
        return CorpModel::verify($receiveid,$token,$encodingAesKey,'gongzi----');
    }


    public function zhuanhuan()
    {
        $agentId = '1000105';
        $config = CorpModel::where('AgentId',$agentId)
        ->field('id,app_name,AgentId,open_corpid,permanent_code')
        ->find();
        $corpid = $config['open_corpid'];
        $secret = $config['permanent_code'];
        $AgentId = $config['AgentId'];
        $api_data = MeetConfigModel::getAccessToken($corpid,$secret);
        $access_token = $api_data['access_token'];
        //$url = 'https://qyapi.weixin.qq.com/cgi-bin/batch/openuserid_to_userid?access_token='.$access_token.'&debug=1';
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/batch/userid_to_openuserid?access_token='.$access_token.'&debug=1';

        $option = [
            "userid_list"=>["lihaole", "renyongtao001",'E_12_115135900','sunxuechun001']
            //'open_userid_list' => ['wo9YL4EQAACGanPo0LkSUPilsi5qy2EQ','wo9YL4EQAAzLHDTHnUQ1EeNW0uZAz_pQ'],
            //'source_agentid' => $AgentId,
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $list = json_decode($req, true);
         print_R($list);
    }

    public function zhuanhuan2()
    {
        /*$row = CorpModel::where("SuiteId",'dkd5562c6d77493873')->find();
        $suite_access_token = WorkModel::get_suite_token($row['SuiteId'],$row['suite_secret'],$row['suite_ticket']);

        $access_token = WorkModel::get_corp_token($suite_access_token,'dkd5562c6d77493873');
        echo '$access_token';
        echo $access_token;*/
        $agentId = '1000105';
        $config = CorpModel::where('AgentId',$agentId)
            ->field('id,app_name,AgentId,open_corpid,permanent_code')
            ->find();
        $corpid = $config['open_corpid'];
        $secret = $config['permanent_code'];
        $AgentId = $config['AgentId'];
        $api_data = MeetConfigModel::getAccessToken($corpid,$secret);
        $access_token = $api_data['access_token'];
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/batch/openuserid_to_userid?access_token='.$access_token.'&debug=1';;
        $option = [
            "userid_list"=>["lihaole", "renyongtao001",'E_12_115135900','sunxuechun001']
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $list = json_decode($req, true);
        print_R($list);
    }
    //用户同步

    public function  usersys()
    {
        $config = MeetConfigModel::config();
        $corpid = $config['corpid'];
       // $corpid = 'wp9YL4EQAArAx6VhvB6j2TKXubt-ot9A';
        $secret = $config['Secret'];
        $secret = 'wrieS_MfpVjtlgrmKQWsEFdD7VS8CUgtnp4BrFXpXK0'; //通讯录独立的secret
        //$secret = 'Zfl9QUb6TIpbRORbsq4l6BrIuwvyumKMBFxqM3popck'; //客户的
        $AgentId = $config['AgentId'];
        $api_data = MeetConfigModel::getAccessToken($corpid,$secret);

        $url  = 'https://qyapi.weixin.qq.com/cgi-bin/user/list_id?access_token='.$api_data['access_token'];
        $option = [
            'cursor' => '',
            'limit' => '500',
        ];
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'json' => $option,
        ]);
        $req = $res->getBody()->getContents();
        $list = json_decode($req, true);
        foreach ($list['dept_user'] as $vv){

            $map = [
                'wx_userid' => $vv['userid']
            ];
            $user = \app\common\model\User::where($map)->find();
            if(!$user){
                $ret = $this->auth->register($vv['userid'], Random::alnum(), '', '',[]);
            }
        }
        die();
        //print_R($list);die();
       /* print_R($list);
        $list = [
            'JinZhi',
            'SunXueChun01',
            'ZhangAnDao',
            'ChunChangZhang',
            'LouRuiJie',
            'HuFaXin',
        ];
        $userid = 'JinZhi';
        $url  = 'https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token='.$api_data['access_token'].'&userid='.$userid;
        $client = new \GuzzleHttp\Client();
        $result = $client->get($url)->getBody()->getContents();
        $apiUser = json_decode($result, true);
        print_R($apiUser);
        die();*/
        foreach($list['dept_user'] as $v){
            $map = [
                'username' => $v['userid']
            ];
            $user = \app\common\model\User::where($map)->find();
            if(!$user['wx_userid']){
                $url  = 'https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token='.$api_data['access_token'].'&userid='.$v['userid'];
                $client = new \GuzzleHttp\Client();
                $result = $client->get($url)->getBody()->getContents();
                $apiUser = json_decode($result, true);
                print_R($apiUser);
                $ext = [
                    'avatar' => $apiUser['avatar'],
                    'nickname' => $apiUser['name'],
                    'username' => $apiUser['userid'],
                    'wx_userid' => $apiUser['userid'],
                ];
                $userModel = new \app\common\model\User();
                $userModel->allowField(true)->save($ext,['id' => $user['id']]);
            }else{
                echo $v['userid'].PHP_EOL;
            }
        }


    }

    public function zhuanhuan3()
        {

            $access_token = 'dSt_tNTlmkBqRSviHg5GgueIq39IZg4z_P7jV6PiZ6C3pP8zFkhoQaRm-D1o-dku9Adbvd6gFGJkua9B_zLZmS8JVz_vDNvoxDO2p8d3GMBdyGhjP6V31aArP9_i1RiMY542mFqDc5rJNScszPx-RtD0pCsqU9jMHNoljuWk40hJE9nk5M6y7OAv7ICOAYpKENJlIHU1xbyyU6lZwkdmiA';
            $url = 'https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token='.$access_token.'&debug=1';

            $option = [
                "openid"=>'wo9YL4EQAAZmKONLqF1fjVvr7dgT8VLg'
            ];
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                'json' => $option,
            ]);
            $req = $res->getBody()->getContents();
            $list = json_decode($req, true);
             print_r($list);
        }


    /*
     * Array
(
    [errcode] => 0
    [errmsg] => ok
    [userid] => JinZhi
    [name] => 槿栀
    [department] => Array
        (
            [0] => 1
        )

    [position] =>
    [mobile] => 18637750743
    [gender] => 1
    [email] =>
    [avatar] => https://wework.qpic.cn/wwpic/156577_HArq6RQzTyeUDNX_1656897354/0
    [status] => 5
    [isleader] => 0
    [extattr] => Array
        (
            [attrs] => Array
                (
                )

        )

    [telephone] =>
    [enable] => 1
    [hide_mobile] => 0
    [order] => Array
        (
            [0] => 0
        )

    [main_department] => 1
    [qr_code] => https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vcfea7a520d13a6755
    [alias] =>
    [is_leader_in_dept] => Array
        (
            [0] => 0
        )

    [thumb_avatar] => https://wework.qpic.cn/wwpic/156577_HArq6RQzTyeUDNX_1656897354/0
    [direct_leader] => Array
        (
        )

    [biz_mail] => jinzhi@xckj230.wecom.work
)*/
    ///通讯录接口的数据
    /**
     * Array
    (
    [errcode] => 0
    [errmsg] => ok
    [next_cursor] => AA85gc2g07LT7FqZiWXTZqYDXGAAeUNE2XCHx9bJWgc
    [dept_user] => Array
    (
    [0] => Array
    (
    [userid] => 17637502443
    [department] => 1
    )

    [1] => Array
    (
    [userid] => 18820268653
    [department] => 1
    )

    [2] => Array
    (
    [userid] => JinZhi
    [department] => 1
    )

    [3] => Array
    (
    [userid] => LiangYongHu
    [department] => 1
    )

    [4] => Array
    (
    [userid] => GaoLianJie
    [department] => 1
    )

    [5] => Array
    (
    [userid] => HaHa
    [department] => 1
    )

    [6] => Array
    (
    [userid] => MeiYouRuGuo
    [department] => 1
    )

    [7] => Array
    (
    [userid] => liwenqi
    [department] => 1
    )

    [8] => Array
    (
    [userid] => SunXueChun01
    [department] => 1
    )

    [9] => Array
    (
    [userid] => ZhangAnDao
    [department] => 1
    )

    )

    )

     */
}
