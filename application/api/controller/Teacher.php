<?php


namespace app\api\controller;

use app\common\model\User;
use app\admin\model\Teacher as TeacherMod;
use app\common\controller\Api;
use think\Db;

class Teacher extends Api
{
    // 无需登录的接口,*表示全部
    // protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $name = input('name') ? input('name') : '';
        $department_id = input('department_id') ? input('department_id') : '';

        $where = [];

        if (!empty($name)) {
            $where['t.name']= ['like',"%{$name}%"];
        }
        if (!empty($department_id)) {
            $where['u.department_id']=$department_id;
        }
        if (!$this->is_all()) {//不能查看所有
            $list = User::alias('u')
                ->field('`t`.name,`u`.nickname,`t`.gender,`t`.age,`t`.department,`t`.subject,`t`.id')
                ->join(['fa_user_teacher'=>'ut'],'ut.pmobile = u.mobile','left')
                ->join(['fa_user'=>'up'],'up.mobile = ut.mobile','right')
                ->join(['fa_teacher'=>'t'],'t.mobile = up.mobile','right')
                ->where($where)
                ->where('u.id','=',$this->auth->id)
                // ->fetchSql(true)
                // ->find();
                ->paginate($limit)
                ->toArray();
        }else{
            $list = User::alias('u')
                ->field('`u`.nickname,`t`.name,`t`.gender,`t`.age,`t`.department,`t`.subject,`t`.id')
                ->join(['fa_teacher'=>'t'],'t.mobile = u.mobile','right')
                ->where($where)
                // ->fetchSql(true)
                // ->find();
                ->paginate($limit)
                ->toArray();
        }
        $this->success('成功', $list);
    }
    /**
     * [me 个人]
     * @since  2022-08-26
     * @return [type]     [description]
     */
    public function me()
    {
        $user = $this->auth->getUser();
        $mobiles = User::alias('u')
                ->field('`t`.*')
                ->where('u.id',$this->auth->id)
                // ->where('u.mobile',$row->mobile)
                ->join(['fa_teacher'=>'t'],'t.mobile = u.mobile','right')
                // ->fetchSql(true)
                ->find();
        if(!empty($mobiles)){
            $mobiles['is_jiao'] = $user['is_jiao'];
            $mobiles['score'] = $user['score'];
        }else{
            $mobiles['is_jiao'] = $user['is_jiao'];
            $mobiles['score'] = $user['score'];
        }
        $this->success('成功', $mobiles);
    }
    /**
     * [is_all 是否能查看所有]
     * @since  2022-08-26
     * @return boolean    [description]
     */
    protected function is_all()
    {
        return User::alias('u')
            ->field('`u`.`id`')
            ->where('u.id',$this->auth->id)
            ->where('ut.is_all',1)
            ->join(['fa_user_teacher'=>'ut'],'ut.pmobile = u.mobile','left')
            ->find();
    }
    /**
     * [detail 详情]
     * @since  2022-08-26
     * @return [type]     [description]
     */
    public function detail()
    {
        $id = input('id') ? input('id') : '0';
        $where = [];
        $where['t.id']=$id;

        if (!$this->is_all()) {//不能查看所有
            $list = User::alias('u')
                ->field('`t`.*')
                ->join(['fa_user_teacher'=>'ut'],'ut.pmobile = u.mobile','left')
                ->join(['fa_user'=>'up'],'up.mobile = ut.mobile','right')
                ->join(['fa_teacher'=>'t'],'t.mobile = up.mobile','left')
                ->where($where)
                ->where('u.id','=',$this->auth->id)
                // ->fetchSql(true)
                // ->find();
                ->find();
        }else{
            $list = User::alias('u')
                ->field('`t`.*')
                ->join(['fa_teacher'=>'t'],'t.mobile = u.mobile','left')
                ->where($where)
                // ->fetchSql(true)
                // ->find();
                ->find();
        }
        $this->success('成功', $list);
    }
    /**
     * [department 部门列表]
     * @since  2022-08-26
     * @return [type]     [description]
     */
    public function department()
    {
        $limit = input('limit') ? input('limit') : '15';
        $list = Db::table('fa_meet_department')
            ->field('`md`.wx_id as department_id,`md`.name as `name`')
            ->alias('md')
            ->paginate($limit)
            ->toArray();;
        // var_dump($user);exit;

        // $list = User::alias('u')
            // ->field('`md`.wx_id as department_id,`md`.name as `name`,`u`.id')
            // ->join(['fa_meet_department'=>'md'],'md.wx_id = `u`.department_id','right')
            // ->whereNotNull('u.department_id')
            // ->group('`u`.department_id')
            // ->paginate($limit)
            // ->toArray();
        $this->success('成功', $list);
    }
    /*
     * 重新提交
     * */
    public function edit()
    {
        $input = input();
        $mobiles = User::alias('u')
            ->field('`t`.id')
            ->where('u.id',$this->auth->id)
            // ->where('u.mobile',$row->mobile)
            ->join(['fa_teacher'=>'t'],'t.mobile = u.mobile','right')
            // ->fetchSql(true)
            ->find();
        if (empty($input['id']) || $mobiles['id'] != $input['id']) {
            $this->error('只可以编辑个人的');
        }

        $info = (new TeacherMod)->edit_teacher($input);
        if ($info !== false) {
            $this->success( '修改成功');
        } else {
            $this->error(  '修改失败');
        }
    }
}
