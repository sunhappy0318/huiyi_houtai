<?php

namespace app\api\controller;

use app\api\library\buiapi\Api;

class MeetTopicTemplate extends Api{

    protected $model = null;

	protected $noNeedRight = '*';
	protected $noNeedLogin = '*';
	protected $_allow_func = ['index'];


	use \app\api\library\buiapi\traits\Api;

    public function _initialize(){
        parent::_initialize();
        $this->model = new \app\api\model\MeetTopicTemplate;
	}

	    /**
     * 列表
     */
    public function index(){
      /*  $this->request->filter('trim,strip_tags,xss_clean');
        list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        $list = $this->model->where($where)->order($sort, $order)->paginate($limit);
        foreach ($list as $row) {
            $row->visible(['id','title','content','createtime']);

        }
		$list = $this->__handle_index__($list);
        return $this->success('数据列表',$list);*/
      $list = $this->model->where("")->field('id,title,content')->select();
      $this->success('成功',$list);
    }
}
