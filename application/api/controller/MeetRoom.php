<?php


namespace app\api\controller;

use addons\mrbs\model\Edifice;
use addons\mrbs\model\RoomWeeks;
use app\api\controller\room\Room;
use app\api\model\MeetModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomEdificeModel;
use app\api\model\room\RoomModel;
use app\api\model\room\RoomOrderModel;
use app\api\model\room\RoomTimesModel;
use app\api\model\room\RoomWeekModel;
use app\common\controller\Api;
use think\Db;
use think\Response;

class MeetRoom extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['info'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    /**
     * 获取大厦列表
     * @return [type] [description]
     */
    public function edifice()
    {
        $year  = $this->request->param('dyearay', date('Y'));
        $month = $this->request->param('month', date('m'));
        $day   = $this->request->param('day', date('d'));
        // 大厦列表
        $edifice = Edifice::order('id desc')->select();
        $week    = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        $month_arr = get_month();
        foreach ($edifice as &$value) {
            // 最近七天房间情况
            $dates = [];
            for ($i = 0; $i < 7; $i++) {
                $ids    = RoomModel::where('edifice_id', $value['id'])->where('status', 1)->column('id');
                $status = RoomWeeks::where('room_id', 'in', $ids)
                    ->where('week', $i + 1)
                    ->count();
                $time    = strtotime($year . '-' . $month . '-' . $day) + $i * 86400;
                $dates[] = [
                    'day'      => date('j', $time),
                    'week'     => $i + 1,
                    'month' => $month_arr[$i],
                    'weekName' => ($day + $i) == date('d') ? '今天' : $week[date('w', $time)],
                    'status'   => $status ? 1 : 0,
                ];
                $value['date'] = $dates;
            }
        }

        return $this->success("查询成功", $edifice);
    }

    // 列表
    public function lists()
    {
        $day = input('date') ? input('date') : date("Y-m-d");

        $year       = $this->request->param('year', date('Y'));
        $month      = $this->request->param('month', date('m'));
        $day        = $this->request->param('day', date('d'));
        $edifice_id = $this->request->param('edifice_id');
        $week       = date('w', strtotime($year . '-' . $month . '-' . $day));
        // 查询时间
        $id_arr = RoomWeekModel::where('week', $week)->select();
        // 获取房间列表
        $rooms = RoomModel::where('id', 'in', array_column($id_arr, 'room_id'))
            ->field("*")
            ->select();
        foreach ($rooms as &$value) {
            $value['edifice'] =  RoomEdificeModel::where('id', $value['edifice_id'])->field('*')->find();
            $value['room_type_str'] = $value['room_type'] == 0 ?'固定': '临时';
            $value['manager'] =  MeetUserModel::where('id', $value['manager_id'])->field("nickname,avatar,id")->find();
           // unset($value['edifice']);
            unset($value['edifice_id']);
            unset($value['images']);
            unset($value['status_text']);
            unset($value['Status']);
            unset($value['room_type_text']);
            unset($value['facilities']);
            unset($value['content']);
            unset($value['weigh']);
            unset($value['manager_id']);
            unset($value['status']);
            unset($value['is_verify']);
            $value['site'] = $value['title'];

        }
        return $this->success("查询成功", $rooms);
    }



    // 预定
    public function add()
    {
        $user = $this->auth->getUser();
        $input = input();
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];
        $info = RoomOrderModel::saveData($input) ;
        if ($info['status'] == 1) {
            $this->success($info['msg']);
        } else {
            $this->error(  $info['msg']);
        }
    }


    /**
     * 房间详情
     * @return [type] [description]
     */
    public function info()
    {
        $year  = $this->request->param('year', date('Y'));
        $month = $this->request->param('month', date('m'));
        $day   = $this->request->param('day', date('d'));
        $id    = $this->request->param('id');
        $room  =RoomModel::where('id', $id)->find();
        // 查询房间预订情况
        $orders      = RoomOrderModel::where('room_id', $room['id'])->where('status', 'neq', -1)->where('starttime', '>', time() - 86400)->field('starttime,endtime')->select();
        $order_times = [];
        foreach ($orders as $order) {
            //每小时分成四份显示
            $step = 15 * 60;
            for ($i = $order['starttime']; $i < $order['endtime']; $i = $i + $step) {
                //获取时间刻钟
                $h = strtotime(date('Y-m-d H:00:00', $i));
                for ($j = 3; $j >= 0; $j--) {
                    if ($i - $h >= $j * $step) {
                        $order_times[] = date('n-j-G', $i) . '-' . $j;
                        break;
                    }
                }
            }
        }
        // 获取开放时间
        $times  = RoomTimesModel::field('time')->where('room_id', $room['id'])->select();
        $_times = [];
        foreach ($times as $time) {
            for ($i = 0; $i < 4; $i++) {
                $_times[$time['time']][] = !in_array($month . '-' . $day . '-' . $time['time'] . '-' . $i, $order_times);
            }
        }
        $room['times'] = $_times;

        return $this->success("查询成功", $room);
    }

    // 详情
    public function detail()
    {
        $id = input('id');
        if (empty($id)) {
            $this->error('请填写会议室');
        }
        $input = input();
        $input['user_id'] = $this->auth->id;
        $day = input('date') ? input('date') : date("Y-m-d");
        $info = RoomModel::detail($input,$day);
        $this->success('成功', $info);
    }

}
