<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetTypeModel;

use app\api\model\MeetUserModel;
use app\api\model\room\RoomOrderModel;
use app\common\controller\Api;
use think\Db;

class Meet extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    // 列表
    public function lists()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map['status'] = input('status');
        }
        if (!empty(input('user_id'))) {
            $map['user_id'] = input('user_id');
        }
        $list = MeetModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }


    // 我的列表发布的
    public function lists_mine()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map['status'] = input('status');
        }
         $map['user_id'] =$this->auth->id;
        $list = MeetModel::where($map)->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }


    // 我参会的列表
    public function lists_attend()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map['status'] = input('status');
        }
        $list = MeetModel::where($map)->where('find_in_set(' . $this->auth->id. ',attend_meet_ids)')->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }


    // 我相关的列表
    public function lists_about()
    {
        $limit = input('limit') ? input('limit') : '15';
        $map = [];
        if (!empty(input('status'))) {
            $map['status'] = input('status');
        }
     /*   check_room_ids` varchar(255) DEFAULT NULL COMMENT '会议室管理人员ID',
  `check_ids` varchar(255) DEFAULT NULL COMMENT '初判人IDS',
  `attend_meet_ids` text COMMENT '参会人员ID',
  `attend_topic_ids` text COMMENT '议题参与人',
  `approver_ids` text COMMENT '审批人ID'
  */
        $list = MeetModel::where($map)->where('find_in_set(' . $this->auth->id. ',check_room_ids) or find_in_set(' . $this->auth->id. ',check_ids) 
          or find_in_set(' . $this->auth->id. ',attend_meet_ids)  or find_in_set(' . $this->auth->id. ',attend_topic_ids) or find_in_set(' . $this->auth->id. ',approver_ids)' )->whereNull('deletetime')
            ->paginate($limit)
            ->toArray();
        $this->success('成功', $list);
    }


    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetModel::detail(['id'=>$id,'user_id'=> $this->auth->id]);
        $this->success('成功', $info);
    }



    // 克隆
    public function clone()
    {
        $id = input('id');
        $input = MeetModel::get($id)->toArray();
        unset($input['id']);
        unset($input['start_time']);
        unset($input['files']);
        $input['user_id'] = $this->auth->id;
        $input['status'] = 2;
        $model = new MeetModel($input);
        $row = $model->allowField(true)->save();
        $this->success('克隆成功', $row);
    }

    public function add()
    {
        $user = $this->auth->getUser();
        $input = input();
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];
        $input['status'] = 2;
        $info = MeetModel::checkTime(['room_id' => $input["room_id"],'start_time' => $input["start_time"],'end_time' => $input["end_time"],]);

      if ($info['status'] == 1) {
          $input['check_room_ids'] = $info['data']['manager_id'];
            $model =  new MeetModel($input);
            $row = $model->allowField(true)->save();
            $input['meet_id'] = $model->getLastInsID();
            //给另一张表增加数据
            $info = RoomOrderModel::saveData($input);
            $this->success('添加成功', $row);
        } else {
            $this->error( $info['msg'],$info);
        }

    }


    /*
     * 重新提交
     * */
    public function edit()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $input['status'] = 2;
        $info = MeetModel::edit($input);
        if ($info['status'] == 1) {
            $this->success(  $info['msg']);
        } else {
            $this->error(  $info['msg']);
        }
    }

    /*
     * 修改基本的三项信息
     * */
    public function edit_info()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $data = ['room_id' => $input["room_id"],'start_time' => $input["start_time"],'id' => $input["id"],'user_id' => $input["user_id"],];
        $info = MeetModel::edit($data);
        if ($info['status'] == 1) {
            $this->success(  $info['msg']);
        } else {
            $this->error(  $info['msg']);
        }
    }


    public function edit_status()
    {
        $input = input();
        $input["user_id"] = $this->auth->id;
        $model = MeetModel::get( $input['id']);
        if (in_array($input["status"], [ 2,3,9,8,0])) {
            $data = ['status' => $input["status"] ,'id' => $input["id"],'user_id' => $input["user_id"]];
            $info = MeetModel::edit_status($data);
            if ($info['status'] == 1) {
                $this->success(  $info['msg']);
            } else {
                $this->error(  $info['msg']);
            }
        }  else  {
            $this->error('修改状态不对');
        }
    }

    public function delete()
    {
        $input = input();
        $model = new MeetModel();
        // $row = $model->where(['id' => $input['id'],'user_id' =>  $this->auth->id])->delete();
        $row = MeetModel::destroy($input['id']);
        if ($row) {
            $this->success('删除成功', $row);
        } else {
            $this->error('删除失败');
        }
    }


    public function types()
    {
        $input = input();
        $list = MeetTypeModel::where("1=1")->group("type")->select();

        $this->success('成功', $list);
    }


    //初始化数据
    public function form()
    {
        $input = input();
        $list = MeetTypeModel::where("1=1")->group("type")->select();
        foreach ($list as $k => &$val) {
            $val['check_ids_user'] =  MeetUserModel::where( "id", "in",$val['check_ids']  )->field("id,nickname,username,avatar")->select();
            $val['attend_meet_ids_user'] =  MeetUserModel::where( "id", "in",$val['attend_meet_ids']  )->field("id,nickname,username,avatar")->select();
            $val['attend_topic_ids_user'] =  MeetUserModel::where( "id", "in",$val['attend_topic_ids']  )->field("id,nickname,username,avatar")->select();
            $val['approver_ids_user'] =  MeetUserModel::where( "id", "in",$val['approver_ids']  )->field("id,nickname,username,avatar")->select();
            $val['issued_ids_user'] =  MeetUserModel::where( "id", "in",$val['issued_ids']  )->field("id,nickname,username,avatar")->select();
        }
        $data['typelist'] = $list ;
        $data['tip'] = [   "late_time"  =>'12:10',
  "normal_time"  =>'12:10',
  "normal_tip"  =>'正常提示语',
   "late_tip"  =>'迟到提示语',
   "absence_tip" => '缺勤提示',] ;
        $this->success('成功', $data);
    }
}
