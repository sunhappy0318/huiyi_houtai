<?php


namespace app\api\controller;

use app\api\model\MeetModel;
use app\api\model\MeetCheckModel;
use app\api\model\MeetNoticeModel;
use app\api\model\MeetReceiveModel;
use app\api\model\MeetSummaryModel;
use app\api\model\MeetTopicModel;
use app\api\model\MeetTypeModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use app\common\controller\Api;
use think\Db;

class MeetCheck extends Api
{
// 无需登录的接口,*表示全部
    protected $noNeedLogin = ['cate'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    // 列表
    public function lists()
    {
        $input = input();
        $list = MeetCheckModel::lists($input);
        $this->success('成功', $list);
    }


    // 详情
    public function detail()
    {
        $id = input('id');
        $info = MeetCheckModel::detail($id);
        $this->success('成功', $info);
    }

    public function add()
    {
        $input = input();
        $meet_id = input('meet_id');
        $model = MeetModel::get($meet_id);
        if (!$model) {
            $this->error('会议不存在');
        }
        if (!empty(input('status'))) {
            $input['status'] = 1;
        }
        $input['user_id'] = $this->auth->id;
        $model = new MeetCheckModel($input);
        $row = $model->allowField(true)->save();
        $this->success('添加成功', $row);
    }

    /*
     * `check_room_ids` varchar(255) DEFAULT NULL COMMENT '会议室管理人员ID',
      `check_ids` varchar(255) DEFAULT NULL COMMENT '初判人IDS',
      `attend_meet_ids` text COMMENT '参会人员ID',
      `attend_topic_ids` text COMMENT '议题参与人',
      `approver_ids` text COMMENT '审批人ID',
    */
    public function room()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $meet = MeetModel::get($input['meet_id']);
        if (!$meet) {
            $this->error('会议不存在');
        } elseif ($meet['status'] != 2) {
            $this->error('会议状态不对');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }

        $checkRole = MeetModel::checkActionRole($meet, $this->auth->id, 'check_room');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }

        $info = ['status' => 0, 'msg' => ''];
        Db::startTrans();
        try {
            //////////
            /// 修改会议的状态
            if (input('status') == 1) {
                //因为党支部会议直接是校长同意的，所以不需要校长再审核会议了，直接同意
                (new MeetModel())->allowField(true)->save(['status' => 1], ['id' => $input['meet_id']]);
            } else {
                (new MeetModel())->allowField(true)->save(['status' => 21], ['id' => $input['meet_id']]);
            }
            $input['type'] = "room";
            $input['user_id'] = $this->auth->id;
            $input['nickname'] = $user['nickname'];
            $model = new MeetCheckModel($input);
            $row = $model->allowField(true)->save();
            $input['check_id'] = $model->getLastInsID();

            $info = MeetNoticeModel::sendMeetNotice($input, "room_check", $meet);

            /////////
            Db::commit();

        } catch (\Exception $e) {
            Db::rollback();
            $info = ['status' => 0, 'msg' => '审核失败!' . $e->getMessage()];
        }
        if ($info['status'] == 1) {
            $this->success($info['msg']);
        } else {
            $this->error($info['msg']);
        }
    }

    //发送会议通知
    public function sendNotice()
    {
        $meet_id = input('meet_id');

        $meet = MeetModel::get($meet_id);
        $user = $this->auth->getUser();
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];


        $info = MeetNoticeModel::sendMeetNotice($input, "meet_notice", $meet);
        $this->success('发送成功','');
    }

    public function order()
    {
        $input = input();
        $user = $this->auth->getUserinfo();
        $model = RoomModel::get($input['room_id']);
        if (!$model) {
            $this->error('会议室不存在');
        }
        $checkRole = MeetModel::checkActionRole($model, $this->auth->id, 'check_room');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }

        $model2 = MeetModel::get($input['meet_id']);
        if (!$model2) {
            $this->error('会议不存在');
        } elseif ($model2['status'] != 2) {
            $this->error('会议状态不对');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }


        $input['type'] = "room";
        $input['user_id'] = $this->auth->id;

        $model = new MeetCheckModel($input);
        Db::startTrans();
        try {
            $row = $model->allowField(true)->save();
            //审核通过的转入下一个流程 改为  3 状态:1=已经通过,2=待审批 （审核会议室） ,3=待审批 （校长审核审核会议）,21=会议室驳回,31=校长驳回，0=草稿
            if (input('status') == 1) {
                (new MeetModel())->allowField(true)->save(['status' => 3], ['id' => $input['meet_id']]);
                $arr = [
                    'title' => '同意了您的预定会议室',
                    'description' => ' 请请前往查看',
                    'url' => 'https://huiyi.bigchun.com/api/wechat/test'
                ];
            } else {
                (new MeetModel())->allowField(true)->save(['status' => 21], ['id' => $input['meet_id']]);
                $arr = [
                    'title' => '驳回了您的预定会议室',
                    'description' => $input['content'] . '请请前往审核',
                    'url' => 'https://huiyi.bigchun.com/api/wechat/test'
                ];
            }
            Db::commit();

            //给管理员发消息
            // $res =  Wechat::sendMessage($arr,$toArr);

        } catch (\Exception $e) {
            Db::rollback();
            $this->error('审核失败' . $e->getMessage());
        }
        $this->success('审核成功');
    }


    public function meet()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $meet = MeetModel::get($input['meet_id']);
        if (!$meet) {
            $this->error('会议不存在');
        } elseif ($meet['status'] != 3 && $meet['status'] != 1) {
            $this->error('会议状态不对');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }

        $checkRole = MeetModel::checkActionRole($meet, $this->auth->id, 'check');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }

        $input['type'] = "meet";
        $input['user_id'] = $this->auth->id;

        $check_ids = explode(",", $meet['check_ids']);
        if (!in_array($this->auth->id, $check_ids)) {
            $this->error('你没有权限审核');
        }
        $model = new MeetCheckModel($input);
        $input['check_id'] = $model->getLastInsID();

        Db::startTrans();
        try {
            $row = $model->allowField(true)->save();
            //审核不通过的修改状态 改为  31 状态:1=已经通过,2=待审批 （审核会议室） ,3=待审批 （校长审核审核会议）,21=会议室驳回,31=校长驳回，0=草稿
            if (input('status') == 1) {
                (new MeetModel())->allowField(true)->save(['status' => 1], ['id' => $input['meet_id']]);
            } else {
                (new MeetModel())->allowField(true)->save(['status' => 31], ['id' => $input['meet_id']]);
            }

            $info = MeetNoticeModel::sendMeetNotice($input, "meet_check", $meet);

            /////////
            Db::commit();

        } catch (\Exception $e) {
            Db::rollback();
            $info = ['status' => 0, 'msg' => '审核失败!' . $e->getMessage()];
        }
        if ($info['status'] == 1) {
            $this->success($info['msg']);
        } else {
            $this->error($info['msg']);
        }
    }

    public function topic()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $model2 = MeetTopicModel::get($input['topic_id']);
        if (!$model2) {
            $this->error('会议主题不存在');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }
        $input['type'] = "topic";
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];
        $meet = MeetModel::get($model2['meet_id']);


        /*$checkRole = MeetModel::checkActionRole($meet, $this->auth->id, 'approver');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }*/
        Db::startTrans();
        try {
            //审核不通过的修改状态 改为  31 状态:1=已经通过,2=待审批 （审核会议室） ,3=待审批 （校长审核审核会议）,21=会议室驳回,31=校长驳回，0=草稿
            if (input('status') == 1) {
                (new MeetTopicModel())->allowField(true)->save(['status' => 1], ['id' => $input['topic_id']]);

                //是否还有议题要审核，否，就更新为已通过
                $smap = [
                    'meet_id' => $meet['id'],
                    'status' => 2,
                ];
                $count = MeetTopicModel::where($smap)->count();
                if($count == 0){
                    $meet->is_yiti = 2;
                    $meet->save();
                }
            } else {
                (new MeetTopicModel())->allowField(true)->save(['status' => 3], ['id' => $input['topic_id']]);
                $meet->is_yiti = 3;
                $meet->save();
            }
            $model = new MeetCheckModel($input);
            $row = $model->allowField(true)->save();
            $input['check_id'] = $model->getLastInsID();

            $info = MeetNoticeModel::sendMeetNotice($input, "topic_check", $meet);

            /////////
            Db::commit();

        } catch (\Exception $e) {
            Db::rollback();
            $info = ['status' => 0, 'msg' => '审核失败!' . $e->getMessage()];
        }
        if ($info['status'] == 1) {
            $this->success($info['msg']);
        } else {
            $this->error($info['msg']);
        }
    }


    public function vote()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $model2 = MeetTopicModel::get($input['topic_id']);
        if (!$model2) {
            $this->error('会议主题不存在');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }
        $input['type'] = "vote";
        $input['user_id'] = $this->auth->id;
        $input['nickname'] = $user['nickname'];
        $meet = MeetModel::get($model2['meet_id']);
        if (!$meet) {
            $this->error('会议不存在');
        }
/*
        $checkRole = MeetModel::checkActionRole($meet, $this->auth->id, 'attend_meet');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }*/

        Db::startTrans();
        try {
            //审核不通过的修改状态 改为  31 状态:1=已经通过,2=待审批 （审核会议室） ,3=待审批 （校长审核审核会议）,21=会议室驳回,31=校长驳回，0=草稿
           /* if (input('status') == 1) {
                (new MeetTopicModel())->allowField(true)->save(['status' => 1], ['id' => $input['topic_id']]);
            } else {
                (new MeetTopicModel())->allowField(true)->save(['status' => 3], ['id' => $input['topic_id']]);
            }*/

            $model = new MeetCheckModel($input);
            $row = $model->allowField(true)->save();
            $input['check_id'] = $model->getLastInsID();

            $info = MeetNoticeModel::sendMeetNotice($input, "topic_check", $meet);

            /////////
            Db::commit();

        } catch (\Exception $e) {
            Db::rollback();
            $info = ['status' => 0, 'msg' => '审核失败!' . $e->getMessage()];
        }
        if ($info['status'] == 1) {
            $this->success($info['msg']);
        } else {
            $this->error($info['msg']);
        }
    }

    public function summary()
    {
        $user = $this->auth->getUserinfo();
        $input = input();
        $model2 = MeetSummaryModel::get($input['summary_id']);
        if (!$model2) {
            $this->error('会议纪要不存在');
        }
        if (empty($input['status'])) {
            if (empty($input['content'])) {
                $this->error('请输入审核意见');
            }
        }
        $meet = MeetModel::get($model2['meet_id']);

        $checkRole = MeetModel::checkActionRole($meet, $this->auth->id, 'issued');
        if ($checkRole['status'] == 0) {
            $this->error($checkRole['msg']);
        }

        $input['type'] = "summary";
        $input['meet_id'] = $model2['meet_id'];
        $input['user_id'] = $this->auth->id;

        Db::startTrans();
        try {
            //审核不通过的修改状态 改为  31 状态:1=已经通过,2=待审批 （审核会议室） ,3=待审批 （校长审核审核会议）,21=会议室驳回,31=校长驳回，0=草稿
            if (input('status') == 1) {
                (new MeetSummaryModel())->allowField(true)->save(['status' => 1], ['id' => $input['summary_id']]);
                $meet->is_jiyao = 2;

            } else {
                (new MeetSummaryModel())->allowField(true)->save(['status' => 3], ['id' => $input['summary_id']]);
                $meet->is_jiyao = 3;
            }
            $meet->save();
            $model = new MeetCheckModel($input);
            $row = $model->allowField(true)->save();
            $input['check_id'] = $model->getLastInsID();

            $info = MeetNoticeModel::sendMeetNotice($input, "summary_check", $meet);

            /////////
            Db::commit();

        } catch (\Exception $e) {
            Db::rollback();
            $info = ['status' => 0, 'msg' => '审核失败!' . $e->getMessage()];
        }
        if ($info['status'] == 1) {
            $this->success($info['msg'],$info);
        } else {
            $this->error($info['msg']);
        }
    }


    public function edit()
    {
        $user = $this->auth->getUser();
        $input = input();
        $model = new MeetCheckModel();
        $row = $model->allowField(true)->save($input, ['id' => $input['id'], 'user_id' => $this->auth->id]);
        $this->success('修改成功', $row);
    }

    public function delete()
    {
        $input = input();
        $model = new MeetCheckModel();
        $row = $model->where(['id' => $input['id'], 'user_id' => $this->auth->id])->delete();
        if ($row) {
            $this->success('删除成功', $row);
        } else {
            $this->error('删除失败');
        }
    }

}
