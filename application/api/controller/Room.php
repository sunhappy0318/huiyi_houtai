<?php


namespace app\api\controller;


use app\api\model\room\RoomModel;
use app\api\service\Format;
use app\common\controller\Api;

class Room extends Api
{
    protected $noNeedLogin = [''];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    public function config()
    {
        $suser = $this->auth->getUserinfo();
        $data = [
            'is_confg' => $suser['is_config'],
        ];
        $this->success('成功',$data);
    }

    //列表
    public function lists()
    {
        $suser = $this->auth->getUserinfo();
        $list = RoomModel::where("")
            ->field('id,name,title,status,room_type,manager_id,people,floor,number,weigh')
            ->order('id desc')
            ->paginate()
            ->toArray();
        foreach ($list['data'] as &$val){
            $user = \app\common\model\User::get($val['manager_id']);
            $val['nickname'] = $user?$user['nickname']:'';
            $val['people'] = $val['people'].'人';
            $val['time_text'] = '9:30 - 18:30';
            $val['status_text'] =  $val['status']==1?'开放':'关闭';
            $val['room_type_text'] = $val['room_type']==1?'临时':'固定';
        }
        $list['is_config'] = $suser['is_config'];
        $this->success('成功',$list);
    }

    public function detail()
    {
        $id = input('id');
        $val = RoomModel::where('id',$id)->field('id,name,title,status,room_type,manager_id,people,floor,number,weigh')->find();
        if($val){
            $val= $val->toArray();
            $user = \app\common\model\User::get($val['manager_id']);
            $val['wx_userid'] = $user?$user['wx_userid']:'';
            $val['nickname'] = $user?$user['nickname']:'';
            $val['avatar'] = $user?$user['avatar']:'';
            $val['people'] = $val['people'];
            $val['time_text'] = '9:30 - 18:30';
            $val['status_text'] =  $val['status']==1?'开放':'关闭';
            $val['room_type_text'] = $val['room_type']==1?'临时':'固定';
        }
        $this->success('成功',$val);
    }

    public function edit()
    {
        $input = input();

        $id = (int)input('id');
        if(empty($id)){
            $this->error('id必传');
        }
        if(empty($input['name'])){
            $this->error('请输入会议名称');
        }
        if(empty($input['title'])){
            $this->error('请输入会议地点');
        }
        if(empty($input['manager_id'])){
            $this->error('请选择管理员');
        }
        if(!empty($input['manager_id'])){
            $input['manager_id'] = Format::formatInput($input['manager_id']);
            $user = \app\common\model\User::get($input['manager_id']);
            if($user && $user['is_check_room'] != 1){
                $user->is_check_room = 1;
                $user->save();
            }
        }
        $model = new RoomModel();
        $model->allowField(true)->save($input,['id' => $input['id']]);

        $this->success('保存成功');
    }

}
