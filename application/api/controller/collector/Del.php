<?php

namespace app\api\controller\collector;


use app\api\model\CollectorM;
use app\common\controller\Api;

class Del extends Api
{
    public function index()
    {
        $input = input();
        if (empty($input['id'])) {
            $this->error('必传收集者id');
        }

        $user = CollectorM::find($input['id']);
        if (!$user) {
            $this->error('无效的收集者id');
        }

        $destroy = CollectorM::destroy([
            'user_id' => $input['id']
        ]);

        $this->success('', get_defined_vars());
    }
}