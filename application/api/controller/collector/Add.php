<?php

namespace app\api\controller\collector;


use app\api\model\CollectorM;
use app\api\model\UserM;
use app\common\controller\Api;

class Add extends Api
{
    public function index()
    {
        $input = input();
        if (empty($input['id'])) {
            $this->error('必传用户id');
        }

        $user = UserM::find($input['id']);
        if (!$user) {
            $this->error('无效的用户id');
        }

        $exist = CollectorM::where('user_id', $input['id'])->find();
        if ($exist) {
            $this->error('当前用户已经是收集者,不必再重复添加');
        }

        $create = CollectorM::create([
            'user_id' => $input['id']
        ]);

        $this->success('', get_defined_vars());
    }
}