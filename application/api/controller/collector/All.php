<?php

namespace app\api\controller\collector;


use app\api\model\CollectorM;
use app\common\controller\Api;

class All extends Api
{
    public function index()
    {
        $collectors = CollectorM::with('profile')->select();

        $this->success('', get_defined_vars());
    }
}