<?php

namespace app\admin\controller;

//use app\common\controller\Backend;
//use app\common\model\Attachment;
//use fast\Date;
//use think\Db;
use think\Config;
use think\Controller;

class Wechat extends  Controller{
    public function index()
    {

    }
    //获取token的url
   public function getTokenUrl(): string
   {
        //企业id
        $corpId = Config::get("wechat.corpId");
        //secret
        $corpSecret = Config::get("wechat.corpSecret");
        //请求token的url
        $tokenUrl = Config::get("wechat.getTokenUrl")."corpid=".$corpId."&corpsecret=".$corpSecret;
       // echo $tokenUrl;
        return $tokenUrl;
   }
   public  function getToken(): string
   {

       return "";

   }

}
