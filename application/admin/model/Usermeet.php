<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Usermeet extends Model
{

    use SoftDelete;



    // 表名
    protected $name = 'meet';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'status_text',
        'start_time_text',
        'end_time_text',
        'sign_way_text',
        'late_time_text',
        'normal_time_text',
        'append_topic_submit_off_time'
    ];

    public function getAppendTopicSubmitOffTimeAttr($value, $data)
    {
        $value = $data['topic_submit_off_time'] ?? '';
        $value = (int)$value;
        if ($value && strlen($value) == 10) {
            return date('Y-m-d H:i:s', $value);
        }else{
            return '-';
        }
    }


    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }


    public function getStatusList()
    {
        return [ '2' => __('Status 2'),'1' => __('Status 1'), '3' => __('Status 3'), '21' => __('Status 21'), '31' => __('Status 31'), '8' => __('Status 8'),'9' => __('Status 9'), '-1' => __('Status -1')];
    }

    public function getSignWayList()
    {
        return ['0' =>'无须签到','1' => __('Sign_way 1'), '2' => __('Sign_way 2')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStartTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['start_time']) ? $data['start_time'] : '');
        return $value;
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['end_time']) ? $data['end_time'] : '');
        return $value;
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getSignWayTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sign_way']) ? $data['sign_way'] : '');
        $list = $this->getSignWayList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['late_time']) ? $data['late_time'] : '');
        return $value;
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getNormalTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['normal_time']) ? $data['normal_time'] : '');
        return $value;
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

  /*  protected function setStartTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setLateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setNormalTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }*/


    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function type()
    {
        return $this->belongsTo('app\admin\model\meet\Type', 'type', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function room()
    {
        return $this->belongsTo('app\admin\model\mrbs\Room', 'room_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
