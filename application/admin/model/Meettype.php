<?php

namespace app\admin\model;

use app\api\model\UserM;
use think\Model;
use think\model\relation\BelongsTo;
use traits\model\SoftDelete;

class Meettype extends Model
{

    //use SoftDelete;



    // 表名
    protected $name = 'meet_type';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
    ];


    public function getTypeList()
    {
        return [
            '1' => __('Type1'),
            '2' => __('Type2'),
            '3' => __('Type3'),
            '4' => __('Type4'),
            '5' => __('Type5'),
            '6' => __('Type6'),
        ];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function collector(): BelongsTo
    {
        return $this->belongsTo(UserM::class,'shouji_ids','id');
    }




}
