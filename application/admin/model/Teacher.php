<?php

namespace app\admin\model;

use think\Model;


class Teacher extends Model
{

    

    

    // 表名
    protected $name = 'teacher';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'working_time_text',
        'entry_time_text',
        'leave_time_text'
    ];
    

    



    public function getWorkingTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['working_time']) ? $data['working_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEntryTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['entry_time']) ? $data['entry_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getLeaveTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['leave_time']) ? $data['leave_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setWorkingTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEntryTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setLeaveTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function edit_teacher($param)
    {
        return $this->where('id',$param['id'])->strict(false)->data($param)->update();
    }
    public function user()
    {
        return $this->belongsTo('User', 'mobile', 'mobile', [], 'LEFT')->setEagerlyType(0);
    }
}
