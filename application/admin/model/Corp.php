<?php

namespace app\admin\model;

use think\Model;


class Corp extends Model
{





    // 表名
    protected $name = 'corp';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'ticket_time_text'
    ];






    public function getTicketTimeTextAttr($value, $data)
    {

        return $value ? date("Y-m-d H:i:s", $value) : $value;
    }




}
