<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Meet extends Model
{

    use SoftDelete;



    // 表名
    protected $name = 'meet';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'sign_way_text',
        'have_goods_text',
        'status_text',
        'start_time_text',
        'end_time_text',
        'late_time_text',
        'normal_time_text'
    ];


    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }


    public function getStatusList()
    {
        return [ '2' => __('Status 2'),'1' => __('Status 1'), '3' => __('Status 3'), '21' => __('Status 21'),
           // '31' => __('Status 31'),
            '8' => __('Status 8'),'9' => __('Status 9'),'0' => __('Status 0'), '-1' => __('Status -1')];
    }



    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getSignWayList()
    {
        return [0=>'无','1' => __('sign_way1'), '2' => __('sign_way2')];
    }

    public function getSignWayTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sign_way']) ? $data['sign_way'] : '');
        $list = $this->getSignWayList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHaveGoodsList()
    {
        return ['0' => __('have_goods0'), '1' => __('have_goods1')];
    }

    public function getHaveGoodsTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['have_goods']) ? $data['have_goods'] : '');
        $list = $this->getHaveGoodsList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStartTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['start_time']) ? $data['start_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['end_time']) ? $data['end_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getLateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['late_time']) ? $data['late_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getNormalTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['normal_time']) ? $data['normal_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setStartTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setLateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setNormalTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
