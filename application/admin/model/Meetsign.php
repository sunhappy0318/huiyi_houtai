<?php

namespace app\admin\model;

use app\api\model\MeetTypeModel;
use app\api\model\MeetUserModel;
use app\api\model\room\RoomModel;
use think\Model;
use traits\model\SoftDelete;

class Meetsign extends Model
{


    // 表名
    protected $name = 'meet_sign';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime =  false;

    // 追加属性
    protected $append = [
        'status_text',
        'sign_way_text',
    ];




    public function getStatusList()
    {
        return ['1' => __('Status 1'), '2' => __('Status 2'), '3' => __('Status 3'), '4' => __('Status 4'), '0' => __('Status 0')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getSignWayList()
    {
        return ['1' => __('sign_way1'), '2' => __('sign_way2')];
    }

    public function getSignWayTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sign_way']) ? $data['sign_way'] : '');
        $list = $this->getSignWayList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function meet()
    {
        return $this->belongsTo('Meet', 'meet_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
