<?php

namespace app\admin\model\footprint;

use app\admin\model\Admin;
use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;

class Article extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'footprint_article';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'send_time_text'
    ];

    public function getSendTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['send_time']) ? $data['send_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setSendTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_user_id', 'id', 'c', 'left')->setEagerlyType(0);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'b', 'left')->setEagerlyType(0);
    }
}
