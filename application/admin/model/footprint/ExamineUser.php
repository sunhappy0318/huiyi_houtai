<?php

namespace app\admin\model\footprint;

use think\Model;
use traits\model\SoftDelete;

class ExamineUser extends Model
{
    use SoftDelete;
    // 表名
    protected $name = 'footprint_examine_user';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];

    public function user()
    {
        return $this->belongsTo(\app\common\model\User::class, 'user_id', 'id', 'b', 'left')->setEagerlyType(0);
    }
}
