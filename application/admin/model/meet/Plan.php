<?php

namespace app\admin\model\meet;

use think\Model;


class Plan extends Model
{

    

    

    // 表名
    protected $name = 'meet_plan';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'use_time_text'
    ];
    

    



    public function getUseTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['use_time']) ? $data['use_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUseTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function department()
    {
        return $this->belongsTo('Department', 'department_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
