<?php

namespace app\admin\model;

use think\Model;


class Meetconfig extends Model
{

    

    

    // 表名
    protected $name = 'meet_config';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'integer';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'app_type_text'
    ];
    

    
    public function getAppTypeList()
    {
        return ['1' => __('App_type 1'), '2' => __('App_type 2')];
    }


    public function getAppTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['app_type']) ? $data['app_type'] : '');
        $list = $this->getAppTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
