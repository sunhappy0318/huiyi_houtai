<?php

return [
    'Id'              => 'ID',
    'User_id'         => '会员ID',
    'Department_id'   => '部门',
    'Leader_content'  => '领导交办事项',
    'End_content'     => '完成情况',
    'Xiaojie'         => '部门小结',
    'Next_content'    => '下周工作内容',
    'Createtime'      => '创建时间',
    'Updatetime'      => '更新时间',
    'User.nickname'   => '昵称',
    'Department.name' => '部门名称'
];
