<?php

return [
    'Meet_id'    => '会议ID',
    'User_id'    => '发起人',
    'Title'      => '会议主题',
    'Subtitle'   => '副标题',
    'Status'     => '状态',
    'Status 1'   => '已经通过',
    'Status 2'   => '待审批',
    'Status 3'   => '已驳回',
    'Status 0'   => '草稿',
    'Sort'       => '排序(从大到小)',
    'Images'     => '多图',
    'Content'    => '会议内容详情',
    'Files'      => '附件内容',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
