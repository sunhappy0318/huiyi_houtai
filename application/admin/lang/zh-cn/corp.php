<?php

return [
    'Id'             => 'ID',
    'Suiteid'        => '第三方应用的SuiteId',
    'Open_corpid'    => '代开发corpid',
    'Ticket_time'    => '更新时间',
    'Permanent_code' => '永久授权码',
    'Corpid'         => '企业ID',
    'Corp_name'      => '企业名称',
    'Corp_text'      => '授权信息',
    'Ticket_json'    => '隔间30分钟推送的suite_ticket',
    'Createtime'     => '创建时间',
    'Updatetime'     => '更新时间'
];
