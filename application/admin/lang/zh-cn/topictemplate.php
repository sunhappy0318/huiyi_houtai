<?php

return [
    'Id'         => 'ID',
    'Title'      => '模板标题',
    'Content'    => '议题模板内容',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
