<?php

return [
    'Room_id'    => '会议室ID（不为零代表审核的是会议室）',
    'Meet_id'    => '会议ID',
    'Topic_id'   => '议题ID',
    'Summary_id' => '纪要ID',
    'User_id'    => '审核人',
    'Type'       => '类别',
    'Type1'       => '会议室',
    'Type2'       => '会议',
    'Type3'       => '议题',
    'Type4'       => '纪要',
    'Type5'       => '投票',
    'Title'      => '审核主题',
    'Status'     => '状态',
    'Status 1'   => '同意',
    'Status 2'   => '待回复',
    'Status 0'   => '不同意',
    'Content'    => '审核意见',
    'Files'      => '附件内容(签字盖章图片)',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
