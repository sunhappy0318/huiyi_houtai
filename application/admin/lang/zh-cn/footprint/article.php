<?php

return [
    'User_id'    => '用户名称',
    'Title'      => '标题',
    'Content'    => '文章内容',
    'Diagram'    => '摘要图',
    'Video'      => '视频',
    'Images'     => '图片',
    'Type'       => '是否草稿箱',
    'Status'     => '审核状态',
    'Send_time'  => '发送时间',
    'Createtime' => '创建时间',
    'Updatetime' => '修改时间',
    'Deletetime' => '删除时间',
    'Is_article' => '视频OR文章',
    'Examine'    => '拒绝理由'
];
