<?php

return [
    'Meet_id'    => '会议ID',
    'User_id'    => '签到人',
    'Title'      => '签到问候语',
    'Sign_way'   => '签到方式',
    'Sign_way1'   => '页面签到',
    'Sign_way2'   => '二维码签到',
    'Status'     => '状态',
    'Status 1'   => '签到',
    'Status 2'   => '迟到 ',
    'Status 3'   => '缺席',
    'Status 4'   => '缺勤',
    'Status 0'   => '草稿',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
