<?php

return [
    'Id'               => 'ID',
    'Type'             => '会议类型',
    'Type1'             => '党总支会议',
    'Type2'             => '校长办公会议',
    'Type3'             => '行政会议',
    'Type4'             => '全体教职工会议',
    'Type5'             => '其它会议',
    'Type6'             => '清外活动',
    'Title'            => '会议名称',
    'Auth'             => '权限范围',
    'Examine'          => '审核流程',
    'Content'          => '内容',
    'Attend_meet_ids'  => '参会人',
    'Attend_topic_ids' => '议题参与人',
    'Approver_ids'     => '审批人',
    'Issued_ids'       => '签发人',
    'Check_ids'        => '校长',
    'Deletetime'       => '删除时间'
];
