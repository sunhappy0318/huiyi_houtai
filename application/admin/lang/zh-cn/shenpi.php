<?php

return [
    'Id'            => 'ID',
    'Date_string'   => '审批日期 例如2022-09-13',
    'User_id'       => '申请人userid',
    'Department'    => '申请人部门',
    'Sp_status'     => '审批节点状态 已同意',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'User.nickname' => '昵称'
];
