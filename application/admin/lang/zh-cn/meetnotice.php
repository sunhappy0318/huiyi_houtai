<?php

return [
    'Link_id'    => '关联ID',
    'From_id'    => '来自',
    'User_id'    => '接收人',
    'Type'       => '类别：room,meet,topic,summary等',
    'Title'      => '审核主题',
    'Status'     => '状态',
    'Status 1'   => '已读',
    'Status 2'   => '待回复',
    'Status 0'   => '删除',
    'Content'    => '审核内容详情',
    'Start_time' => '展示时间',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
