<?php

return [
    'Id'         => 'ID',
    'App_type'   => '应用类型',
    'App_type 1' => '自建应用',
    'App_type 2' => '代开发应用',
    'Title'      => '应用名称',
    'Corpid'     => '类型',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
