<?php

return [
    'Id'         => 'ID',
    'Contact'    => '联系方式',
    'Room_id'    => '房间ID',
    'Title'      => '会议名称',
    'People'     => '人数',
    'Starttime'  => '开始时间',
    'Endtime'    => '结束时间',
    'Status'     => '状态',
    'Status 0'   => '待审核',
    'Status 1'   => '通过',
    'Status -1'  => '已拒绝',
    'Date'       => '会议开始时间',
    'Hour'       => '会议时长',
    'Users'      => '参会人员',
    'Push'       => '推送状态',
    'Push 0'     => '未推送',
    'Push 1'     => '已推送',
    'Push 2'     => '推送失败',
    'Createtime' => '创建时间',
    'Room.title' => '房间标题',
];
