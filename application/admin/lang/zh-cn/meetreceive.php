<?php

return [
    'Meet_id'    => '会议ID',
    'User_id'    => '领取人',
    'Title'      => '领取物品',
    'Num'        => '份数',
    'Status'     => '状态',
    'Status 1'   => '领取',
    'Status 2'   => '代领',
    'Status 0'   => '未领',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
