<?php

return [
    'Id'          => 'ID',
    'Title'       => '名称',
    'Field'       => '类型',
    'Field_value' => '值',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
