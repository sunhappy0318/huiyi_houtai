<?php

return [
    'Id'              => 'ID',
    'Date_string'     => '日期',
    'Week_text'       => '星期',
    'Week_num'        => '星期几 数字',
    'User_id'         => '会员ID',
    'Department_id'   => '部门',
    'Fuzeren'         => '负责任',
    'Cangsuo'         => '使用场所',
    'Use_time'        => '使用时间',
    'Is_chongtu'      => '是否和其他人时间冲突1=是,0=否',
    'Zhongxin'        => '中心计划',
    'Remark'          => '备注',
    'Images'          => '多图',
    'Files'           => '附件内容',
    'Createtime'      => '创建时间',
    'Updatetime'      => '更新时间',
    'User.nickname'   => '昵称',
    'Department.name' => '部门名称'
];
