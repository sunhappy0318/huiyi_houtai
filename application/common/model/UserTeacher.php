<?php

namespace app\common\model;

use think\Db;
use think\Model;

/**
 * 会员老师模型
 */
class UserTeacher extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;

    public function auth($pmobile,$param)
    {
        if (empty($param['is_all']) || $param['is_all'] != '1') {
            $param['is_all'] = 0;
        }
        $param['mobile'] = explode(',',$param['mobile']);
        if (empty($param['mobile'])) {
            $param['mobile'] = [$pmobile];
        }
        if ($param['is_all'] == '1') {
            $param['mobile'] = [$pmobile];
        }
        $adds = [];
        foreach ($param['mobile'] as $mobile) {
            $adds[] = [
                'pmobile'=>$pmobile,
                'is_all'=>$param['is_all'],
                'mobile'=>$mobile,
                'auth_id' => isset($param['auth_id'])?$param['auth_id']:0,
            ];
        }
        $this->where('pmobile',$pmobile)->delete();
        return $this->saveAll($adds);
    }
}
