<?php

return array (
  'name' => '我的网站',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' =>
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' =>
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' =>
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'user' => 'User',
    'example' => 'Example',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'attachmentcategory' =>
  array (
    'category1' => 'Category1',
    'category2' => 'Category2',
    'custom' => 'Custom',
  ),
  'meet_sign_tip' => '{"late_time":"10:00-10:10","normal_time":"10:10-11:10","normal_tip":"谢谢你准时参加会议","late_tip":"下次记得时间哦","absence_tip":"你错过了一个重要的会议"}',
);

